/**
 * Created by rgu on 6/15/16.
 */
window.GLOBAL = {};
//local
// GLOBAL.baseURL = 'http://192.168.0.51:9789';
//test
// GLOBAL.baseURL = 'http://120.24.219.30/jzh';
//production
GLOBAL.baseURL = 'http://52.80.8.152:8080/jzh';
GLOBAL.contentType = 'application/json;charset=utf-8';
GLOBAL.alibabaDelay = null;
Date.prototype.Format = function (fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
Array.prototype.contains = function ( val ) {
  for (var i = 0; i < this.length; i++) {
      if(this[i] == val){
          return false;
          break;
      }
  }
  return true;
}
