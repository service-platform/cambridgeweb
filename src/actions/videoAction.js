/**
 * Created by Administrator on 2016/10/9.
 */
import { notification, Modal, message } from 'antd';
import {browserHistory} from 'react-router';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;
//家长汇 视频详情
export function videoDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/video/videoDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          var tempVideo = response.data.data;
          if(tempVideo.commitTime != undefined){
              tempVideo.commitTime = new Date(tempVideo.commitTime).Format('yyyy-MM-dd hh:mm:ss');
          }
          obj.setState({
            spinLoading: false,
            visibleVideo: true,
            videoData: response.data.data
          })
        }else{
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，查看详情错误。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除文章音频
export function removeArticleAudio(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/removeArticleAudio', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            delete obj.state.serviceVideoData.audio;
            obj.setState({
                deleteAudioLoading: false
            });
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '音频删除成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({deleteAudioLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除文章视频
export function removeArticleVideo(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/removeArticleVideo', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            delete obj.state.serviceVideoData.video;
            obj.setState({
                deleteVideoLoading: false
            });
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '视频删除成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({deleteVideoLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除图文元素
export function removeImageText(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    var itId = paramData.itId;
    axios.post('/webAPI/article/removeImageText', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            for (var i = 0; i < obj.state.serviceVideoData.imageTextList.length; i++) {
                var imageText = obj.state.serviceVideoData.imageTextList[i];
                if(imageText.id == itId){
                    var temp = obj.state.serviceVideoData.imageTextList.splice(i, 1);
                }
            }
            if(obj.state.serviceVideoData.imageTextList.length <= 1){
                $(".divSorting").css("display", "none");
                $("#divImageText").css("backgroundColor", "#fff");
                $("#divImageTextTitle").css("backgroundColor", "#4B95E9");
                obj.setState({sortIsEnabled: false});
            }
            obj.setState({
                deleteImageLoading: false
            });
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '图文删除成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({deleteImageLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//只删除图片部分（图文）
export function removeImageTextPic(obj, id, index) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    var itId = id;
    axios.post('/webAPI/article/removeImageTextPic?id=' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            for (var i = 0; i < obj.state.serviceVideoData.imageTextList.length; i++) {
                var imageText = obj.state.serviceVideoData.imageTextList[i];
                if(imageText.id == itId){
                    delete imageText.pic; break;
                }
            }
            obj.setState({serviceVideoData: obj.state.serviceVideoData});
            obj.refs["imageText_" + index].setState({spinning: false});
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '图片，删除成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.refs["imageText_" + index].setState({spinning: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//获取文章预览
export function findArticleDetialForPreview(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/findArticleDetial/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({
                loading: false,
                serviceData: response.data.data
            });
        }
    }).catch(function(err){
        obj.setState({loading: false});
        message.error('对不起，连接错误。');
    })
}
//获取文章详情
export function findArticleDetial(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/findArticleDetial/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({
                isAddImageAndTextLock: false,
                serviceVideoData: response.data.data
            });
            if(response.data.data.article != undefined){
                obj.props.gotoFinish();
            }

            obj.refs["tableView"].refreshData();
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}

//设置显示状态（inactive、active）
export function changeArticleStatus(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    var status = paramData.status;
    axios.post('/webAPI/article/changeArticleStatus/' + paramData.id + '/' + paramData.status, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({
                serviceVideoData:{
                    ...obj.state.serviceVideoData,
                    article:{
                        ...obj.state.serviceVideoData.article,
                        status: status
                    }
                }
            });
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '状态设置成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({deleteVideoLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}

//设置显示状态，来自列表（inactive、active）
export function changeArticleStatusByList(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    var status = paramData.status;
    var currentTarget = paramData.currentTarget;
    axios.post('/webAPI/article/changeArticleStatus/' + paramData.id + '/' + paramData.status, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data.status == "inactive"){
                $(currentTarget).css("display", "none");
                $(currentTarget).prev().css("display", "inline-block");
                $(currentTarget).next().text("状态：未公开");
                $(currentTarget).next().attr("class", "spanStatusRed");
            }else{
                $(currentTarget).css("display", "none");
                $(currentTarget).next().css("display", "inline-block");
                $(currentTarget).next().next().text("状态：公开");
                $(currentTarget).next().next().attr("class", "spanStatus");
            }
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '状态设置成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//编辑图文，图片可选
export function editImageText(obj, paramData) {
    var formData = new FormData();
    if(paramData.id != undefined){
        formData.append("id", paramData.id);
    }
    if(paramData.itId != undefined){
        formData.append("itId", paramData.itId);
    }
    formData.append("content", paramData.content.replace(/&quot;/g,"\"").replace(/&nbsp;/g," "));
    formData.append("order", paramData.order);
    formData.append("type", paramData.type);
    if ($("#filePic")[0].files.length >= 1){
        formData.append("pic", $("#filePic")[0].files[0]);
    }
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/article/editImageText",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      xhr: function(){
　　　　　　var xhr = $.ajaxSettings.xhr();

　　　　　　if(xhr.upload && $("#filePic")[0].files.length >= 1) {
　　　　　　　　xhr.upload.addEventListener("progress" , function(evt){
                var loaded = evt.loaded;  //已经上传大小情况
                var tot = evt.total;      //附件总大小
                var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
                if(per < 100){
                    obj.setState({
                      updateImageAndTextProgress: '上传图片中，请耐心等待（'+ per + "%" + '）...'
                    });
                }else{
                    obj.setState({
                      updateImageAndTextProgress: '文件上传中，请耐心等待...'
                    });
                }
              }, false);
　　　　　　　　return xhr;
　　　　　　}
           return xhr;
　　　 },
      success: function(response){
          if(response.code == 1){
              if(response.data.status != "0"){
                  $("#filePic").val('');
                  obj.state.id = response.data.id;
                  obj.btnShowImage(false);
                  obj.onRefreshData();
                  obj.setState({isAddImageAndTextLock: false});

                  notification.success({
                      placement: 'bottomRight',
                      message: '提示：',
                      description: '图文信息，保存成功。',
                      style: {
                        borderLeft: '4px solid rgb(23, 168, 84)'
                      }
                  });
              }else if (true) {
                  Modal.warning({
                      title: '提示：',
                      content: '请刷新后重试。',
                      onOk: function(){
                        obj.btnShowImage(false);
                        obj.setState({isAddImageAndTextLock: false});
                        obj.onRefreshData();
                      }
                  });
              }
          }
      },
      error: function(err){
          obj.setState({updateImageLoading: false, isAddImageAndTextLock: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}

//编辑基础信息，首次图片必选
export function editArticle(obj, paramData) {
    var formData = new FormData();
    if(paramData.id != undefined){
        formData.append("id", paramData.id);
    }
    formData.append("title", paramData.title);
    formData.append("summary", paramData.summary);
    formData.append("type", paramData.type);
    if ($("#basePic")[0].files.length >= 1){
        formData.append("coverPicture", $("#basePic")[0].files[0]);
    }
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/article/editArticle",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      xhr: function(){
　　　　　　var xhr = $.ajaxSettings.xhr();
　　　　　　if(xhr.upload) {
　　　　　　　　xhr.upload.addEventListener("progress" , function(evt){
                var loaded = evt.loaded;  //已经上传大小情况
                var tot = evt.total;      //附件总大小
                var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
                if(per < 100){
                    obj.setState({
                      updateBaseProgress: '上传图片中，请耐心等待（'+ per + "%" + '）...'
                    });
                }else{
                    obj.setState({
                      updateBaseProgress: '文件上传中，请耐心等待...'
                    });
                }
              }, false);
　　　　　　　　return xhr;
　　　　　　}
           return xhr;
　　　 },
      success: function(response){
          if(response.code == 1){
              $("#basePic").val('');
              obj.state.id = response.data.id;
              obj.btnShowBaseInfo(false);

              if(obj.state.serviceVideoData == null){
                  obj.onRefreshData();
              }else{
                  if(response.data.type == obj.state.serviceVideoData.article.type){
                      obj.onRefreshData();
                  }else {
                      var isTravel = location.href.split('/')[location.href.split('/').length-1];
                      browserHistory.push("/articleDesign/" + response.data.id + "," + response.data.type + "/" + isTravel);
                      location.reload();
                  }
              }

              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '基础信息，保存成功。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }
      },
      error: function(err){
          obj.setState({updateBaseLoading: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}
//编辑基础信息类型，首次图片必选
export function UpdateArticleType(obj, paramData) {
    var formData = new FormData();
    if(paramData.id != undefined){
        formData.append("id", paramData.id);
    }
    formData.append("title", paramData.title);
    formData.append("summary", paramData.summary);
    formData.append("type", paramData.type);
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/article/editArticle",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      success: function(response){
          if(response.code == 1){
              obj.state.id = response.data.id;
              obj.onRefreshData();

              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '文章类型，修改成功。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }
      },
      error: function(err){
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}
//编辑音频信息，首次音频必选
export function editArticleAudio(obj, paramData) {
    var formData = new FormData();
    if(paramData.id != undefined){
        formData.append("id", paramData.id);
    }
    formData.append("title", paramData.title);
    formData.append("author", paramData.author);
    formData.append("content", paramData.content);
    formData.append("type", paramData.type);
    if ($("#fileAudio")[0].files.length >= 1){
        formData.append("audio", $("#fileAudio")[0].files[0]);
    }
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/article/editArticleAudio",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      timeout: 900000, //超时时间：900 秒
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      xhr: function(){
　　　　　　var xhr = $.ajaxSettings.xhr();
　　　　　　if(xhr.upload) {
　　　　　　　　xhr.upload.addEventListener("progress" , function(evt){
                var loaded = evt.loaded;  //已经上传大小情况
                var tot = evt.total;      //附件总大小
                var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
                if(per < 100){
                    obj.setState({
                      updateAudioProgress: '上传中，音频超过 5M，请耐心等待（'+ per + "%" + '）...'
                    });
                }else{
                    obj.setState({
                      updateAudioProgress: '文件上传中，请耐心等待...'
                    });
                    window.GLOBAL.alibabaDelay = setTimeout(function(){
                        $("#fileAudio").val('');
                        obj.btnShowAudio(false);
                        obj.onRefreshData();

                        notification.warning({
                            placement: 'bottomRight',
                            message: '提示：',
                            description: '上传音频超大，如刷新，没有音频，请等候服务器处理完成。',
                            style: {
                              borderLeft: '4px solid rgb(23, 168, 84)'
                            }
                        });
                    },300000);
                }
              }, false);
　　　　　　　　return xhr;
　　　　　　}
           return xhr;
　　　 },
      success: function(response){
          if(response.code == 1){
              clearTimeout(window.GLOBAL.alibabaDelay);
              $("#fileAudio").val('');
              obj.state.id = response.data.id;
              obj.btnShowAudio(false);
              obj.onRefreshData();

              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '音频信息，保存成功。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }
      },
      error: function(err){
          obj.setState({updateAudioLoading: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}
//上传音频 缩略图
export function uploadThumbnail(obj, id) {
    var formData = new FormData();
    formData.append("id", id);
    if ($("#fileThumbnail")[0].files.length >= 1){
        formData.append("thumbnail", $("#fileThumbnail")[0].files[0]);
    }
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/audio/uploadThumbnail",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      timeout: 900000, //超时时间：900 秒
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      xhr: function(){
　　　　　　var xhr = $.ajaxSettings.xhr();
　　　　　　if(xhr.upload) {
　　　　　　　　xhr.upload.addEventListener("progress" , function(evt){
                var loaded = evt.loaded;  //已经上传大小情况
                var tot = evt.total;      //附件总大小
                var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
                if(per < 100){
                    obj.setState({
                      updateCoverPicProgress: '上传封面图，请等待（'+ per + "%" + '）...'
                    });
                }else{
                    obj.setState({
                      updateCoverPicProgress: '文件上传中，请耐心等待...'
                    });
                }
              }, false);
　　　　　　   return xhr;
　　　　　　}
           return xhr;
　　　 },
      success: function(response){
          if(response.code == 1){
              $("#fileThumbnail").val('');
              obj.setState({updateThumbnailVisible: false, updateThumbnailLoading: false});
              obj.onRefreshData();

              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '缩略图，上传成功。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }
      },
      error: function(err){
          obj.setState({updateThumbnailLoading: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}
//删除音频 缩略图
export function removeThumbnail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/audio/removeThumbnail?id=' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            $("#btnDeleteThumbnail").css("display", "none");
            $("#thumbnailImg").attr("src", "/src/styles/images/disk.png");
            obj.onRefreshData();
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '缩略图，删除成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
        obj.setState({deleteAudioLoading: false});
    }).catch(function(err){
        obj.setState({deleteAudioLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//编辑视频信息，首次视频必选
export function editArticleVideo(obj, paramData) {
    var formData = new FormData();
    if(paramData.id != undefined){
        formData.append("id", paramData.id);
    }
    formData.append("author", paramData.author);
    formData.append("content", paramData.content);
    formData.append("type", paramData.type);
    if ($("#fileVideo")[0].files.length >= 1){
        formData.append("video", $("#fileVideo")[0].files[0]);
    }
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/article/editArticleVideo",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      timeout: 900000, //超时时间：900 秒
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      xhr: function(){
　　　　　　var xhr = $.ajaxSettings.xhr();
　　　　　　if(xhr.upload) {
　　　　　　　　xhr.upload.addEventListener("progress" , function(evt){
                var loaded = evt.loaded;  //已经上传大小情况
                var tot = evt.total;      //附件总大小
                var per = Math.floor(100 * loaded / tot);  //已经上传的百分比
                if(per < 100){
                    obj.setState({
                      updateVideoProgress: '上传中，视频超过 5M，请耐心等待（'+ per + "%" + '）...'
                    });
                }else{
                    obj.setState({
                      updateVideoProgress: '文件上传中，请耐心等待...'
                    });
                    window.GLOBAL.alibabaDelay = setTimeout(function(){
                        $("#fileVideo").val('');
                        obj.btnShowVideo(false);
                        obj.onRefreshData();

                        notification.warning({
                            placement: 'bottomRight',
                            message: '提示：',
                            description: '上传视频超大，如刷新，没有视频，请等候服务器处理完成。',
                            style: {
                              borderLeft: '4px solid rgb(23, 168, 84)'
                            }
                        });
                    },300000);
                }
              }, false);
　　　　　　　　return xhr;
　　　　　　}
           return xhr;
　　　 },
      success: function(response){
          if(response.code == 1){
              clearTimeout(window.GLOBAL.alibabaDelay);
              $("#fileVideo").val('');
              obj.state.id = response.data.id;
              obj.btnShowVideo(false);
              obj.onRefreshData();

              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '视频信息，保存成功。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }
      },
      error: function(err){
          obj.setState({updateVideoLoading: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}
//设置评论的状态，显示 或 隐藏（inactive、active）
export function changeCommentStatus(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/changeCommentStatus/' + paramData.id + '/' + paramData.status, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(obj.onRefreshData != undefined){obj.onRefreshData();}
            setTimeout(function(){
              obj.refs["tableView"].refreshData();
            }, 500);
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '评论状态，设置成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//设置推广文章，（extension要么为true要么为false）
export function weatherExtension(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/weatherExtension/' + paramData.id + '/' + paramData.extension, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.onRefreshData();
            var strExtension = '首页推广，设置成功。';
            if(response.data.data.extension == false){
                strExtension = '取消首页推广';
            }
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: strExtension,
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//启用关闭 报名、投票 功能，（enrollOrVote 要么为true要么为false）
export function changeEnroll(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/changeEnroll/' + paramData.id + '/' + paramData.enrollOrVote, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.onRefreshData();
            var strExtension = '报名、投票，启用成功。';
            if(response.data.data.enrollOrVote == false){
                strExtension = '关闭报名、投票';
            }
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: strExtension,
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//管理员回复文章评论（添加、修改）
export function replayComment(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/replayComment', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            if(obj.onRefreshData != undefined){
                obj.onRefreshData();
            }else{
                obj.refs["tableView"].refreshData();
            }
            obj.setState({showAdminReplay: false});
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: "评论回复成功，等待刷新。",
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除管理员回复
export function removeReplyComment(obj, cId) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/removeReplyComment/' + cId, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(obj.onRefreshData != undefined){
                obj.onRefreshData();
            }else{
                obj.refs["tableView"].refreshData();
            }
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: "回复删除成功，等待刷新。",
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//文章图文排序接口
export function adjustArticleOrder(obj, aId, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }

    axios.post('/webAPI/article/adjustArticleOrder/' + aId, paramData)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data.length > 0){
                obj.setState({orderUpdating: false});
                obj.onRefreshData();
                notification.success({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: "排序成功，自动刷新。",
                    style: {
                      borderLeft: '4px solid rgb(23, 168, 84)'
                    }
                });
            }else {
                Modal.warning({
                    title: '提示：',
                    content: '请刷新后重试。',
                    onOk: function(){
                      obj.setState({orderUpdating: false});
                      obj.onRefreshData();
                    }
                });
            }
        }
    }).catch(function(err){
        obj.setState({orderUpdating: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//点击某条评论，查看详情数
export function commentDetail(obj, cId) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/commentDetail/' + cId, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data.comment.status == "active"){
                obj.setState({commentDetail: response.data.data, commentStatus: "（状态：公开）"});
            }else {
                obj.setState({commentDetail: response.data.data, commentStatus: "（状态：禁用）"});
            }
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//文章点赞人详细信息
export function findThumbsUpsByAid(obj, aId) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/findThumbsUpsByAid?aId=' + aId, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({articleThumbsUpsDetail: response.data.data});
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//添加、编辑，相关推荐
export function addOrEditReviewOfThePast(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/addOrEditReviewOfThePast', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({articleList: false, updateLoading: false});
            obj.onRefreshData();
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: "相关推荐，编辑成功。",
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除，相关推荐
export function removeReviewOfThePast(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/removeReviewOfThePast/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({deleteReviewOfThePast: false});
            obj.onRefreshData();
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: "相关推荐，删除成功。",
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//根据 openId , 查询家长信息
export function findParentByOpenId(obj, openId) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/parent/findParentByOpenId/' + openId, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.renderByOpenId(response.data.data);
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查询用户详情
export function commentDetailByUserDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/commentDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.renderByUserDetail(response.data.data.replyUser);
        }
    })
}
//根据 Id 查询文章
export function findArticleByAId(obj, articleId, flag) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/comment/findArticleByAId/' + articleId, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(flag == true){
              obj.renderByArticleDetail(response.data.data);
            }else {
              obj.setState({spinningOpenDetails: false});
              var isTravel = 0;
              if(response.data.data.article.type >= 1 && response.data.data.article.type <= 12){
                  isTravel = 0;
              }else if(response.data.data.article.type >= 13 && response.data.data.article.type <= 15){
                  isTravel = 1;
              }else if(response.data.data.article.type >= 16 && response.data.data.article.type <= 29){
                  isTravel = 2;
              }
              browserHistory.push("/articleDesign/" + response.data.data.article.id + "," + response.data.data.article.type + "/" + isTravel);
            }
        }
    }).catch(function(err){
        obj.setState({spinningOpenDetails: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//根据时间区间，查询家长信息
export function findParentsByOpenIds(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/Notify/findParentsByOpenIds')
    .then(function (response) {
        obj.setState({
          parentsListData: response.data
        });
        window.onresize();
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//根据 openIds 发送所有通知
export function sendNotification(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/Notify/sendNotification', paramData)
    .then(function (response) {
        obj.setState({sendingNotifications: false});
        if(response.data == "success"){
          obj.setState({
            tempParentsData: [],
            isSendNotifNext: true,
            tempInfoData:{
              title: '',
              type: '',
              summary: '',
              Remark: ''
            }
          });
          obj.btnShowParentsList(false);
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: "通知发送成功。",
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }
    }).catch(function(err){
        obj.setState({sendingNotifications: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
