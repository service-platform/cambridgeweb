/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//轮播图 添加、编辑
export function addOrEditCarousel(obj, paramData) {
    obj.setState({updateCarouselLoading: true});
    var formData = new FormData();
    if(paramData.cId != undefined){
        formData.append("cId", paramData.cId);
    }
    if(paramData.aId != undefined){
        formData.append("aId", paramData.aId);
    }
    if(paramData.describe != undefined){
        formData.append("describe", paramData.describe);
    }
    if($("#carouselPic")[0].files.length > 0){
        formData.append("pic", $("#carouselPic")[0].files[0]);
    }
    $.ajax({
      url: GLOBAL.baseURL + "/webAPI/Carousel/addOrEditCarousel",
      type: "POST",
      cache: false,
      data: formData,
      dataType: "json",
      headers: {
        Accept: "*/*",
        'X-Requested-With': 'XMLHttpRequest',
        userAuth: sessionStorage.getItem('userAuth')
      },
      contentType: "multipart/form-data",
      processData: false,  // 告诉jQuery不要去处理发送的数据
      contentType: false,   // 告诉jQuery不要去设置Content-Type请求头
      success: function(response){
          if(response.code == 1){
              obj.setState({serverData: []});
              obj.btnShowCarousel(false);
              obj.onRefreshData();
              obj.setState({updateCarouselLoading: false});

              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '轮播图片，保存成功（默认：禁用）。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }
      },
      error: function(err){
          obj.setState({updateCarouselLoading: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，服务器连接错误，请联系相关管理人员。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
      }
    });
}
export function findCarousels(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/Carousel/findCarousels', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({serverData: response.data.data});
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//修改轮播状态(active 和 inactive)
export function changeStatus(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    var strStatus = ((paramData.status == 'active')?'启用':'禁用')
    axios.post('/webAPI/Carousel/changeStatus?cId=' + paramData.cId +'&status=' + paramData.status, null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.onRefreshData();
          obj.setState({visibleStatusAlert: false});
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '状态修改成功（' + strStatus + '）。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//移除轮播图
export function removeCarousel(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/Carousel/remove/', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.onRefreshData();
          obj.setState({visibleDeleteAlert: false});
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '轮播图片，删除成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//获取文章详情
export function findArticleDetial(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/article/findArticleDetial/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({
            tempArticleData: response.data.data.article,
            articleData: response.data.data.article,
            isHaveArticle: true,
            btnSaveDisabled: false
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
