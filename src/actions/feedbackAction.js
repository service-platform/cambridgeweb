/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import {browserHistory} from 'react-router';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//家长汇 反馈详情
export function feedbackDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.get('/webAPI/Feedback/feedbackDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            var parent = response.data.data.parent;
            if(parent.joinTime != undefined){
                parent.joinTime = new Date(parent.joinTime).Format('yyyy-MM-dd hh:mm:ss');
            }
            var feedback = response.data.data.feedback;
            if(feedback.feedbackTime != undefined){
                feedback.feedbackTimeStr = new Date(feedback.feedbackTime).Format('yyyy-MM-dd hh:mm:ss');
            }
            var phonesString = ""; //临时字符串
            for (var i = 0; i < parent.phones.length; i++) {
                if(i < (parent.phones.length-1)){
                   phonesString += parent.phones[i] + "、";
                }else{
                   phonesString += parent.phones[i];
                }
            }
            parent.phones = phonesString;

            if (response.data.data.feedback.status == '0') {
                for (var i = 0; i < response.data.data.students.length; i++) {
                  response.data.data.students[i].bindTime = new Date(response.data.data.students[i].bindTime).Format('yyyy-MM-dd hh:mm:ss');
                }
                obj.setState({
                    isDisable: false,
                    isLoading: 2,
                    parent: response.data.data.parent,
                    feedback: response.data.data.feedback,
                    students: response.data.data.students
                })
            }else if (response.data.data.feedback.status == '1') {
                var feedback = response.data.data.feedback;
                if(feedback.replyTime != undefined){
                    feedback.replyTime = new Date(feedback.replyTime).Format('yyyy-MM-dd hh:mm:ss');
                }
                for (var i = 0; i < response.data.data.students.length; i++) {
                  response.data.data.students[i].bindTime = new Date(response.data.data.students[i].bindTime).Format('yyyy-MM-dd hh:mm:ss');
                }
                obj.setState({
                    isDisable: true,
                    isLoading: 2,
                    parent: response.data.data.parent,
                    feedback: response.data.data.feedback,
                    students: response.data.data.students
                })
            }
            if(response.data.data.feedback.picUrl != undefined){
                $('#viewerImg').viewer({
              		url: 'data-original',
              		navbar: false,
              		fullscreen: false,
              		scalable: false,
              		keyboard: false,
              		movable: true
              	});
            }
        }else{
          obj.setState({isLoading: 1});
          notification.error({
              placement: 'bottomRight',
              message: '提示你：',
              description: '对不起，加载错误。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    }).catch(function (err) {
        obj.setState({isLoading: 1});
        notification.error({
            placement: 'bottomRight',
            message: '提示你：',
            description: '对不起，网络错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//家长汇 反馈回复
export function replyFeedback(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/Feedback/reply', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({
            replyContent: '',
            feedbackVisible: false
          })
          obj.refreshData();
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '反馈回复成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }else{
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，回复失败。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    })
}
