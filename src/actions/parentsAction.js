/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import {browserHistory} from 'react-router';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//家长详情
export function findParentById(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/parent/findParentById/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            var parent = response.data.data.parent;
            if(parent.joinTime != undefined){
                parent.joinTime = new Date(parent.joinTime).Format('yyyy-MM-dd hh:mm:ss');
            }
            var phonesString = ""; //临时字符串
            for (var i = 0; i < parent.phones.length; i++) {
                if(i < (parent.phones.length-1)){
                   phonesString += parent.phones[i] + "、";
                }else{
                   phonesString += parent.phones[i];
                }
            }
            parent.phones = phonesString;

            for (var i = 0; i < response.data.data.students.length; i++) {
                var student = response.data.data.students[i];
                if(student.bindTime != undefined){
                    student.bindTime = new Date(student.bindTime).Format('yyyy-MM-dd hh:mm:ss');
                }
            }
            obj.setState({
              registerInfo: JSON.parse(sessionStorage.getItem("registerInfo")),
              parent: parent,
              students: response.data.data.students
            })
        }else{
          notification.error({
              placement: 'bottomRight',
              message: '提示你：',
              description: '对不起，获取详情失败。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    })
}

//获取学生详情
export function authorizationBindingDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/parent/authorizationBindingDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({showAuthorizedInfo: true, authorizedInfo: response.data.data});
            if(obj.state.msgLoading != null){obj.state.msgLoading();}
        }
    }).catch(function(err){
        if(obj.state.msgLoading != null){obj.state.msgLoading();}
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除授权绑定数据
export function removeAuthorizationBinding(obj, abId) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/parent/removeAuthorizationBinding/' + abId, null)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({isSpinning: false, isEdit: false, showAuthorizedInfo: false});
            obj.refs["tableView"].refreshData();
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '授权绑定，删除成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({isSpinning: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//修改学生信息
export function updateAuthorizationBindingDetail(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/parent/updateAuthorizationBindingDetail', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            obj.setState({isSpinning: false, isEdit: false});
            obj.refs["tableView"].refreshData();
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '学生信息，保存成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({isSpinning: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}

//工作人员  帮助绑定
export function staffHelpBind(obj, abId) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/parent/staffHelpBind/' + abId, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data.status == "success"){
                obj.setState({isSpinning: false, showAuthorizedInfo: false});
                obj.refs["tableView"].refreshData();
                notification.success({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '学生绑定，成功。',
                    style: {
                      borderLeft: '4px solid rgb(23, 168, 84)'
                    }
                });
            }else {
                obj.setState({isSpinning: false});
                notification.error({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: ((response.data.data.message == undefined)?"绑定失败，请核对信息。":response.data.data.message),
                    style: {
                      borderLeft: '4px solid rgb(240, 70, 52)'
                    }
                });
            }
        }
    }).catch(function(err){
        obj.setState({isSpinning: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//获取学生详情
export function findStuDetial(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/student/findStuDetial/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data.status == "success"){
                obj.setState({findStuDetial: JSON.parse(response.data.data.data), loading: false});
            }else {
                notification.warning({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: response.data.data.message,
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
            }

        }
    }).catch(function(err){
        obj.setState({loading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
