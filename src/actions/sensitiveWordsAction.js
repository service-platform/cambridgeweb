/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import {browserHistory} from 'react-router';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//家长汇 添加敏感词
export function addSensitiveWords(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/addOrEditSensitiveWord', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({
            addSensitiveWords: false,
            visibleAddAlert: false,
            updateLoading: false
          });
          obj.refs["tableView"].refreshData();
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '敏感词，添加成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }else{
          obj.setState({updateLoading: false});
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '敏感词，添加失败。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    }).catch(function(err){
        obj.setState({updateLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//删除敏感词
export function removeSensitiveWord(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/removeSensitiveWord/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({visibleDeleteAlert: false});
          obj.refs["tableView"].refreshData();
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '敏感词，删除成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//获取用户 ID
export function findUserProfile(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/findUserProfile', null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data != undefined){
                obj.setState({uId: response.data.data.uId})
            }
        }
    })
}
//获取关键字详情
export function sensitiveWordDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/sensitiveWordDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          response.data.data.sensitiveWord.createTime = new Date(response.data.data.sensitiveWord.createTime).Format("yyyy-MM-dd hh:mm:ss");
          response.data.data.sensitiveWord.updateTime = new Date(response.data.data.sensitiveWord.updateTime).Format("yyyy-MM-dd hh:mm:ss");
          obj.setState({
            createUser: response.data.data.createUser,
            updateUser: ((response.data.data.updateUser == undefined)?null:response.data.data.updateUser),
            sensitiveWord: response.data.data.sensitiveWord
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，服务器连接错误，请联系相关管理人员。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
