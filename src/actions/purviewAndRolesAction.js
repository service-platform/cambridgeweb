/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//添加、修改 角色
export function creatOrUpdateRole(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/role/creatOrUpdateRole', paramData)
    .then(function (response) {
        if(response.data.code == 1){
            obj.refs["tableView"].refreshData();
            obj.setState({addSensitiveWords: false, updateLoading: false});
            obj.props.form.resetFields();
            notification.success({
                placement: 'bottomRight',
                message: '提示：',
                description: '角色保存成功。',
                style: {
                  borderLeft: '4px solid rgb(23, 168, 84)'
                }
            });
        }
    }).catch(function(err){
        obj.setState({updateLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查询角色详情
export function findRoleDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/role/findRoleDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({
            selectData: response.data.data.role,
            createTimer: ((response.data.data.role.createTime == undefined)?null:new Date(response.data.data.role.createTime).Format("yyyy-MM-dd hh:mm:ss")),
            updateTimer: ((response.data.data.role.updateTime == undefined)?null:new Date(response.data.data.role.updateTime).Format("yyyy-MM-dd hh:mm:ss")),
          });
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查询角色详情（Select 用）
export function findRoleDetailBySelect(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/role/findRoleDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          var strPermissiones = "";
          for (var i = 0; i < response.data.data.permissiones.length; i++) {
              strPermissiones += response.data.data.permissiones[i].pName + "、";
          }
          strPermissiones = strPermissiones.substr(0, strPermissiones.length - 1);
          obj.strCallback(strPermissiones);
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查找所有角色
export function findRolesAll(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/role/findRoles', null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({rolesOptions: response.data.data, isFirstRequest: false});
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查找所有权限
export function findPermissonsAll(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/role/findPermissons', null)
    .then(function (response) {
        if(response.data.code == 1){
          var permissiones = new Array();
          for (var i = 0; i < response.data.data.length; i++) {
            var pName = response.data.data[i].pName;
            var id = response.data.data[i].id;

            permissiones.push({
              label: pName,
              value: id
            });
          }
          obj.setState({purviewOptions: permissiones});
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//添加、修改 权限
export function creatOrUpdatePermission(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/role/creatOrUpdatePermission', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.refs["tableView"].refreshData();
          obj.setState({addSensitiveWords: false, updateLoading: false});
          obj.props.form.resetFields();
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '权限保存成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }
    }).catch(function(err){
        obj.setState({updateLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//添加、修改 账户创建
export function createUser(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/user/createUser', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          if(response.data.data == "创建成功!"){
              obj.refs["tableView"].refreshData();
              obj.setState({addAccount: false, updateLoading: false});
              notification.success({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '账户创建成功。',
                  style: {
                    borderLeft: '4px solid rgb(23, 168, 84)'
                  }
              });
          }else {
              obj.setState({updateLoading: false});
              notification.error({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: response.data.data,
                  style: {
                    borderLeft: '4px solid rgb(240, 70, 52)'
                  }
              });
          }
        }
    }).catch(function(err){
        obj.setState({updateLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查询用户详情
export function findUserDetail(obj, id) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/user/findUserDetail/' + id, null)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({
            baseData: response.data.data.userProfile
          });
          if(response.data.data.userProfile.roles.length >= 1){
            findRoleDetailBySelect(obj, response.data.data.userProfile.roles[0]);
          }
        }
    })
}
//修改用户 角色
export function updateUserRole(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/user/updateUserRole', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({editAccountRole: false, updateRoleLoading: false});
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '角色保存成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }
    }).catch(function(err){
        obj.setState({updateRoleLoading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
