/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//家长给 工作人员的 平均分
export function replyAvgScore(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/Feedback/replyAvgScore', null)
    .then(function (response) {
        if(response.data.code == 1){
          if(response.data.data == '暂无评分'){
              obj.setState({
                replyAvgScore: 5
              });
          }else {
              obj.setState({
                replyAvgScore: response.data.data.toFixed(2)
              });
          }
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//查找登录人员的详情信息
export function findUserProfile(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/findUserProfile', null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data != undefined){
                var percent = 0;
                if(response.data.data.age != undefined && response.data.data.age > 1){
                    percent += 9.09;
                }
                if(response.data.data.email != undefined && response.data.data.email != ""){
                    percent += 9.09;
                }
                if(response.data.data.firstName != undefined && response.data.data.firstName != ""){
                    percent += 9.09;
                }
                if(response.data.data.lastName != undefined && response.data.data.lastName != ""){
                    percent += 9.09;
                }
                if(response.data.data.gender != undefined && response.data.data.gender != ""){
                    percent += 9.09;
                }
                if(response.data.data.job != undefined && response.data.data.job != ""){
                    percent += 9.09;
                }
                if(response.data.data.location != undefined && response.data.data.location != ""){
                    percent += 9.09;
                }
                if(response.data.data.overview != undefined && response.data.data.overview != ""){
                    percent += 9.09;
                }
                if(response.data.data.phoneNum != undefined && response.data.data.phoneNum != ""){
                    percent += 9.09;
                }
                if(response.data.data.position != undefined && response.data.data.position != ""){
                    percent += 9.09;
                }
                if(response.data.data.photoPic != undefined){
                    percent += 9.09;
                }
                obj.setState({serverData: response.data.data,percent: parseInt(percent.toFixed(0))})
            }
        }
    })
}
//查找家长登录次数
export function findParentsLoginTimes(obj, paramData, flap) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    var myChart = echarts.init(document.getElementById('divBars'));
    myChart.showLoading('default',{text: '加载中，请稍候...', color: '#108EE9', textColor: '#108EE9'});
    axios.post('/webAPI/dashboard/findParentsLoginTimes', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          var keys = []; var loginValues = []; var logoutValues = []; var tempLoginTotal = 0; var pieValues = [];
          var count = response.data.data.count; delete response.data.data.count; obj.setState({count: count});

          var startTime = new Date(paramData.startTime);
          var endTime = new Date(paramData.endTime);
          var days = parseInt((endTime.getTime() - startTime.getTime()) / (1000 * 60 * 60 * 24));

          for (var i = 0; i <= days; i++) {
              var temp = new Date(startTime.getTime() + (86400000*i));
              var isHaveData = false; keys.push(temp.Format("yyyy-MM-dd"));

              for (var item in response.data.data) {
                  if(item == temp.Format("yyyy-MM-dd")){
                    loginValues.push(response.data.data[item]);
                    logoutValues.push(count - response.data.data[item])
                    isHaveData = true; //keys.push(item);
                  }
              }
              if(isHaveData == false){
                  loginValues.push(0);
                  logoutValues.push(count);
              }
          }

          for (var item in response.data.data) {
              tempLoginTotal += response.data.data[item];
              pieValues.push({value: response.data.data[item], name: item});
          }
          var titleText = paramData.startTime.split(' ')[0] + " ～ " + paramData.endTime.split(' ')[0]; var dataZoomShow = false; var y2 = 20;
          if(flap == true){
              dataZoomShow = true; y2 = 65;
          }
          myChart.hideLoading();
          var barOption = {
              title:{
                  text: titleText,
                  x:'35px',
                  textStyle: {
                      color: '#54A2D5',
                      fontSize: 14,
                      fontWeight: 'normal'
                  }
              },
              dataZoom: [
                  {
                      show: dataZoomShow,
                      start: 0,
                      end: 100
                  }
              ],
              legend: {
                data:['登入','未登入'],
                selected: {
                    '登入': true,
                    '未登入': false
                }
              },
              grid:{
                  x:40,
                  x2:10,
                  y:25,
                  y2:y2
              },
              color: ['#1C85C7', '#23C6C8'],
              tooltip : {
                  // axisPointer:{
                  //   type: 'none',
                  // },
                  trigger: 'axis'
              },
              xAxis : [
                  {
                      type : 'category',
                      data : keys,
                      axisPointer: {
                          type: 'none'
                      }
                  }
              ],
              yAxis : [
                  {
                      type : 'value',
                      axisLine : {
                        lineStyle: {color: '#909090'}
                      }
                  }
              ],
              series : [
                  {
                      name:'登入',
                      type:'bar',
                      data: loginValues,
                      barMaxWidth: 40,
                      itemStyle: {
                          normal: {
                              opacity: 0.8
                          },
                          emphasis: {
                              shadowColor : '#1C85C7',
                              shadowBlur: 10,
                              opacity: 1
                          }
                      }
                  },
                  {
                      name:'未登入',
                      type:'bar',
                      data: logoutValues,
                      barMaxWidth: 40,
                      itemStyle: {
                          normal: {
                              opacity: 0.8
                          },
                          emphasis: {
                              shadowColor : '#23C6C8',
                              shadowBlur: 10,
                              opacity: 1
                          }
                      }
                  }
              ]
          };
          var pieOption = {
              title:[{
                  text: titleText,
                  x:'center',
                  textStyle: {
                      color: '#54A2D5',
                      fontSize: 14,
                      fontWeight: 'normal'
                  }
              }],
              tooltip: {
                  trigger: 'item',
                  formatter: "{a} <br/>{b} : {c} （{d}%）"
              },
              color: ['#1C85C7', '#23C6C8'],
              series : [
                  {
                      name:'登入',
                      type:'pie',
                      data: pieValues,
                      radius : '65%',
                      center: ['50%', '50%'],
                      itemStyle: {
                          emphasis: {
                              shadowBlur: 10,
                              shadowOffsetX: 0,
                              shadowColor: 'rgba(0, 0, 0, 0.5)'
                          }
                      }
                  }
              ]
          };

          // 使用刚指定的配置项和数据显示图表。
          if(obj.state.txtPieAndBar == '饼状图'){
              myChart.clear();
              myChart.setOption(barOption);
          }else {
              myChart.clear();
              myChart.setOption(pieOption);
          }
          var that = obj;
          myChart.on('click', function (params) {
            if(params.seriesType == "bar"){
                that.setState({showLoginList: true, loginList: null, unloginList: null, seriesName: params.seriesName});
                that.state.actions.findParentsByDay(that,{
                    startTime: params.name + " 00:00:00",
                    endTime: params.name + " 23:59:59"
                })
            }else if(params.seriesType == "pie"){
                if(params.seriesName == "登入"){
                    that.state.pieOption.series[0].center[0] = "25%";
                    var loginRatio = {
                        name: params.name,
                        type:'pie',
                        data: [{name:'登入', value: params.value},{name:'未登入', value: (that.state.count - params.value)}],
                        radius : '65%',
                        center: ['75%', '50%'],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                    if(that.state.pieOption.series.length <= 1){
                        that.state.pieOption.series.push(loginRatio);
                    }else {
                        that.state.pieOption.series[1] = loginRatio;
                    }
                    that.state.myChart.setOption(that.state.pieOption);
                }else {
                    that.setState({showLoginList: true, loginList: null, unloginList: null, seriesName: params.data.name});
                    that.state.actions.findParentsByDay(that,{
                        startTime: params.seriesName + " 00:00:00",
                        endTime: params.seriesName + " 23:59:59"
                    })
                }
            }
          });
          obj.setState({myChart: myChart, barOption: barOption, pieOption: pieOption, loginTotal: tempLoginTotal});
        }
    }).catch(function(err){
        myChart.showLoading('default',{text: '对不起，网络链接错误。', color: '#FFF', textColor: '#F00'});

        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
//点击柱状图，查看登录用户详情
export function findParentsByDay(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/dashboard/findParentsByDay', paramData)
    .then(function (response) {
        if(response.data.code == 1){
          obj.setState({loginList: response.data.data.loginP, unloginList: response.data.data.unLoginP});
        }
    }).catch(function(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
