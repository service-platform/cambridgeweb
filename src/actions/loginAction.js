/**
 * Created by Administrator on 2016/10/9.
 */
import { notification } from 'antd';
import {browserHistory} from 'react-router';
import * as axios from 'axios';
axios.defaults.baseURL = window.GLOBAL.baseURL;
axios.defaults.headers.post['Content-Type'] = window.GLOBAL.contentType;

//家长汇 后台登录
export function login(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    obj.setState({loading: true});
    axios.post('/webAPI/unAuth/login', paramData)
    .then(function (response) {
        obj.setState({loading: false});
        if(response.data.code == 1){
            sessionStorage.setItem("loginData", JSON.stringify(response.data.data));
            axios.defaults.headers.common['userAuth'] = response.data.data.token;
            sessionStorage.setItem('userAuth', response.data.data.token);
            var username = sessionStorage.getItem("username");
            username += "（" + ((response.data.data.userProfile.firstName == undefined)?"":response.data.data.userProfile.firstName) + " " + ((response.data.data.userProfile.lastName == undefined)?"":response.data.data.userProfile.lastName) + "）";
            sessionStorage.setItem("username", username);
            browserHistory.push("home");
        }else{
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，账户或密码错误。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    }).catch(function(err){
        obj.setState({loading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络链接错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}

//修改密码
export function updatePassword(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/updatePassword', paramData)
    .then(function (response) {
        if(response.data.code != 0){
          sessionStorage.removeItem("userAuth");
          sessionStorage.removeItem("username");
          browserHistory.push("/login");

          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '修改密码成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }else{
          obj.setState({ visible: false });
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，旧密码错误。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    })
}
//查找登录人员的详情信息
export function findUserProfile(obj) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/findUserProfile', null)
    .then(function (response) {
        if(response.data.code == 1){
            if(response.data.data != undefined){
                obj.setState({serverData: response.data.data})
                if(response.data.data.roles != undefined && response.data.data.roles.length >= 1){
                    axios.post('/webAPI/role/findRoleDetail/' + response.data.data.roles[0], null)
                    .then(function (response) {
                        if(response.data.code == 1){
                          var strPermissiones = "";
                          for (var i = 0; i < response.data.data.permissiones.length; i++) {
                              strPermissiones += response.data.data.permissiones[i].pName + "、";
                          }
                          strPermissiones = strPermissiones.substr(0, strPermissiones.length-1);
                          obj.setState({
                            roles: response.data.data.role.roleName,
                            strPermissiones: strPermissiones
                          });
                        }
                    })
                }
            }
        }
    })
}
//修改个人信息数据
export function updateUserProfile(obj, paramData) {
    if(sessionStorage.getItem("userAuth") != null){
        axios.defaults.headers.common['userAuth'] = sessionStorage.getItem('userAuth');
    }
    axios.post('/webAPI/settings/updateUserProfile', paramData)
    .then(function (response) {
        if(response.data.code != 0){
          obj.setState({ visiblePersonal: false,visibleAlert: false, modifyLoading: false });
          obj.props.form.resetFields();
          notification.success({
              placement: 'bottomRight',
              message: '提示：',
              description: '修改信息成功。',
              style: {
                borderLeft: '4px solid rgb(23, 168, 84)'
              }
          });
        }else{
          obj.setState({ visiblePersonal: false,visibleAlert: false, modifyLoading: false });
          notification.error({
              placement: 'bottomRight',
              message: '提示：',
              description: '对不起，信息修改错误。',
              style: {
                borderLeft: '4px solid rgb(240, 70, 52)'
              }
          });
        }
    }).catch(function(err){
        obj.setState({ visiblePersonal: false,visibleAlert: false, modifyLoading: false });
        notification.error({
            placement: 'bottomRight',
            message: '提示：',
            description: '对不起，网络请求错误。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    })
}
 // /webAPI  / settings/updateParentPortrait
