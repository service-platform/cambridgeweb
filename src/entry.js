/**
 *
 * @authors jun cocoa
 * @date    2016-10-21 16:42:35
 */

'use strict';
//引入样式文件
// import 'babel-polyfill';
import './styles/app.scss';
import './store/types';

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
//路由器
import {Router, browserHistory, Route, IndexRoute} from 'react-router';
//reducers 规则
import reducers from './reducers/reducers';
//路由列表 声明
import routes from './routes';
//thunk 中间件
import reduxThunk from 'redux-thunk';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);
// 引入单个页面（非嵌套页面）
import login from './pages/login/login.js';
//文章预览
import articlePreview from './pages/components/articleManager/articlePreview';
//主页面渲染入口
ReactDOM.render(
  <Provider store={store}>
      <Router history={browserHistory}>
        <Route path="login" breadcrumbName="登录"  component={login} />
        <Route path="articlePreview/:id" component={articlePreview}/>
        {routes}
      </Router>

  </Provider>
, document.getElementById('app'));
