'use strict';
import React from 'react';
import { Route, DefaultRoute, hashHistory, IndexRoute, Redirect, IndexLink} from 'react-router';

// 引入单个页面（包括嵌套的子页面）
import home from './pages/components/home.js';
import parentsList from './pages/components/parentsList.js';
import parentsDetail from './pages/components/parentsDetail';
import feedbackList from './pages/components/feedbackList.js';
import feedbackDetail from './pages/components/feedbackDetail';

import videoList from './pages/components/videoList.js';
import dashBoard from './pages/components/dashBoard.js';
import articleList from './pages/components/articleManager/articleList.js';
import carouselManage from './pages/components/articleManager/carouselManage';
import articleDesign from './pages/components/articleManager/articleDesign.js';

//设置
import changePwd from './pages/components/setting/changePwd';
import personalInfo from './pages/components/setting/personalInfo';
import sensitiveWords from './pages/components/setting/sensitiveWords';
//在线客服
import onlineCustomers from './pages/components/onlineCustomers';
//登入统计
import loginStatistics from './pages/components/statistics/loginStatistics';
//授权列表
import authorizationList from './pages/components/authorizationList';
//角色管理
import rolesList from './pages/components/purviewAndRoles/rolesList';
//权限管理
import purviewList from './pages/components/purviewAndRoles/purviewList';
//人员管理
import staffMember from './pages/components/purviewAndRoles/staffMember';
//历史记录
import historyRecord from './pages/components/setting/historyRecord';
//留言管理
import commentManage from './pages/components/articleManager/commentManage';
//报名列表
import registerList from './pages/components/registerInfo/registerList';
//报名详情
import registerDetail from './pages/components/registerInfo/registerDetail';
// 配置路由
export default (
    <Route path="/" component={home} breadcrumbName="主页">
        <IndexRoute component={dashBoard} breadcrumbName="仪表盘"/>

        <Route path="home" component={dashBoard} breadcrumbName="仪表盘"/>
        <Route path="parentsList" breadcrumbName="家长列表" component={parentsList} >
            <Route path="parentsDetail/:id" breadcrumbName="家长详情" component={parentsDetail} />
        </Route>

        <Route path="feedbackList" breadcrumbName="反馈列表" component={feedbackList} >
            <Route path="feedbackDetail/:id" breadcrumbName="反馈详情" component={feedbackDetail} />
        </Route>
        <Route path="authorizationList" breadcrumbName="授权列表" component={authorizationList} />
        <Route path="loginStatistics" breadcrumbName="登入统计" component={loginStatistics} />

        <Route path="videoList" breadcrumbName="视频" component={videoList} />
        <Route path="articleList/:isTravel" breadcrumbName="文章列表" component={articleList}/>
        <Route path="articleDesign/:id/:isTravel" breadcrumbName="文章设计" component={articleDesign}>
            <Route path="registerList/:articleId" breadcrumbName="报名列表" component={registerList} >
                <Route path="registerDetail/:pId" breadcrumbName="报名详情" component={registerDetail} />
            </Route>
        </Route>
        <Route path="carouselManage" breadcrumbName="轮播管理" component={carouselManage}/>
        <Route path="commentManage" breadcrumbName="留言管理" component={commentManage}/>

        <Route path="staffMember" breadcrumbName="工作人员管理" component={staffMember}/>
        <Route path="rolesList" breadcrumbName="角色管理" component={rolesList}/>
        <Route path="purviewList" breadcrumbName="权限管理" component={purviewList}/>
        <Route path="changePwd" breadcrumbName="修改密码" component={changePwd} />
        <Route path="historyRecord" breadcrumbName="历史记录" component={historyRecord}/>
        <Route path="personalInfo" breadcrumbName="个人信息" component={personalInfo} />
        <Route path="sensitiveWords" breadcrumbName="敏感词" component={sensitiveWords} />
        <Route path="onlineCustomers" breadcrumbName="在线客服" component={onlineCustomers} />
        <Redirect from="*" to="/login"/>
    </Route>
);
