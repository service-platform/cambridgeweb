import React from 'react';
import {Card, Icon, Input, Button, Tooltip, notification} from 'antd';
import {browserHistory} from 'react-router';
import TableView from './../TableView';

export default class registerList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findParents: GLOBAL.baseURL + "/webAPI/article/findEnrollsByAId",
            totalPageNum: 0,
            ServiceData: [],
            searchObject: { aId: this.props.routeParams.articleId },
        };
    }
    serviceError(err){
        notification.error({
            placement: 'bottomRight',
            message: '提示你：',
            description: '对不起，网络错误，请稍候重试。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            ServiceData: data
        });
    }
    routerParentsDetail(pId, enrollTime, enrollphone){
        var paramData = {
          registrationPhone: enrollphone,
          registrationTime: new Date(enrollTime).Format("yyyy-MM-dd hh:mm:ss")
        };
        sessionStorage.setItem("registerInfo", JSON.stringify(paramData));
        browserHistory.push("/articleDesign/" + this.props.params.id + "/" + this.props.params.isTravel + "/registerList/" + this.props.routeParams.articleId + "/registerDetail/" + pId);
    }
    renderItem(item, index) {
        var imgHeader = null;
        if (item.avatarUrl == undefined || item.avatarUrl == "") {
            imgHeader = <img className="userHead" src='/src/styles/images/defaultHeader.jpg'/>
        } else {
            imgHeader = <img className="userHead" src={item.avatarUrl} title={item.nickName}/>
        }
        return (
              <Card key={index} onClick={this.routerParentsDetail.bind(this, item.pId, item.enrollTime, item.enrollphone)} className="divItem">
                  <div className="divUserHead">
                      {imgHeader}
                  </div>
                  <div className="userInfo" style={{marginTop: '-3px'}}>
                      <div className="divContent">
                          <span>{'姓名：' + ((item.nickName == undefined)?"":item.nickName)}</span>
                          <span>{'，性别：' + ((item.gender==1)?"男":"女")}</span>
                      </div>
                      <div className="divContent">
                          <span>{'省份：' + ((item.province == undefined)?"":item.province)}</span>
                          <span>{'，城市：' + ((item.city == undefined)?"":item.city)}</span>
                      </div>
                      <p className="pOverview">
                        <span>报名电话：<span style={{ color: 'red'}}>{((item.enrollphone == undefined)?"":item.enrollphone)}</span>{'，' + ((item.relationship == undefined)?"":item.relationship)}</span>
                      </p>
                      <p className="pOverview">
                        <span>报名时间：{((item.enrollTime == undefined)?"": new Date(item.enrollTime).Format("yyyy-MM-dd hh:mm:ss"))}</span>
                      </p>
                  </div>
              </Card>
        );
    }
    render() {
        if(this.props.params.pId == undefined){
            var serverList = null;
            if(this.state.ServiceData.length != 0){
                serverList = <div style={{marginBottom: '20px'}}>
                  {
                      this.state.ServiceData.map((item, index) => {
                          return this.renderItem(item, index);
                      })
                  }
                </div>
            }else{
                serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
            }
            return (
                <div>
                    {serverList}
                    <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                         isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                         isService={true} serviceError={this.serviceError.bind(this)} searchObject={this.state.searchObject} pageNum={10}
                         url={this.state.findParents} method="post"/>
                </div>
            )
        }else {
          return(this.props.children)
        }
    }
}
