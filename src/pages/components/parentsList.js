import React from 'react';
import {Card, Icon, Input, Button, Tooltip, notification} from 'antd';
import {browserHistory} from 'react-router';
import TableView from './TableView';

export default class parentsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findParents: GLOBAL.baseURL + "/webAPI/parent/findParents",
            totalPageNum: 0,
            ServiceData: [],
            searchObject: {},
            nickName: '',
            phone: '',
            loading: false
        };
    }
    btnSearch(){
        if(this.state.nickName != ''){
          this.state.searchObject.nickName = this.state.nickName;
        }else {
          delete this.state.searchObject.nickName;
        }
        if(this.state.phone != ''){
          this.state.searchObject.phone = this.state.phone;
        }else {
          delete this.state.searchObject.phone;
        }
        this.setState({loading: true});
        this.refs["tableView"].refreshData();
    }
    inputText(flag, e){
        if(flag == 'nickName'){
          this.setState({nickName: e.target.value});
        }else if (flag == 'phone') {
          this.setState({phone: e.target.value});
        }
    }
    serviceError(err){
        this.setState({loading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示你：',
            description: '对不起，网络错误，请稍候重试。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            loading: false,
            ServiceData: data
        });
    }
    routerParentsDetail(id){
      browserHistory.push("/parentsList/parentsDetail/" + id);
    }
    renderItem(item, index) {
        var imgHeader = null;
        if (item.avatarUrl == undefined || item.avatarUrl == "") {
            imgHeader = <img className="userHead" src='/src/styles/images/defaultHeader.jpg'/>
        } else {
            imgHeader = <img className="userHead" src={item.avatarUrl} title={item.nickName}/>
        }
        return (
              <Card key={index} onClick={this.routerParentsDetail.bind(this, item.id)} className="divItem">
                  <div className="divUserHead">
                      {imgHeader}
                  </div>
                  <div className="userInfo" style={{marginTop: '-3px'}}>
                      <div className="divContent">
                          <span>{'姓名：' + ((item.nickName == undefined)?"":item.nickName)}</span>
                          <span>{'，性别：' + ((item.gender==1)?"男":"女")}</span>
                      </div>
                      <div className="divContent">
                          <span>{'省份：' + ((item.province == undefined)?"":item.province)}</span>
                          <span>{'，城市：' + ((item.city == undefined)?"":item.city)}</span>
                      </div>
                      <p className="pOverview">
                        <span>{'关系：' + ((item.relationship == undefined)?"":item.relationship)}</span>
                      </p>
                      <p className="pOverview">
                        <span>{'上次登录时间：' + ((item.lastLoginTime == undefined)?"": new Date(item.lastLoginTime).Format("yyyy-MM-dd hh:mm:ss"))}</span>
                      </p>
                  </div>
              </Card>
        );
    }
    render() {
        if(this.props.params.id == undefined){
            var serverList = null;
            if(this.state.ServiceData.length != 0){
                serverList = <div style={{marginBottom: '20px'}}>
                  {
                      this.state.ServiceData.map((item, index) => {
                          return this.renderItem(item, index);
                      })
                  }
                </div>
            }else{
                serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
            }
            return (
                <div>
                    <div className="divAdvancedSearch">
                      姓名：<Tooltip trigger="click" placement="topLeft" title="区分大小写"><Input value={this.state.nickName} placeholder="请输入姓名.." style={{width: '126px', marginRight: '10px', marginBottom: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'nickName')}/></Tooltip>
                      电话：<Input value={this.state.phone} placeholder="请输入电话.." style={{width: '126px', marginRight: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'phone')}/>
                      <Button loading={this.state.loading} icon="search" type="primary" onClick={this.btnSearch.bind(this)}>查询</Button>
                    </div>
                    {serverList}
                    <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                               isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                               isService={true} serviceError={this.serviceError.bind(this)} searchObject={this.state.searchObject} pageNum={10}
                               url={this.state.findParents} method="post" sort="joinTime" dir="DESC"/>

                </div>
            )
        }else{
            return(this.props.children)
        }
    }
}
