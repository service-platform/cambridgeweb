import React from 'react';
import { Card, Button, Icon, message, Spin, Modal } from 'antd';
import {browserHistory} from 'react-router';
import TableView from './TableView';
import * as actions from './../../actions/videoAction';

export default class videoList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          findVedioes: GLOBAL.baseURL + "/webAPI/video/videoes",
          totalPageNum: 0,
          ServiceData: [],
          searchObject: {},
          visibleVideo: false,
          videoData: {},
          spinLoading: false
        }
    }
    componentDidMount(){
        this.setState({
            spinLoading: true
        });
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            spinLoading: true,
            ServiceData: data
        });
    }
    routerParentsDetail(id){
        actions.videoDetail(this, id);
    }
    onVideoLoaded(index){
        this.refs["Spin_" + index].setState({spinning: false});
    }
    onVideoDetailLoaded(){
        this.refs["Spin_Detail"].setState({spinning: false});
    }
    renderItem(item, index) {
        return (
            <Card key={index} className="divItem" onClick={this.routerParentsDetail.bind(this, item.id)}>
              <Spin spinning={this.state.spinLoading} ref={"Spin_" + index}>
                <video src={item.videoUrl} controls="controls" className="videoTag" onLoadedData={this.onVideoLoaded.bind(this, index)}>
                    您的浏览器不支持 video 标签。
                </video>
              </Spin>
            </Card>
        );
    }
    render() {
        var btnClose=<Button icon="close" type="danger" onClick={()=>{this.setState({visibleVideo: false, videoData:{}})}}>关 闭</Button>
        var serverList = null;
        if(this.state.ServiceData.length != 0){
            serverList = <div style={{marginBottom: '20px', minHeight: '294px'}}>
              {
                  this.state.ServiceData.map((item, index) => {
                      return this.renderItem(item, index);
                  })
              }
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }
        return (
            <div>
                {serverList}
                <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                     isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                     isService={true} searchObject={this.state.searchObject} pageNum={10}
                     url={this.state.findVedioes} method="post"/>

               <Modal title={((this.state.videoData.author == undefined)?"视频详情":this.state.videoData.author)} visible={this.state.visibleVideo} width="355px"
                  footer={btnClose} onCancel={()=>{this.setState({visibleVideo: false, videoData:{}})}} maskClosable={false}>
                  <Spin ref={"Spin_Detail"}>
                    <video src={this.state.videoData.videoUrl} controls="controls" style={{width: '326px'}} onLoadedData={this.onVideoDetailLoaded.bind(this)}>
                        您的浏览器不支持 video 标签。
                    </video>
                  </Spin>
                  <div>{"作者：" + ((this.state.videoData.author == undefined)?"":this.state.videoData.author)}</div>
                  <div className="divVideoContent" dangerouslySetInnerHTML={{__html: ((this.state.videoData.content == undefined)?"":this.state.videoData.content)}}></div>
                  <div>{"播放量：" + ((this.state.videoData.playCount == undefined)?"":this.state.videoData.playCount)}</div>
                  <div>{"时间：" + ((this.state.videoData.commitTime == undefined)?"":this.state.videoData.commitTime)}</div>
                </Modal>
            </div>
        )
    }
}
