import React from 'react';
import {Card, Button, message, notification, Modal, Input, Tooltip, Upload, Icon, Popconfirm, Spin, Switch, Select, Popover, Checkbox} from 'antd';
import {browserHistory} from 'react-router';
import * as actions from './../../../actions/videoAction';
import TableView from './../TableView';
const { Option, OptGroup } = Select;
const { TextArea } = Input;

const typeData = ['热门活动', '留学须知', '学生风采', '家长必读','旅游项目','大学预备语言和文化（CPLC）线上课程', '大学预备语言和文化（CPLC）线下课程', '私教辅导'];
const subTypeData = {
  '热门活动': [{key: '线上活动',value: "1"}, {key: '线下活动',value: "2"},{key: '剑桥快报',value: "3"}],
  '留学须知': [{key: '学业篇',value: "4"}, {key: '法律篇',value: "5"}, {key: '文化篇',value: "6"}, {key: '生活篇',value: "7"}],
  '学生风采': [{key: '每周播报',value: "8"}, {key: '美高捷报',value: "9"}],
  '家长必读': [{key: '在线培训',value: "10"}, {key: '避雷指南',value: "11"}, {key: 'CIIE洞察',value: "12"}],
  '旅游项目': [{key: '走遍美国',value: "13"}, {key: '假期游学',value: "14"}, {key: '访校之旅',value: "30"}, {key: '其他服务',value: "15"}],
  '大学预备语言和文化（CPLC）线上课程': [{key: '课程介绍',value: "16"}, {key: '教师简介',value: "17"}, {key: '学生成功案例',value: "18"}, {key: '常见问题与解答',value: "19"}],
  '大学预备语言和文化（CPLC）线下课程': [{key: '课程介绍',value: "20"}, {key: '教师简介',value: "21"}, {key: '学生成功案例',value: "22"}, {key: '常见问题与解答',value: "23"}],
  '私教辅导': [{key: '学科辅导介绍',value: "24"}, {key: '标准化考试备考 （托福，SAT, ACT）',value: "25"}, {key: '一对一辅导介绍',value: "26"},{key: '教师简介',value: "27"}, {key: '成功案例',value: "28"}, {key: '常见问题与解答', value: "29"}]
};
const menus = [
    'source',
    '|',
    'bold',
    'underline',
    'italic',
    'strikethrough',
    'eraser',
    'forecolor',
    // 'bgcolor',
    '|',
    'link',
    'unlink',
    '|',
    'quote',
    'fontfamily',
    'fontsize',
    'unorderlist',
    'alignleft',
    'aligncenter',
    'alignright',
    // '|',
    // 'lineheight',
    // 'indent',
    // 'table',
    // 'emotion',
    '|',
    'undo',
    'redo',
    'fullscreen'
];
export default class designArticle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findArticle: GLOBAL.baseURL + "/webAPI/article/findArticles",
            totalPageNum: 0,
            articleListData: [],
            searchObject: {
                app: "app",
                type: "1"
            },
            updateLoading: false,
            tempArticleData: null,
            pastDescription: "",
            relatedArticleTip: "加载中...",
            deleteReviewOfThePast: false,
            isAddOrEdite: 'Add',
            editeParamData: null,
            width: '500px',

            id: null,
            coverId: null,
            typeData: subTypeData[typeData[0]],
            typeIndex: 0,
            editTypeIndex: 0,
            subTypeData: subTypeData[typeData[0]][0],
            editSubTypeData: subTypeData[typeData[0]][0],
            updateVisible: false,
            updateThumbnailVisible: false,
            updateAudioVisible: false,
            updateImageVisible: false,
            updateBaseVisible: false,
            baseData: {
                title: "",
                summary: "",
                type: this.props.selectType
            },
            videoData: {
                author: "",
                content: "",
                type: this.props.selectType
            },
            audioData: {
                author: "",
                content: "",
                type: this.props.selectType
            },
            imageData: {
                content: "",
                type: this.props.selectType,
                order: 1
            },
            serviceVideoData: null,
            videoLoading: true,
            audioLoading: false,
            deleteVideoLoading: false,
            deleteAudioLoading: false,
            deleteImageLoading: false,
            imgTextEditor: null,
            imgVideoEditor: null,
            updateBaseLoading: false,
            updateThumbnailLoading: false,
            updateImageLoading: false,
            updateVideoLoading: false,
            updateVideoProgress: '上传中，视频超过 5M，请耐心等待...',
            updateAudioLoading: false,
            updateAudioProgress: '上传中，音频超过 5M，请耐心等待...',
            fileVerify: false,
            updateBaseProgress: '上传图片中...',
            updateImageAndTextProgress: '上传中...',
            updateCoverPicProgress: '上传封面图...',
            showAdminReplay: false,
            replayCommentName: '',
            commentItem: null,
            replyContent: "",
            tipImageTextSpinning: '删除中，请等待...',
            sortIsEnabled: false,
            orderUpdating: false,
            showCommentDetails: false,
            commentDetail: null,
            commentStatus: "（状态：加载中）",
            showArticleThumbsUps: false,
            articleThumbsUpsDetail: null,
            isAddImageAndTextLock: true,
            showPreviewArticle: false,

            showParentsList: false,
            parentsListData: [],
            tempParentsData: [],
            isSendNotifNext: true,
            isPreview: false,
            sendingNotifications: false,
            labelTimer: "12月28日",
            tempInfoData:{
                title: '',
                type: '',
                summary: '',
                Remark: ''
            }
        };
    }
    componentDidMount(){
        if(this.props.id != null){
            this.onRefreshData();
        }else {
            this.setState({isAddImageAndTextLock: false });
        }
        var selectType = parseInt(this.props.selectType);

        if(selectType >= 1 && selectType <= 3){
            this.setState({typeIndex: 0, editTypeIndex: 0, subTypeData: subTypeData[typeData[0]][selectType-1], editSubTypeData: subTypeData[typeData[0]][selectType-1]});
        }else if(selectType >= 4 && selectType <= 7){
            this.setState({typeIndex: 1, editTypeIndex: 1, subTypeData: subTypeData[typeData[1]][selectType-4], editSubTypeData: subTypeData[typeData[1]][selectType-4]});
        }else if(selectType >= 8 && selectType <= 9){
            this.setState({typeIndex: 2, editTypeIndex: 2, subTypeData: subTypeData[typeData[2]][selectType-8], editSubTypeData: subTypeData[typeData[2]][selectType-8]});
        }else if(selectType >= 10 && selectType <= 12){
            this.setState({typeIndex: 3, editTypeIndex: 3, subTypeData: subTypeData[typeData[3]][selectType-10], editSubTypeData: subTypeData[typeData[3]][selectType-10]});
        }else if(selectType >= 13 && selectType <= 15){
            this.setState({typeIndex: 4, editTypeIndex: 4, subTypeData: subTypeData[typeData[4]][selectType-13], editSubTypeData: subTypeData[typeData[4]][selectType-13]});
        }else if(selectType >= 16 && selectType <= 19){
            this.setState({typeIndex: 5, editTypeIndex: 5, subTypeData: subTypeData[typeData[5]][selectType-16], editSubTypeData: subTypeData[typeData[5]][selectType-16]});
        }else if(selectType >= 20 && selectType <= 23){
            this.setState({typeIndex: 6, editTypeIndex: 6, subTypeData: subTypeData[typeData[6]][selectType-20], editSubTypeData: subTypeData[typeData[6]][selectType-20]});
        }else if(selectType >= 24 && selectType <= 29){
            this.setState({typeIndex: 7, editTypeIndex: 7, subTypeData: subTypeData[typeData[7]][selectType-24], editSubTypeData: subTypeData[typeData[7]][selectType-24]});
        }else if(selectType == 30){
            this.setState({typeIndex: 4, editTypeIndex: 4, subTypeData: subTypeData[typeData[4]][2], editSubTypeData: subTypeData[typeData[4]][2]});
        }

        var that = this;
        window.onresize = function(){
          try {
              if($(".center-modal").length > 0){
                  var scrollbarHeight = that.getSizeWithScrollbar();
                  $(".divFlexListMini").css("maxHeight",(scrollbarHeight.height-213)+"px")
                  $(".center-modal").find(".ant-modal-content").css("height", scrollbarHeight.height + "px");
              }
              $("#leftMenu").find(".ant-layout-sider-children").css("height", (document.body.clientHeight-48)+"px");
              $("#leftMenu").find(".ant-layout-sider-children").mCustomScrollbar({theme:"minimal"});

              if(document.body.clientWidth <= 952){
                  that.setState({width: (document.body.clientWidth - 460) + 'px'});
              }else {
                  that.setState({width: '500px'});
              }
          } catch (e) {
              console.log(e);
          }
        }
    }
    onTypeChangeAssociation(value){
        this.setState({
            typeData: subTypeData[value],
            subTypeData: subTypeData[value][0],
        });
    }
    onTypeChange(value){
        var typeIndex = 0;
        if(value == '热门活动'){
            typeIndex = 0;
            this.state.baseData.type = "1";
        }else if(value == '留学须知'){
            typeIndex = 1;
            this.state.baseData.type = "4";
        }else if(value == '学生风采'){
            typeIndex = 2;
            this.state.baseData.type = "8";
        }else if(value == '家长必读'){
            typeIndex = 3;
            this.state.baseData.type = "10";
        }else if(value == '旅游项目'){
            typeIndex = 4;
            this.state.baseData.type = "13";
        }else if(value == '大学预备语言和文化（CPLC）线上课程'){
            typeIndex = 5;
            this.state.baseData.type = "16";
        }else if(value == '大学预备语言和文化（CPLC）线下课程'){
            typeIndex = 6;
            this.state.baseData.type = "20";
        }else if(value == '私教辅导'){
            typeIndex = 7;
            this.state.baseData.type = "24";
        }
        this.setState({
          editTypeIndex: typeIndex,
          editSubTypeData: subTypeData[value][0],
        });
    }
    onSubTypeChange(value){
        this.state.baseData.type = value.key;

        var valueType = '热门活动';
        if(this.state.editTypeIndex == 0){
            valueType = '热门活动';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-1]});
        }else if(this.state.editTypeIndex == 1){
            valueType = '留学须知';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-4]});
        }else if(this.state.editTypeIndex == 2){
            valueType = '学生风采';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-8]});
        }else if(this.state.editTypeIndex == 3){
            valueType = '家长必读';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-10]});
        }else if(this.state.editTypeIndex == 4){
            valueType = '旅游项目';
            if(value.key == "30"){
                this.setState({editSubTypeData: subTypeData[valueType][2]});
            }else {
                this.setState({editSubTypeData: subTypeData[valueType][value.key-13]});
            }
        }else if(this.state.editTypeIndex == 5){
            valueType = '大学预备语言和文化（CPLC）线上课程';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-16]});
        }else if(this.state.editTypeIndex == 6){
            valueType = '大学预备语言和文化（CPLC）线下课程';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-20]});
        }else if(this.state.editTypeIndex == 7){
            valueType = '私教辅导';
            this.setState({editSubTypeData: subTypeData[valueType][value.key-24]});
        }
    }
    onRefreshData(){
        if(this.props.id == null){
            if(this.state.id != null){
                this.state.baseData.id = this.state.id;
                this.state.videoData.id = this.state.id;
                this.state.imageData.id = this.state.id;
                actions.findArticleDetial(this, this.state.id);
            }
        }else{
            if(this.props.id != "add"){
                this.state.id = this.props.id;
            }
            this.state.baseData.id = this.state.id;
            this.state.videoData.id = this.state.id;
            this.state.imageData.id = this.state.id;
            actions.findArticleDetial(this, this.state.id);
        }
    }
    onVideoLoaded(){
        this.setState({videoLoading: false});
    }
    onPlay(){
        this.setState({audioLoading: true});
    }
    onAudioLoaded(){
        this.setState({audioLoading: false});
    }
    btnShowAudio(value){
        if(value == true){
          this.setState({updateAudioVisible: value});
        }else if(value == false){
          this.setState({updateAudioVisible: value, updateAudioLoading: false});
        }
    }
    btnShowVideo(value){
        if(value == true){
            if(this.state.imgVideoEditor == null){
                var that = this;
                setTimeout(function(){
                  that.state.imgVideoEditor = new wangEditor('divVideo');
                  that.state.imgVideoEditor.config.menus = menus;
                  that.state.imgVideoEditor.onchange = function () {
                      that.setState({
                        videoData: {
                            ...that.state.videoData,
                            content: this.$txt.html()
                        }
                      });
                  };
                  that.state.imgVideoEditor.create();
                  that.state.imgVideoEditor.$txt.html('<p>请输入内容</p>');
                }, 400);
            }else{
                this.state.imgVideoEditor.$txt.html('<p>请输入内容</p>');
            }
        }else{
          this.setState({updateVideoLoading: value});
        }
        this.setState({updateVisible: value});
    }
    btnShowBaseInfo(value){
        if(value == false){
            this.setState({updateBaseLoading: value});
        }
        this.setState({updateBaseVisible: value});
    }
    btnShowImage(value){
        if(value == true){
            setTimeout(function(){
              $("#imgImageAndText").attr("src", "/src/styles/images/default.jpg");
            },500);
            if(this.state.imgTextEditor == null){
                var that = this;
                setTimeout(function(){
                  wangEditor.config.printLog = false;
                  that.state.imgTextEditor = new wangEditor('divImgText');
                  that.state.imgTextEditor.config.menus = menus;
                  that.state.imgTextEditor.onchange = function () {
                      that.setState({
                        imageData: {
                            ...that.state.imageData,
                            content: this.$txt.html()
                        }
                      });
                  };
                  that.state.imgTextEditor.create();
                  that.state.imgTextEditor.$txt.html('<p>请输入内容</p>');
                }, 400);
            }else{
                this.state.imgTextEditor.$txt.html('<p>请输入内容</p>');
            }
            this.onRefreshData(); //同步一下数据
        }else{
            $("#imgImageAndText").attr("src", "/src/styles/images/default.jpg");
            this.setState({updateImageLoading: value});
        }

        if(this.state.id == null){
            this.setState({updateImageVisible: value});
        }else{
            this.setState({
              updateImageVisible: value,
              imageData: {
                  id: this.state.id,
                  content: "",
                  type: this.props.selectType,
                  order: this.state.imageData.order
              }
            });
        }
    }
    onChangeBaseInfo(e, type){
        if(type == "title"){
            this.setState({
              baseData: {
                  ...this.state.baseData,
                  title: e.target.value
              }
            });
        }else if(type == "summary"){
            this.setState({
              baseData: {
                  ...this.state.baseData,
                  summary: e.target.value
              }
            });
        }
    }
    onChangeText(e){
        this.setState({
          videoData: {
              ...this.state.videoData,
              author: e.target.value
          }
        });
    }
    onChangeAudioText(e, flap){
        if(flap == 'title'){
            this.setState({
              audioData: {
                  ...this.state.audioData,
                  title: e.target.value
              }
            });
        }else if(flap == 'author'){
            this.setState({
              audioData: {
                  ...this.state.audioData,
                  author: e.target.value
              }
            });
        }else if(flap == 'content'){
            this.setState({
              audioData: {
                  ...this.state.audioData,
                  content: e.target.value
              }
            });
        }
    }
    verifyMsgAndImage(e){
        if($("#basePic")[0].files.length >= 1){
            var file = $("#basePic")[0].files[0];
            if (!(file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif' || file.type === 'image/bmp')) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '您只能上传 JPG、PNG、GIF 和 BMP 文件！',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              return false;
            }
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '照片必须小于 2MB!',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              return false;
            }
            this.getImgSize1($("#basePic")[0], "imgArticleThumbnail");
            return true;
        }
        return true;
    }
    beforeUploadImage(e) {
        if($("#filePic")[0].files.length >= 1){
            var file = $("#filePic")[0].files[0];
            if (!(file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif' || file.type === 'image/bmp')) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '您只能上传 JPG、PNG、GIF 和 BMP 文件！',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              return false;
            }
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '照片必须小于 2MB!',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              return false;
            }
            this.getImgSize1($("#filePic")[0], "imgImageAndText");
            return true;
        }
        return true;
    }
    beforeUploadThumbnail(e){
      if($("#fileThumbnail")[0].files.length >= 1){
          var file = $("#fileThumbnail")[0].files[0];
          if (!(file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif' || file.type === 'image/bmp')) {
            notification.warn({
                placement: 'bottomRight',
                message: '提示：',
                description: '您只能上传 JPG、PNG、GIF 和 BMP 文件！',
                style: {
                  borderLeft: '4px solid rgb(255, 191, 0)'
                }
            });
            this.state.fileVerify = false;
            return;
          }
          this.getImgSize($("#fileThumbnail")[0]);
      }
    }
    beforeUploadAudio(e) {
        if($("#fileAudio")[0].files.length >= 1){
            var file = $("#fileAudio")[0].files[0];
            if (!(file.type === 'audio/mp3' || file.type === 'audio/mpeg')) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '您只能上传 MP3 音频文件！',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              return false;
            }
            return true;
        }
        return true;
    }
    beforeUploadVideo(e) {
        if($("#fileVideo")[0].files.length >= 1){
            var file = $("#fileVideo")[0].files[0];
            if (!(file.type === 'audio/ogg' || file.type === 'video/mp4' || file.type === 'video/webm')) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '您只能上传 MP4、OGG 和 WebM 文件！',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              return false;
            }
            const isLt2M = (file.size / 1024 / 1024) <= 30;
            if (!isLt2M) {
              Modal.warning({
                title: '提示：',
                content: '视频必须小于 30 MB!',
              });
              return false;
            }
            return true;
        }
        return true;
    }
    btnModifyAudio (){
        this.setState({
            updateAudioVisible: true,
            audioData: {
                id: this.state.id,
                title:  this.state.serviceVideoData.audio.title,
                author:  this.state.serviceVideoData.audio.author,
                content: this.state.serviceVideoData.audio.content,
                type: this.props.selectType
            }
        });
    }
    btnModifyVideo (){
        this.setState({
            updateVisible: true,
            videoData: {
                id: this.state.id,
                author:  this.state.serviceVideoData.video.author,
                content: this.state.serviceVideoData.video.content,
                type: this.props.selectType
            }
        });

        if(this.state.imgVideoEditor == null){
            var that = this;
            setTimeout(function(){
              that.state.imgVideoEditor = new wangEditor('divVideo');
              that.state.imgVideoEditor.config.menus = menus;
              that.state.imgVideoEditor.onchange = function () {
                  that.setState({
                    videoData: {
                        ...that.state.videoData,
                        content: this.$txt.html()
                    }
                  });
              };
              that.state.imgVideoEditor.create();
              that.state.imgVideoEditor.$txt.html(that.state.serviceVideoData.video.content);
            }, 400);
        }else{
            this.state.imgVideoEditor.$txt.html(this.state.serviceVideoData.video.content);
        }
    }
    btnDeleteVideo(){
        this.setState({deleteVideoLoading: true});

        actions.removeArticleVideo(this, {
            id: this.state.id,
            videoUrl: this.state.serviceVideoData.video.videoUrl
        });
    }
    btnDeleteAudio(){
        this.setState({deleteAudioLoading: true});
        actions.removeArticleAudio(this, {
            id: this.state.id,
            audioUrl: this.state.serviceVideoData.audio.audioUrl
        });
    }
    btnRemoveReviewOfThePast(id){
        this.setState({deleteReviewOfThePast: true});
        actions.removeReviewOfThePast(this, id);
    }
    btnAddVideo(){
        if(this.state.id == null){
            this.setState({
                videoData: {
                    author: "",
                    content: "",
                    type: this.props.selectType
                }
            });
        }else{
            this.setState({
                videoData: {
                    id: this.state.id,
                    author: "",
                    content: "",
                    type: this.props.selectType
                }
            });
        }
        this.btnShowVideo(true);
    }
    btnAddAudio(){
        if(this.state.id == null){
            this.setState({
                audioData: {
                    author: "",
                    content: "",
                    type: this.props.selectType
                }
            });
        }else{
            this.setState({
                audioData: {
                    id: this.state.id,
                    author: "",
                    content: "",
                    type: this.props.selectType
                }
            });
        }
        this.btnShowAudio(true);
    }
    btnDeleteImage(itId, picUrl, index){
        this.refs["imageText_" + index].setState({spinning: true});

        actions.removeImageText(this, {
            id: this.state.id,
            itId: itId,
            pic: picUrl
        });
    }
    btnOnlyDeleteImage(id, index){
        this.refs["imageText_" + index].setState({spinning: true});

        actions.removeImageTextPic(this, id, index);
    }
    btnModifyImage(itId, order, content, item){
        this.setState({
            updateImageVisible: true,
            imageData: {
                id: this.state.id,
                itId: itId,
                order: order,
                content: content,
                type: this.props.selectType
            }
        });
        setTimeout(function(){
            if(item.pic == undefined || item.pic == ""){
                $("#imgImageAndText").attr("src", "/src/styles/images/default.jpg");
            }else {
                $("#imgImageAndText").attr("src", item.pic);
            }
        },500);
        if(this.state.imgTextEditor == null){
            var that = this;
            setTimeout(function(){
                wangEditor.config.printLog = false;
                that.state.imgTextEditor = new wangEditor('divImgText');
                that.state.imgTextEditor.config.menus = menus;
                that.state.imgTextEditor.onchange = function () {
                    that.setState({
                      imageData: {
                          ...that.state.imageData,
                          content: this.$txt.html()
                      }
                    });
                };
                that.state.imgTextEditor.create();
                that.state.imgTextEditor.$txt.html(content);
            }, 400);
        }else{
            this.state.imgTextEditor.$txt.html(content);
        }
    }
    btnModifyBase(article){
        this.setState({
            updateBaseVisible: true,
            baseData: {
                id: this.state.id,
                title: article.title,
                summary: article.summary,
                type: ((this.state.baseData.type == undefined)?this.props.selectType:this.state.baseData.type)
            },
        });
        setTimeout(function(){
          $("#imgArticleThumbnail").attr("src", article.coverPicture);
        },500);

        var selectType = parseInt(((this.state.baseData.type == undefined)?this.props.selectType:this.state.baseData.type));
        if(selectType >= 1 && selectType <= 3){
            this.setState({editTypeIndex: 0, editSubTypeData: subTypeData[typeData[0]][selectType-1]});
        }else if(selectType >= 4 && selectType <= 7){
            this.setState({editTypeIndex: 1, editSubTypeData: subTypeData[typeData[1]][selectType-4]});
        }else if(selectType >= 8 && selectType <= 9){
            this.setState({editTypeIndex: 2, editSubTypeData: subTypeData[typeData[2]][selectType-8]});
        }else if(selectType >= 10 && selectType <= 12){
            this.setState({editTypeIndex: 3, editSubTypeData: subTypeData[typeData[3]][selectType-10]});
        }else if(selectType >= 13 && selectType <= 15){
            this.setState({editTypeIndex: 4, editSubTypeData: subTypeData[typeData[4]][selectType-13]});
        }else if(selectType >= 16 && selectType <= 19){
            this.setState({editTypeIndex: 5, editSubTypeData: subTypeData[typeData[5]][selectType-16]});
        }else if(selectType >= 20 && selectType <= 23){
            this.setState({editTypeIndex: 6, editSubTypeData: subTypeData[typeData[6]][selectType-20]});
        }else if(selectType >= 24 && selectType <= 29){
            this.setState({editTypeIndex: 7, editSubTypeData: subTypeData[typeData[7]][selectType-24]});
        }else if(selectType == 30){
            this.setState({editTypeIndex: 4, editSubTypeData: subTypeData[typeData[4]][2]});
        }
    }
    setArticleStatus(val){
        if(val == true){
            if(confirm("是否确定发布该文章?")){
              actions.changeArticleStatus(this, {
                  id: this.state.id,
                  status: 'active'
              });
            }else{
              return false;
            }
        }else{
            if(confirm("是否确认要禁用该文章？")){
              actions.changeArticleStatus(this, {
                  id: this.state.id,
                  status: 'inactive'
              });
            }else{
              return false;
            }
        }
    }
    onImgError(imgUrl){
        this.setState({...this.state});
    }
    getHostNode(internalInstance) {
        return internalInstance.getHostNode();
    }
    btnSaveRichText(){
        if(/<([\w]+):([\w]+)>/.test(this.state.imgTextEditor.$txt.html()) == false){
            if(this.beforeUploadImage() && confirm("你确定要保存：图文信息 吗？")){
                this.setState({updateImageLoading: true});
                //获取图文，最大 order，在 (order+1)
                if(this.state.imageData.itId == undefined && this.state.serviceVideoData != null){
                  if(this.state.serviceVideoData.imageTextList == undefined){
                      this.state.imageData.order = 1;
                  }else{
                      var order = 1;
                      for (var i = 0; i < this.state.serviceVideoData.imageTextList.length; i++) {
                          var tempOrder = parseInt(this.state.serviceVideoData.imageTextList[i].order);
                          if(tempOrder > order){
                              order = tempOrder;
                          }
                      }
                      this.state.imageData.order = (order + 1);
                  }
                }
                if ($("#filePic")[0].files.length >= 1){
                    this.setState({updateImageAndTextProgress: "上传图文中...", isAddImageAndTextLock: true});
                }else{
                    this.setState({updateImageAndTextProgress: "提交中...", isAddImageAndTextLock: true});
                }
                actions.editImageText(this, this.state.imageData);
            }
        }else{
            notification.warning({
                placement: 'bottomRight',
                message: '提示：',
                description: '请勿输入 XAML 扩展标签，如：<x:smart> 这一类的标签',
                style: {
                  borderLeft: '4px solid rgb(255, 191, 0)'
                }
            });
        }
    }
    btnSaveBaseInfo(){
        if(this.state.baseData.id == undefined){
            if ($("#basePic")[0].files.length <= 0){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '首次，请选择图片（必选）！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.baseData.title == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '标题 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.baseData.summary == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '摘要 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.verifyMsgAndImage() && confirm("你确定要保存：基础信息 吗？")){
                this.setState({updateBaseLoading: true, updateBaseProgress: '上传图片中...'});
                actions.editArticle(this, this.state.baseData);
            }
        }else{
            if(this.state.baseData.title == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '标题 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.baseData.summary == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '摘要 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if (this.state.serviceVideoData != null && this.state.serviceVideoData.article.coverPicture == undefined){
                if ($("#basePic")[0].files.length <= 0){
                    notification.warn({
                        placement: 'bottomRight',
                        message: '提示：',
                        description: '首次，请选择图片（必选）！',
                        style: {
                          borderLeft: '4px solid rgb(255, 191, 0)'
                        }
                    });
                    return;
                }
            }
            if(this.verifyMsgAndImage() && confirm("你确定要保存：基础信息 吗？")){
                this.setState({updateBaseLoading: true, updateBaseProgress: '上传图片中...'});
                actions.editArticle(this, this.state.baseData);
            }
        }
    }
    //尺寸：300 x 300（px）
    getImgSize(imgFile){
        var that = this;
        var reader = new FileReader();
        reader.readAsDataURL(imgFile.files[0])
        reader.onload = function (e) {
           var image = new Image();
           image.src = e.target.result;
           image.onload= () => {
             if(image.naturalWidth != 300 || image.naturalHeight != 300){
                 notification.warn({
                     placement: 'bottomRight',
                     message: '提示：',
                     description: '缩略图尺寸：必须为 300 * 300 像素!',
                     style: {
                       borderLeft: '4px solid rgb(255, 191, 0)'
                     }
                 });
                 that.state.fileVerify = false;
                //  document.getElementById("imgThumbnail").src = "/src/styles/images/disk.png";
             }else{
                 that.state.fileVerify = true;
                 document.getElementById("imgThumbnail").src = e.target.result;
             }
   	      }
       }
    }
    getImgSize1(imgFile, imgId){
        var that = this;
        var reader = new FileReader();
        reader.readAsDataURL(imgFile.files[0])
        reader.onload = function (e) {
           var image = new Image();
           image.src = e.target.result;
           image.onload= () => {
                document.getElementById(imgId).src = e.target.result;
   	      }
       }
    }
    btnSaveThumbnailInfo(){
        if ($("#fileThumbnail")[0].files.length >= 1){
            if(this.state.fileVerify == true){
                if(confirm("你确定要上传：封面图 吗？")){
                    this.setState({updateThumbnailLoading: true, updateCoverPicProgress: '上传封面图...'});
                    actions.uploadThumbnail(this, this.state.coverId);
                }
            }else{
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '文件类型、大小、尺寸(300 * 300)，不正确!',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
            }
        }else{
            notification.warn({
                placement: 'bottomRight',
                message: '提示：',
                description: '首次，请选择封面图（必选）！',
                style: {
                  borderLeft: '4px solid rgb(255, 191, 0)'
                }
            });
        }
    }
    btnSaveAudioInfo(){
        if(this.state.audioData.id == undefined){
            if ($("#fileAudio")[0].files.length <= 0){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '首次，请选择音频（必选）！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.audioData.title == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '标题 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.audioData.author == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '作者 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }

            if(this.beforeUploadAudio() && confirm("你确定要保存：音频信息 吗？")){
                this.setState({updateAudioLoading: true, updateAudioProgress: '上传中，音频超过 5M，请耐心等待...'});
                actions.editArticleAudio(this, this.state.audioData);
            }
        }else{
            if (this.state.serviceVideoData != null && this.state.serviceVideoData.audio == undefined){
                if ($("#fileAudio")[0].files.length <= 0){
                    notification.warn({
                        placement: 'bottomRight',
                        message: '提示：',
                        description: '首次，请选择音频（必选）！',
                        style: {
                          borderLeft: '4px solid rgb(255, 191, 0)'
                        }
                    });
                    return;
                }
            }
            if(this.state.audioData.title == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '标题 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.audioData.author == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '作者 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }

            if(this.beforeUploadAudio() && confirm("你确定要保存：音频信息 吗？")){
                this.setState({updateAudioLoading: true, updateAudioProgress: '上传中，音频超过 5M，请耐心等待...'});
                actions.editArticleAudio(this, this.state.audioData);
            }
        }
    }
    btnSaveVideoInfo(){
        if(this.state.videoData.id == undefined){
            if ($("#fileVideo")[0].files.length <= 0){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '首次，请选择视频（必选）！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.videoData.author == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '作者 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.videoData.content == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '描述内容 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(/<([\w]+):([\w]+)>/.test(this.state.imgVideoEditor.$txt.html()) == false){
                if(this.beforeUploadVideo() && confirm("你确定要保存：视频信息 吗？")){
                    this.setState({updateVideoLoading: true, updateVideoProgress: '上传中，视频超过 5M，请耐心等待...'});
                    actions.editArticleVideo(this, this.state.videoData);
                }
            }else {
                notification.warning({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '请勿输入 XAML 扩展标签，如：<x:smart> 这一类的标签',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
            }
        }else{
            if (this.state.serviceVideoData != null && this.state.serviceVideoData.video == undefined){
                if ($("#fileVideo")[0].files.length <= 0){
                    notification.warn({
                        placement: 'bottomRight',
                        message: '提示：',
                        description: '首次，请选择视频（必选）！',
                        style: {
                          borderLeft: '4px solid rgb(255, 191, 0)'
                        }
                    });
                    return;
                }
            }
            if(this.state.videoData.author == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '作者 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(this.state.videoData.content == ""){
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '描述内容 不能为空！',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
                return;
            }
            if(/<([\w]+):([\w]+)>/.test(this.state.imgVideoEditor.$txt.html()) == false){
                if(this.beforeUploadVideo() && confirm("你确定要保存：视频信息 吗？")){
                    this.setState({updateVideoLoading: true, updateVideoProgress: '上传中，视频超过 5M，请耐心等待...'});
                    actions.editArticleVideo(this, this.state.videoData);
                }
            }else{
                notification.warning({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '请勿输入 XAML 扩展标签，如：<x:smart> 这一类的标签',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
            }
        }
    }
    changeCommentStatus(id, value){
        var strTip = "";
        if(value == "active"){
            strTip = "你确定要 公开 评论吗？";
        }else{
            strTip = "你确定要 禁用 评论吗？";
        }
        if(confirm(strTip)){
          actions.changeCommentStatus(this,{
            id: id,
            status: value
          });
        }
    }
    showAdminReplay(value, item){
        if(item == null){
            this.setState({showAdminReplay: value, replayCommentName: '', commentItem: null});
        }else {
            var replayCommentName = ((item.commentorName == undefined)?"匿名":item.commentorName);
            if(item.replyContent == undefined){
                this.setState({showAdminReplay: value, replayCommentName: replayCommentName, commentItem: item, replyContent: ''});
            }else {
                this.setState({showAdminReplay: value, replayCommentName: replayCommentName, commentItem: item, replyContent: item.replyContent});
            }

        }
    }
    setWeatherExtension(value){
        if(value == true){
            if(confirm("确定要首页推广吗?")){
                actions.weatherExtension(this,{
                  id: this.state.id,
                  extension: value
                });
            }
        }else{
          if(confirm("确定要取消首页推广吗?")){
              actions.weatherExtension(this,{
                id: this.state.id,
                extension: value
              });
          }
        }
    }
    setEnrollOrVote(value){
        if(value == true){
            if(confirm("确定要 启用 报名投票 吗?")){
                actions.changeEnroll(this,{
                  id: this.state.id,
                  enrollOrVote: value
                });
            }
        }else{
          if(confirm("确定要 关闭 报名投票 吗?")){
              actions.changeEnroll(this,{
                id: this.state.id,
                enrollOrVote: value
              });
          }
        }
    }
    showCommentDetails(val, cId){
        this.setState({showCommentDetails: val});
        if(cId != null){
            this.setState({commentDetail: null});
            actions.commentDetail(this, cId);
        }
    }
    renderRow(item, index){
        var btnStatus = null; var txtStatus = ""; var imgHeader = null;
        if(item.status == "active"){
            txtStatus = "公开";
            btnStatus = <Tooltip placement="topLeft" title="点击：修改状态"><Button type="danger" className="btnStatus" onClick={(e)=>{e.stopPropagation();this.changeCommentStatus(item.id, "inactive")}}>禁 用</Button></Tooltip>
        }else if(item.status == "inactive"){
            txtStatus = "禁用";
            btnStatus = <Tooltip placement="topLeft" title="点击：修改状态"><Button type="primary" className="btnStatus" onClick={(e)=>{e.stopPropagation();this.changeCommentStatus(item.id, "active")}}>公 开</Button></Tooltip>
        }
        if(item.avatarUrl == undefined){
            imgHeader = <img src="/src/styles/images/default.jpg" />
        }else{
            imgHeader = <img src={item.avatarUrl} />
        }
        var commentorName = ((item.commentorName == undefined)?"匿名":item.commentorName)
        if(item.replyId == undefined){
            return(<div className="divCommentItem" key={index} onClick={this.showCommentDetails.bind(this, true, item.id)}>
                {imgHeader}
                <Tooltip placement="topLeft" title={"回复：" + commentorName}>
                  <Button type="primary" shape="circle" icon="export" className="btnStatus" onClick={(e)=>{e.stopPropagation();this.showAdminReplay(true, item)}}></Button>
                </Tooltip>
                {btnStatus}
                <span className="spanStatus">当前状态：{txtStatus}</span>
                <div className="divName">{(item.commentorName == undefined)?"匿名":item.commentorName}</div>
                <div className="divContent" title={item.content}>{item.content}</div>
                <div className="divTimer">{"（"}<Icon type="like" title="赞" style={{fontSize: '15px', marginRight: '4px', color: '#108ee9'}}/><span style={{color: '#108ee9'}}>{item.commentThumbsUpIds.length}</span>{ "） " + new Date(item.commentDate).Format("yyyy-MM-dd hh:mm")}</div>
            </div>);
        }else {
            return(<div className="divCommentItem" key={index} onClick={this.showCommentDetails.bind(this, true, item.id)}>
                {imgHeader}
                {btnStatus}
                <span className="spanStatus">当前状态：{txtStatus}</span>
                <div className="divName">{(item.commentorName == undefined)?"匿名":item.commentorName}</div>
                <div className="divContent" title={item.content}>{item.content}</div>
                <div className="divTimer">{"（"}<Icon type="like" title="赞" style={{fontSize: '15px', marginRight: '4px', color: '#108ee9'}}/><span style={{color: '#108ee9'}}>{item.commentThumbsUpIds.length}</span>{ "） " + new Date(item.commentDate).Format("yyyy-MM-dd hh:mm")}</div>
                <div className="divReply">
                    <div className="divReplyTitle">
                        作者回复
                        <Tooltip placement="topLeft" title={"修改：" + commentorName}>
                          <Button type="primary" shape="circle" icon="edit" className="btnOperation" onClick={(e)=>{e.stopPropagation();this.showAdminReplay(true, item)}}></Button>
                        </Tooltip>
                        <Popconfirm title="你确定要 删除 回复吗?" onConfirm={this.removeReplyComment.bind(this, item.id)} okText="确定" cancelText="取消" placement="topRight">
                          <Button type="danger" shape="circle" icon="delete" className="btnOperation" onClick={(e)=>e.stopPropagation()}></Button>
                        </Popconfirm>
                    </div>
                    <div className="divContent" style={{paddingLeft: '8px',width: '513px'}} title={item.replyContent}>{item.replyContent}</div>
                    <div className="divTimer">{"（"}<Icon type="like" title="赞" style={{fontSize: '15px', marginRight: '4px', color: '#ef84dc'}}/><span style={{color: '#ef84dc'}}>{item.replyThumbsUpIds.length}</span>{ "） " + new Date(item.replyDate).Format("yyyy-MM-dd hh:mm")}</div>
                </div>
            </div>);
        }
    }
    removeReplyComment(cId){
        actions.removeReplyComment(this, cId);
    }
    onlyDeletedImg(id, pic, index){
        if(pic == undefined){
            return;
        }else{
            var onlyDeletedImg = <Popconfirm title="你确定要仅删除图片吗?"  onConfirm={this.btnOnlyDeleteImage.bind(this, id, index)} placement="topRight">
                <Button icon="close" type="danger" className="btnDeleteImage" shape="circle" title="点击删除图片"/>
            </Popconfirm>
            return onlyDeletedImg;
        }
    }
    modifyTheCover(id, thumbnailUrl){
        this.setState({updateThumbnailVisible: true, coverId: id, fileVerify: false});
        setTimeout(function() {
            document.getElementById("imgThumbnail").src = thumbnailUrl;
        },500);
    }
    deleteThumbnail(id){
        this.setState({deleteAudioLoading: true});
        actions.removeThumbnail(this, id);
    }
    setReplyContent(e){
        this.setState({replyContent: e.target.value});
    }
    btnReplayComment(){
        if(this.state.replyContent.trim() == ""){
            notification.warning({
                placement: 'bottomRight',
                message: '提示：',
                description: '回复评论，不能为空。',
                style: {
                  borderLeft: '4px solid rgb(255, 191, 0)'
                }
            });
        }else {
            this.state.commentItem.replyContent = this.state.replyContent;
            delete this.state.commentItem.whetherCommentThumbsUp;
            delete this.state.commentItem.whetherReplyThumbsUp;

            actions.replayComment(this, this.state.commentItem);
        }
    }
    enableSorting(e){
        if(this.state.sortIsEnabled == false){
          $(".divSorting").css("display", "flex");
          $("#divImageText").css("backgroundColor", "#f7f7f7");
          $("#divImageTextTitle").css("backgroundColor", "#6b8e23");
          this.setState({sortIsEnabled: true});
          this.onRefreshData();
        }else {
          if(this.state.orderUpdating == false){
              if(confirm("你确定要 保存 排序吗？")){
                  $(".divSorting").css("display", "none");
                  $("#divImageText").css("backgroundColor", "#fff");
                  $("#divImageTextTitle").css("backgroundColor", "#4B95E9");
                  this.setState({sortIsEnabled: false, orderUpdating: true});
                  $("#btnEnableSorting")[0].blur();
                  var testObj = this.state.serviceVideoData.imageTextList
                  actions.adjustArticleOrder(this, this.state.serviceVideoData.article.id, {its: testObj});
              }
          }
        }
    }
    btnSortFunction(flap, item){
        var testObj = this.state.serviceVideoData.imageTextList;
        if(flap == "up"){
            for (var i = 0; i < testObj.length; i++) {
                if(testObj[i].id == item.id){
                  var tempOrder = testObj[i-1].order;
                  testObj[i-1].order = testObj[i].order;
                  testObj[i].order = tempOrder;

                  var tempArray = testObj[i-1];
                  testObj[i-1] = testObj[i];
                  testObj[i] = tempArray;
                  break;
                }
            }

            this.state.serviceVideoData.imageTextList = testObj;
        }else if(flap == "down"){
            for (var i = 0; i < testObj.length; i++) {
                if(testObj[i].id == item.id){
                  var tempOrder = testObj[i+1].order;
                  testObj[i+1].order = testObj[i].order;
                  testObj[i].order = tempOrder;

                  var tempArray = testObj[i+1];
                  testObj[i+1] = testObj[i];
                  testObj[i] = tempArray;
                  break;
                }
            }

            this.state.serviceVideoData.imageTextList = testObj;
        }else if(flap == "toFirst"){
            for (var i = 0; i < testObj.length; i++) {
                if(testObj[i].id == item.id){
                  var tempOrder = testObj[0].order;
                  testObj[0].order = testObj[i].order;
                  testObj[i].order = tempOrder;

                  var tempArray = testObj[0];
                  testObj[0] = testObj[i];
                  testObj[i] = tempArray;
                  break;
                }
            }

            this.state.serviceVideoData.imageTextList = testObj;
        }else if(flap == "toBottom"){
            for (var i = 0; i < testObj.length; i++) {
                if(testObj[i].id == item.id){
                  var tempOrder = testObj[testObj.length - 1].order;
                  testObj[testObj.length - 1].order = testObj[i].order;
                  testObj[i].order = tempOrder;

                  var tempArray = testObj[testObj.length - 1];
                  testObj[testObj.length - 1] = testObj[i];
                  testObj[i] = tempArray;
                  break;
                }
            }

            this.state.serviceVideoData.imageTextList = testObj;
        }
        this.setState({serviceVideoData: this.state.serviceVideoData});
    }
    renderSortButton(item, index){
        if(index == 0){
            return(<div className="divSorting">
                <Button icon="arrow-down" ghost className="firstUp" type="primary" onClick={this.btnSortFunction.bind(this, 'down', item)} title="向下"></Button>
                <Button icon="download" ghost className="firstDown" type="danger" onClick={this.btnSortFunction.bind(this, 'toBottom', item)} title="置底"></Button>
            </div>);
        }else if(index == (this.state.serviceVideoData.imageTextList.length - 1)){
            return(<div className="divSorting">
                <Button icon="arrow-up" ghost className="firstUp" type="primary" onClick={this.btnSortFunction.bind(this, 'up', item)} title="向上"></Button>
                <Button icon="to-top" ghost className="firstDown" type="danger" onClick={this.btnSortFunction.bind(this, 'toFirst', item)} title="置顶"></Button>
            </div>);
        }else {
            return(<div className="divSorting">
                <Button icon="arrow-up" ghost className="firstUp" type="primary" onClick={this.btnSortFunction.bind(this, 'up', item)} title="向上"></Button>
                <Button icon="arrow-down" ghost className="firstDown" type="danger" onClick={this.btnSortFunction.bind(this, 'down', item)} title="向下"></Button>
            </div>);
        }
    }
    renderCommentDetail(){
        var commentDetail = this.state.commentDetail;
        if(commentDetail == null){
          return(<div className="divLoading" style={{paddingBottom: '8px', width: '700px', backgroundColor: '#fff'}}><img src="/src/styles/images/timg.gif"/>数据加载中...</div>)
        }else if(commentDetail != null){
          var phoneLists = "无";
          if(commentDetail.commentParent.phones.length > 0){
            phoneLists = "";
            for (var i = 0; i < commentDetail.commentParent.phones.length; i++) {
                phoneLists += commentDetail.commentParent.phones[i] + "、";
            }
            phoneLists = phoneLists.substr(0, phoneLists.length - 1);
          }

          if(commentDetail.comment.replyId == undefined){
              var commentThumbsList = null; var commentThumbsUpCount = 0;
              if(commentDetail.commentThumbsUpParents == undefined){
                  commentThumbsList = <div className="divEmptData">无点赞</div>
                  commentThumbsUpCount = 0;
              }else {
                  commentThumbsUpCount = commentDetail.commentThumbsUpParents.length;
                  var colorIndex = 1;
                  commentThumbsList = commentDetail.commentThumbsUpParents.map((item, index)=>{
                      var avatar = null; var colorClass = "divItem divColor1";
                      if(item.avatarUrl == undefined || item.avatarUrl == ""){
                          avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                      }else {
                          avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                      }
                      if(colorIndex < 5){
                          colorIndex++
                          colorClass = "divItem divColor" + colorIndex;
                      }else {
                          colorIndex = 1;
                          colorClass = "divItem divColor" + colorIndex;
                      }
                      return(
                        <div key={index} className={colorClass}>
                            {avatar}
                            <div><span className="spanText">昵称：{item.nickName}，Email：{item.email}，性别：{(item.gender == 1)?"男":"女"}</span></div>
                            <div>关系：{item.relationship}，省份：{item.province}，城市：{item.city}</div>
                        </div>
                      )
                  });
              }
              return(
                  <div className="divCommentDetail">
                      <div className="divContainer">
                          <div className="divComment" style={{width: '530px'}}>
                              <img src="/src/styles/images/quotes.png" className="imgQuotes" />
                              <div className="divCommentContent">{commentDetail.comment.content}</div>
                              <div className="divOtherInfo">
                                {new Date(commentDetail.comment.commentDate).Format("yyyy-MM-dd")}
                              </div>
                              <img src="/src/styles/images/downArrow.png" className="imgDownArrow" />
                          </div>
                          <div className="divCommentParent">
                              <a href={"/parentsList/parentsDetail/" + commentDetail.commentParent.id} target="_blank" title="转到家长信息"><img src={commentDetail.commentParent.avatarUrl} /></a>
                              <div className="divContent">
                                <div>
                                  <span style={{color: '#3498DB', fontWeight: 'bold'}}>评论人</span>：{commentDetail.commentParent.nickName}， Email：{commentDetail.commentParent.email}， 性别：{(commentDetail.commentParent.gender == "1")?"男":"女"}
                                </div>
                                <div>
                                  国家：{commentDetail.commentParent.country}， 省份：{commentDetail.commentParent.province}， 城市：{commentDetail.commentParent.city}
                                </div>
                                <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
                                  最后登录时间：{((commentDetail.commentParent.lastLoginTime == undefined)?"":new Date(commentDetail.commentParent.lastLoginTime).Format("yyyy-MM-dd"))}，手机：<label style={{cursor: 'pointer'}} title={phoneLists}>{phoneLists}</label>
                                </div>
                              </div>
                          </div>
                          <div className="divThumbsList">
                              <div className="divTitle"><Icon type="message" style={{marginRight: '7px'}}/>评论：点赞（{commentThumbsUpCount} 个）</div>
                              <div className="divThumbsContent">
                                {commentThumbsList}
                              </div>
                          </div>
                      </div>
                </div>
              )
          }else {
              var commentThumbsList = null; var replyThumbsList = null; var commentThumbsUpCount = 0; var replyThumbsUpCount = 0;
              if(commentDetail.commentThumbsUpParents == undefined){
                  commentThumbsList = <div className="divEmptData">无点赞</div>
                  commentThumbsUpCount = 0;
              }else {
                  commentThumbsUpCount = commentDetail.commentThumbsUpParents.length;
                  var colorIndex = 1;
                  commentThumbsList = commentDetail.commentThumbsUpParents.map((item, index)=>{
                      var avatar = null; var colorClass = "divItem divColor1";
                      if(item.avatarUrl == undefined || item.avatarUrl == ""){
                          avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                      }else {
                          avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                      }
                      if(colorIndex < 5){
                          colorIndex++
                          colorClass = "divItem divColor" + colorIndex;
                      }else {
                          colorIndex = 1;
                          colorClass = "divItem divColor" + colorIndex;
                      }
                      return(
                        <div key={index} className={colorClass}>
                            {avatar}
                            <div><span className="spanText">昵称：{item.nickName}，Email：{item.email}，性别：{(item.gender == 1)?"男":"女"}</span></div>
                            <div>关系：{item.relationship}，省份：{item.province}，城市：{item.city}</div>
                        </div>
                      )
                  });
              }
              if(commentDetail.replyThumbsUpParents == undefined){
                  replyThumbsList = <div className="divEmptData">无点赞</div>
                  replyThumbsUpCount = 0;
              }else {
                  replyThumbsUpCount = commentDetail.replyThumbsUpParents.length;
                  replyThumbsList = commentDetail.replyThumbsUpParents.map((item, index)=>{
                      var avatar = null; var colorClass = "divItem divColor1";
                      if(item.avatarUrl == undefined || item.avatarUrl == ""){
                          avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                      }else {
                          avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                      }
                      if(colorIndex < 5){
                          colorIndex++
                          colorClass = "divItem divColor" + colorIndex;
                      }else {
                          colorIndex = 1;
                          colorClass = "divItem divColor" + colorIndex;
                      }
                      return(
                        <div key={index} className={colorClass}>
                            {avatar}
                            <div><span className="spanText">昵称：{item.nickName}，Email：{item.email}，性别：{(item.gender == 1)?"男":"女"}</span></div>
                            <div>关系：{item.relationship}，省份：{item.province}，城市：{item.city}</div>
                        </div>
                      )
                  });
              }
              return(
                  <div className="divCommentDetail">
                    <div className="divContainer">
                        <div className="divComment" style={{width: '530px'}}>
                            <img src="/src/styles/images/quotes.png" className="imgQuotes" />
                            <div className="divCommentContent">{commentDetail.comment.content}</div>
                            <div className="divOtherInfo">
                              {new Date(commentDetail.comment.commentDate).Format("yyyy-MM-dd")}
                            </div>
                            <img src="/src/styles/images/downArrow.png" className="imgDownArrow" />
                        </div>
                        <div className="divCommentParent">
                            <a href={"/parentsList/parentsDetail/" + commentDetail.commentParent.id} target="_blank" title="转到家长信息"><img src={commentDetail.commentParent.avatarUrl} /></a>
                            <div className="divContent">
                              <div>
                                <span style={{color: '#3498DB', fontWeight: 'bold'}}>评论人</span>：{commentDetail.commentParent.nickName}， Email：{commentDetail.commentParent.email}， 性别：{(commentDetail.commentParent.gender == "1")?"男":"女"}
                              </div>
                              <div>
                                国家：{commentDetail.commentParent.country}， 省份：{commentDetail.commentParent.province}， 城市：{commentDetail.commentParent.city}
                              </div>
                              <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
                                最后登录时间：{((commentDetail.commentParent.lastLoginTime == undefined)?"":new Date(commentDetail.commentParent.lastLoginTime).Format("yyyy-MM-dd"))}，手机：<label style={{cursor: 'pointer'}} title={phoneLists}>{phoneLists}</label>
                              </div>
                            </div>
                        </div>
                        <div className="divThumbsList">
                            <div className="divTitle"><Icon type="message" style={{marginRight: '7px'}}/>评论：点赞（{commentThumbsUpCount} 个）</div>
                            <div className="divThumbsContent">
                              {commentThumbsList}
                            </div>
                        </div>
                    </div>
                    <div className="divContainer" style={{marginLeft: '10px'}}>
                        <div className="divComment" style={{width: '530px'}}>
                            <img src="/src/styles/images/quotes.png" className="imgQuotes" />
                            <div className="divCommentContent">{commentDetail.comment.replyContent}</div>
                            <div className="divOtherInfo">
                              {new Date(commentDetail.comment.replyDate).Format("yyyy-MM-dd")}
                            </div>
                            <img src="/src/styles/images/downArrow.png" className="imgDownArrow" />
                        </div>
                        <div className="divCommentParent">
                            <img src={((commentDetail.replyUser.photoPic == undefined || commentDetail.replyUser.photoPic == "")?"/src/styles/images/defaultHeader.jpg":commentDetail.replyUser.photoPic)} />
                            <div className="divContent">
                              <div>
                                <span style={{color: '#E84C3C', fontWeight: 'bold'}}>回复人</span>：{((commentDetail.replyUser.firstName == undefined)?"":commentDetail.replyUser.firstName) + " " + ((commentDetail.replyUser.lastName == undefined)?"":commentDetail.replyUser.lastName)}， Email：{commentDetail.replyUser.email}， 性别：{(commentDetail.replyUser.gender == "1")?"男":"女"}
                              </div>
                              <div>
                                工作：{((commentDetail.replyUser.job == undefined)?"":commentDetail.replyUser.job)}， 手机号：{commentDetail.replyUser.phoneNum}
                              </div>
                              <div>
                                位置：{((commentDetail.replyUser.location == undefined)?"":commentDetail.replyUser.location) + " " + ((commentDetail.replyUser.position == undefined)?"":commentDetail.replyUser.position)}
                              </div>
                            </div>
                        </div>
                        <div className="divThumbsList">
                            <div className="divTitle"><Icon type="export" style={{marginRight: '7px'}}/>回复：点赞（{replyThumbsUpCount} 个）</div>
                            <div className="divThumbsContent">
                              {replyThumbsList}
                            </div>
                        </div>
                    </div>
                </div>
              )
          }
        }
    }
    renderThumbsUpsByArticle(){
        var articleThumbsUpsDetail = this.state.articleThumbsUpsDetail;
        if(articleThumbsUpsDetail == null){
          return(<div className="divLoading" style={{paddingBottom: '8px', width: '563px', backgroundColor: '#fff', borderStyle: 'dashed'}}><img src="/src/styles/images/timg.gif"/>数据加载中...</div>)
        }else{
          if(articleThumbsUpsDetail.length <= 0){
              return(<div className="divLoading" style={{paddingBottom: '8px', width: '563px', backgroundColor: '#fff', borderStyle: 'dashed', padding: '14px'}}>无点赞</div>)
          }else {
              var commentThumbsList = articleThumbsUpsDetail.map((item, index)=>{
                  var avatar = null;
                  if(item.avatarUrl == undefined || item.avatarUrl == ""){
                      avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                  }else {
                      avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                  }
                  return(
                    <div key={index} className="divItemThumbsUps">
                        {avatar}
                        <div className="divNickName">{item.nickName}</div>
                        <div className="divText" style={{marginBottom: '10px', fontSize: '13px'}}>{item.email}</div>
                        <div className="divText">关系：{item.relationship}，性别：{(item.gender == 1)?"男":"女"}</div>
                        <div className="divText">省份：{item.province}，城市：{item.city}</div>
                    </div>
                  )
              });

              return(
                  <div className="divArticleThumbsUps">
                      {commentThumbsList}
                  </div>
              )
          }
       }
    }
    onShowThumbsUpsByArticle(){
        this.setState({showArticleThumbsUps: true, articleThumbsUpsDetail: null});
        actions.findThumbsUpsByAid(this, this.state.id);
    }
    showArticleThumbsUps(val){
        this.setState({showArticleThumbsUps: val});
    }
    getSizeWithScrollbar(){
        if(window.innerWidth){
            return {
              width : window.innerWidth,
              height: window.innerHeight
            }
        }else if(document.documentElement.offsetWidth == document.documentElement.clientWidth){
            return {
              width : document.documentElement.offsetWidth,
              height: document.documentElement.offsetHeight
            }
        }else{
            return {
              width : document.documentElement.clientWidth + getScrollWith(),
              height: document.documentElement.clientHeight + getScrollWith()
            }
        }
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            articleListData: data,
            updateLoading: false
        });
    }
    btnShowArticleList(val){
        var that = this;
        this.setState({articleList: val});
        if(val == true){
          that.setState({updateLoading: true,relatedArticleTip: "加载中...", tempArticleData: null});
          setInterval(function(){
            window.onresize();
          }, 300);
          setTimeout(function(){
            that.refs["articleListView"].refreshData();
            window.onresize();
          }, 800);
        }else {
          that.setState({updateLoading: false, pastDescription: ''});
        }
    }
    btnRelatedArticle(){
        if(this.state.tempArticleData == null){
            Modal.warning({
              title: '提示：',
              content: '你还未选择，请先选择文章。',
            });
            return;
        }
        if(this.state.pastDescription == ""){
            Modal.warning({
              title: '提示：',
              content: '文章描述，不能为空。',
            });
            return;
        }

        var isAssociation = false;
        for (var i = 0; i < this.state.serviceVideoData.articleConnections.length; i++) {
          if(this.state.serviceVideoData.articleConnections[i].acId == this.state.tempArticleData.id){
              isAssociation = true; break;
          }
        }
        if(isAssociation == false){
            this.setState({updateLoading: true, relatedArticleTip: "关联中，请稍候..."});
            var paramData = {};
            if(this.state.isAddOrEdite == "Add"){
                paramData = {
                  aId: this.state.baseData.id,
                  acId: this.state.tempArticleData.id,
                  description: this.state.pastDescription
                }
            }else if(this.state.isAddOrEdite == "Edit"){
                paramData = this.state.editeParamData;
                paramData.aId = this.state.baseData.id;
                paramData.acId = this.state.tempArticleData.id;
                paramData.description = this.state.pastDescription;
            }
            actions.addOrEditReviewOfThePast(this, paramData);
        }else {
            Modal.warning({
              title: '提示：',
              content: '你已关联这篇文章，请选择其它文章。',
            });
        }
    }

    onSelectType(e){
        this.state.searchObject.type = e.key;
        this.refs["articleListView"].refreshData();
        this.setState({updateLoading: true , relatedArticleTip: "加载中..."});
    }
    onImgLoad(e){
        $(e.currentTarget).css("backgroundImage", "none");
    }
    selectArticle(item){
        this.setState({tempArticleData: item, pastDescription: item.title});
    }
    setPastDescription(e){
        this.setState({pastDescription: e.target.value});
    }
    btnSelectAndEdit(item){
        this.btnShowArticleList(true);
        this.setState({pastDescription: item.description, tempArticleData: {id: item.acId}, isAddOrEdite: 'Edit', editeParamData: item});
    }
    btnShowPreviewArticle(val){
        this.setState({showPreviewArticle: val});

        if(val == true){
          var that = this;
          var interval = setInterval(function(){
            if($("#previewIframe").length > 0){
                $("#previewIframe").attr("src", "/articlePreview/" + that.state.id);
                clearInterval(interval);
            }
          }, 10);
        }
    }
    btnSendNotifNext(val){
        if(val == false){
            if(this.state.tempParentsData.length <= 0){
              Modal.warning({
                title: '提示：',
                content: '对不起，请先选择家长。',
              });
            }else {
              this.setState({
                isSendNotifNext: val,
                labelTimer: new Date().Format("MM月dd日"),
                tempInfoData:{
                  ...this.state.tempInfoData,
                  title: this.state.serviceVideoData.article.title,
                  type: $($(".divSelectType1").find(".ant-select")[1]).find(".ant-select-selection-selected-value").text(),
                  summary: this.state.serviceVideoData.article.summary
                }
              });
            }
        }else {
            this.setState({isSendNotifNext: val});
        }
    }
    onChangeByTempInfo(val, e){
        if(val == "title"){
          this.setState({
            tempInfoData: {
              ...this.state.tempInfoData,
              title: e.target.value
            }
          });
        }else if (val == "type") {
          this.setState({
            tempInfoData: {
              ...this.state.tempInfoData,
              type: e.target.value
            }
          });
        }else if (val == "summary") {
          this.setState({
            tempInfoData: {
              ...this.state.tempInfoData,
              summary: e.target.value
            }
          });
        }else if (val == "Remark") {
          this.setState({
            tempInfoData: {
              ...this.state.tempInfoData,
              Remark: e.target.value
            }
          });
        }
    }
    btnSendNotification(){
        var that = this;
        Modal.confirm({
          title: '提示：',
          content: '你确定要 发送 通知吗？',
          onOk: function(){
              that.setState({sendingNotifications: true});
              var openIds = []
              for (var i = 0; i < that.state.tempParentsData.length; i++) {
                  openIds.push(that.state.tempParentsData[i]);
              }
              var paramData = {
                title: that.state.tempInfoData.title,
                type: that.state.tempInfoData.type,
                summary: that.state.tempInfoData.summary,
                Remark: that.state.tempInfoData.Remark,
                aId: that.state.serviceVideoData.article.id,
                aTitle: that.state.serviceVideoData.article.title,
                openIds: openIds
              };
              actions.sendNotification(that, paramData);
          }
        });
    }
    selectParents(item){
        if(this.state.tempParentsData.length <= 0){
            this.state.tempParentsData.push(item.openId);
        }else {
            var isHave = false;
            for (var i = 0; i < this.state.tempParentsData.length; i++) {
                if (this.state.tempParentsData[i] == item.openId){
                  this.state.tempParentsData.splice(i, 1);
                  isHave = true;
                }
            }
            if(isHave == false){
                this.state.tempParentsData.push(item.openId);
            }
        }
        this.setState({tempParentsData: this.state.tempParentsData});
    }
    btnShowParentsList(flag){
        this.setState({showParentsList: flag});
        if(flag == true){
          setTimeout(function(){
            window.onresize();
          }, 100);
          actions.findParentsByOpenIds(this);
        }
    }
    onChangeBySwitch(val){
        if(val == true){
          var openIds = [];
          for (var i = 0; i < this.state.parentsListData.length; i++) {
              openIds.push(this.state.parentsListData[i].openId);
          }
          this.setState({tempParentsData: openIds});
        }else if(val == false){
          this.setState({tempParentsData: []});
        }
    }
    renderItemByParents(item, index) {
        var imgCoverPicture = null;
        if(item.avatarUrl != undefined){
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src={item.avatarUrl} onLoad={this.onImgLoad.bind(this)}/>
            </div>
        }else{
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src="/src/styles/images/default.jpg"/>
            </div>
        }

        var divSelected = null; var isSelected = false;
        for (var i = 0; i < this.state.tempParentsData.length; i++) {
            if (this.state.tempParentsData[i] == item.openId){
              isSelected = true;
            }
        }

        if(isSelected == true){
            divSelected = <div className="divSelected" style={{display: 'block'}}></div>
        }else{
            divSelected = <div className="divSelected"></div>
        }
        return (
            <Card key={index} onClick={this.selectParents.bind(this, item)} className="divItemLineMini" style={{width: '485px', marginBottom: '16px'}}>
                {imgCoverPicture}
                <div className="userInfo" style={{marginTop: '0px'}}>
                    <div className="divArticleTitle1">
                        <span>{'姓名：' + ((item.nickName == undefined)?"":item.nickName)}</span>
                        <span>{'，性别：' + ((item.gender == 1)?"男":"女")}</span>
                    </div>
                    <div className="divContent">
                        <span>{'省份：' + ((item.province == undefined)?"":item.province)}</span>
                        <span>{'，城市：' + ((item.city == undefined)?"":item.city)}</span>
                    </div>
                    <div className="divContent">
                        <span>{'关系：' + ((item.relationship == undefined)?"":item.relationship)}</span>
                        <span>{'，电子邮件：' + ((item.email == undefined)?"":item.email)}</span>
                    </div>
                    <div className="divContent" style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '379px'}}>
                        {'手机号：' + ((item.phones == undefined)?"":item.phones)}
                    </div>
                    {divSelected}
                </div>
            </Card>
        );
    }
    renderItem(item, index) {
        var imgCoverPicture = null;
        if(item.coverPicture != undefined){
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src={item.coverPicture} onLoad={this.onImgLoad.bind(this)}/>
              <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
            </div>
        }else{
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src="/src/styles/images/default.jpg"/>
              <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
            </div>
        }
        var divSelected = null;
        if(this.state.tempArticleData != null){
            if(item.id == this.state.tempArticleData.id){
                divSelected = <div className="divSelected" style={{display: 'block'}}></div>
            }else{
                divSelected = <div className="divSelected"></div>
            }
        }
        return (
            <Card key={index} onClick={this.selectArticle.bind(this, item)} className="divItemLineMini" style={{width: '485px', marginBottom: '16px'}}>
                {imgCoverPicture}
                <div className="userInfo" style={{marginTop: '0px'}}>
                    <div className="divArticleTitle1" title={((item.title == undefined)?"":item.title)}>
                        <span>{'标题：' + ((item.title == undefined)?"":item.title)}</span>
                    </div>
                    <div className="divArticleContent2" title={((item.summary == undefined)?"":item.summary)}>
                        <span>{'摘要：' + ((item.summary == undefined)?"":item.summary)}</span>
                    </div>
                    <p className="pOverview">
                        <span style={{color: '#9a9a9a'}}>
                          <Icon type="like" className="iconLike" title="点赞" />{item.likeIds.length}
                          <Icon type="eye" className="iconEye" title="浏览"/>{item.browseNumbers}
                          <Icon type="share-alt" className="iconEye" title="转载"/>{item.shareNumbers}
                          <Icon type="message" className="iconEye" title="评论"/>{item.commentIds.length}
                        </span>
                    </p>
                    <div className="divContent">
                        <span>{'创建：' + ((item.commitTime == undefined)?"":new Date(item.commitTime).Format("yyyy-MM-dd"))}</span>
                        <span>{'，修改：' + ((item.updateTime == undefined)?"":new Date(item.updateTime).Format("yyyy-MM-dd"))}</span>
                    </div>
                    {divSelected}
                </div>
            </Card>
        );
    }
    btnShowSignUpList(articleId) {
        browserHistory.push("/articleDesign/" + this.props.routeParams.id + "/" + this.props.routeParams.isTravel + "/registerList/" + articleId);
    }
    render() {
        var baseFlap = false;
        const selectTypeOptions = typeData.map((type, index) => {
          return (<Option key={type}>{type}</Option>)
        });
        const selectSubTypeOptions = this.state.typeData.map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);

        const typeOptions = typeData.map(type => <Option key={type}>{type}</Option>);
        const subTypeOptions = this.state.typeData.map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);
        const editSubTypeOptions = subTypeData[typeData[this.state.editTypeIndex]].map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);

        var serverArticleList = null;
        if(this.state.articleListData.length != 0){
            serverArticleList = <div className="divFlexListMini">
              {
                  this.state.articleListData.map((item, index) => {
                      return this.renderItem(item, index);
                  })
              }
            </div>
        }else{
            serverArticleList = <div className="divEmptyDataMini">对不起，没有相关数据。</div>
        }
        //家长列表数据
        var serverParentsList = null;
        if(this.state.parentsListData.length != 0){
            serverParentsList = <div className="divFlexListMini" style={{justifyContent: 'initial'}}>
              {
                  this.state.parentsListData.map((item, index) => {
                      return this.renderItemByParents(item, index);
                  })
              }
            </div>
        }else{
            serverParentsList = <div className="divEmptyDataMini">对不起，没有相关数据。</div>
        }
        //基础信息
        var baseElement = <Button icon="idcard" type="primary" onClick={this.btnShowBaseInfo.bind(this, true)}>添加基础信息</Button>;
        if(this.state.serviceVideoData != null && this.state.serviceVideoData.article.title != undefined){
            baseFlap = true; var divCreatePeople = <div className="divArticleTitle">创建者：未知</div>;
            if(this.state.serviceVideoData.createPeople != undefined){
                divCreatePeople = <div className="divArticleTitle">创建者：{((this.state.serviceVideoData.createPeople.firstName == undefined)?"":this.state.serviceVideoData.createPeople.firstName)+" "+((this.state.serviceVideoData.createPeople.lastName == undefined)?"":this.state.serviceVideoData.createPeople.lastName)}</div>
            }
            baseElement = <div className="divSubItem" style={{marginBottom: '10px', width: '517px', height: '220px'}}>
                  <Spin spinning={this.state.deleteImageLoading} tip="加载中...">
                      <img src={this.state.serviceVideoData.article.coverPicture} className="imgCoverPicture" onError={this.onImgError.bind(this, this.state.serviceVideoData.article.coverPicture)}/>
                  </Spin>
                  <div className="divArticleTitle" title={this.state.serviceVideoData.article.title}>标题：{this.state.serviceVideoData.article.title}</div>
                  <div className="divArticleContent1" title={this.state.serviceVideoData.article.summary}>摘要：{this.state.serviceVideoData.article.summary}</div>
                  {divCreatePeople}
                  <div style={{lineHeight: '21px'}}>创建：{new Date(this.state.serviceVideoData.article.commitTime).Format("yyyy-MM-dd hh:mm:ss")}，修改：{(this.state.serviceVideoData.article.updateTime == undefined)?"":new Date(this.state.serviceVideoData.article.updateTime).Format("yyyy-MM-dd hh:mm:ss")}</div>
                  <div style={{lineHeight: '21px'}}>发布：{(this.state.serviceVideoData.article.publishDate == undefined)?"":new Date(this.state.serviceVideoData.article.publishDate).Format("yyyy-MM-dd hh:mm:ss")}，推广：{(this.state.serviceVideoData.article.extensionTime == undefined)?"":new Date(this.state.serviceVideoData.article.extensionTime).Format("yyyy-MM-dd hh:mm:ss")}</div>
                  <div style={{lineHeight: '24px', fontSize: '14px'}}>
                    <Tooltip placement="bottomLeft" title="点击查看详情">
                      <a href="javascript:void(0)" onClick={this.onShowThumbsUpsByArticle.bind(this)} style={{color: 'rgba(0,0,0,.65)', textDecoration: 'none'}}><Icon type="like" className="iconLike" />{this.state.serviceVideoData.article.likeIds.length}</a>
                    </Tooltip>
                    <Icon type="eye" className="iconEye" title="浏览"/>{this.state.serviceVideoData.article.browseNumbers}
                    <Icon type="share-alt" className="iconEye" title="转载"/>{this.state.serviceVideoData.article.shareNumbers}
                    <Icon type="message" className="iconEye" title="评论"/>{this.state.serviceVideoData.article.commentIds.length}
                  </div>
                  <div style={{lineHeight: '21px'}}>
                    <span style={{marginRight: '5px',verticalAlign: 'text-top'}}>状态：{(this.state.serviceVideoData.article.status == "inactive")?"未公开  ":"公开  "}</span>
                    <Switch style={{marginRight: '10px'}} checked={(this.state.serviceVideoData.article.status == "active")} checkedChildren={'开'} unCheckedChildren={'关'} onChange={this.setArticleStatus.bind(this)}/>
                    <span style={{marginRight: '5px',verticalAlign: 'text-top'}}>首页推广：</span>
                    <Switch style={{marginRight: '10px'}} checked={this.state.serviceVideoData.article.extension} checkedChildren={'开'} unCheckedChildren={'关'} onChange={this.setWeatherExtension.bind(this)}/>
                    <span style={{marginRight: '5px',verticalAlign: 'text-top'}} style={{marginRight: '5px',verticalAlign: 'text-top'}}>报名 | 投票：</span>
                    <Switch checked={this.state.serviceVideoData.article.enrollOrVote} checkedChildren={'开'} unCheckedChildren={'关'} onChange={this.setEnrollOrVote.bind(this)}/>
                  </div>
                  <div style={{lineHeight: '21px', marginLeft: '186px', marginTop: '5px'}}>
                    <Button icon="mail" type="primary" style={{marginRight: '10px'}} onClick={this.btnShowParentsList.bind(this, true)}>推送通知</Button>
                    <Button icon="edit" type="primary" onClick={this.btnModifyBase.bind(this, this.state.serviceVideoData.article)}>修改基础信息</Button>
                  </div>
          </div>
        }
        //视频元素
        var videoElement = <Button icon="video-camera" type="primary" onClick={this.btnAddVideo.bind(this)}>添加视频</Button>;
        if(this.state.serviceVideoData != null && this.state.serviceVideoData.video != undefined){
            videoElement = <div className="divSubItem">
                <Spin spinning={this.state.deleteVideoLoading} tip="删除中...">
                    <Spin spinning={this.state.videoLoading} tip="加载中...">
                        <video src={this.state.serviceVideoData.video.videoUrl} controls="controls" className="videoTag" onLoadedData={this.onVideoLoaded.bind(this)}>
                            您的浏览器不支持 video 标签。
                        </video>
                    </Spin>
                    <div>作者：{this.state.serviceVideoData.video.author}</div>
                    <div dangerouslySetInnerHTML={{__html: this.state.serviceVideoData.video.content}} className="divContent"/>
                    <div style={{textAlign: 'right'}}>
                        <Popconfirm title="你确定要 删除 视频吗?" onConfirm={this.btnDeleteVideo.bind(this)} placement="topRight">
                            <Button icon="delete" type="danger" className="deleteVideo">删除</Button>
                        </Popconfirm>

                        <Button icon="edit" type="primary" onClick={this.btnModifyVideo.bind(this)}>修改视频</Button>
                    </div>
                </Spin>
            </div>
        }
        //音频元素
        var audioElement = <Button icon="customer-service" type="primary" onClick={this.btnAddAudio.bind(this)}>添加音频</Button>; var thumbnailImg = null; var btnDeleteThumbnail = null;
        if(this.state.serviceVideoData != null && this.state.serviceVideoData.audio != undefined){
            if(this.state.serviceVideoData.audio.thumbnailUrl == undefined || this.state.serviceVideoData.audio.thumbnailUrl == ""){
                thumbnailImg = <img id="thumbnailImg" src="/src/styles/images/disk.png" title="点击：修改封面" onClick={this.modifyTheCover.bind(this, this.state.serviceVideoData.audio.id, "/src/styles/images/disk.png")} />
            }else{
                thumbnailImg = <img id="thumbnailImg" src={this.state.serviceVideoData.audio.thumbnailUrl} title="点击：修改封面" onClick={this.modifyTheCover.bind(this, this.state.serviceVideoData.audio.id, this.state.serviceVideoData.audio.thumbnailUrl)} />
                btnDeleteThumbnail = <Popconfirm title="你确定要 删除 封面图吗?" onConfirm={this.deleteThumbnail.bind(this,this.state.serviceVideoData.audio.id)} placement="topRight">
                    <Button id="btnDeleteThumbnail" type="danger" shape="circle" icon="close" className="btnDeleteThumbnail" title="删除封面图"/>
                </Popconfirm>
            }
            audioElement = <div className="divSubItem">
                <Spin spinning={this.state.deleteAudioLoading} tip="删除中...">
                    <div className="divThumbnail">
                        {thumbnailImg}
                        {btnDeleteThumbnail}
                    </div>
                    <Spin spinning={this.state.audioLoading}>
                      <audio src={this.state.serviceVideoData.audio.audioUrl} preload="none" controls="controls" className="audioTag" onPlay={this.onPlay.bind(this)} onLoadedData={this.onAudioLoaded.bind(this)}>
                          您的浏览器不支持 audio 标签。
                      </audio>
                    </Spin>
                    <div>标题：{this.state.serviceVideoData.audio.title}，作者：{this.state.serviceVideoData.audio.author}</div>
                    <div style={{marginBottom: '7px'}}>摘要：{this.state.serviceVideoData.audio.content}</div>
                    <div style={{textAlign: 'right'}}>
                        <span style={{float: 'left', marginTop: '5px', color: '#4d97f7'}}>播放量：{this.state.serviceVideoData.audio.playCount}</span>
                        <Popconfirm title="你确定要 删除 音频吗?" onConfirm={this.btnDeleteAudio.bind(this)} placement="topRight">
                            <Button icon="delete" type="danger" className="deleteVideo">删除</Button>
                        </Popconfirm>
                        <Button icon="edit" type="primary" onClick={this.btnModifyAudio.bind(this)}>修改音频</Button>
                    </div>
                </Spin>
            </div>
        }
        //图文元素（遍历）
        var imageElement = <Button icon="picture" type="primary" disabled={this.state.isAddImageAndTextLock} onClick={this.btnShowImage.bind(this, true)} style={{marginTop: '10px', marginBottom: '10px'}}>添加图文</Button>;
        if(this.state.serviceVideoData != null && this.state.serviceVideoData.imageTextList != undefined){
            imageElement = <Spin spinning={this.state.orderUpdating} tip="排序（Order）提交中..." style={{right: '0px', maxHeight: '81px'}}>
                <div id="divImageText" style={{paddingTop: '10px'}}>
                  {
                      this.state.serviceVideoData.imageTextList.map((item, index)=>{
                            return(<div key={index} className="divSubItem" style={{marginBottom: '10px'}}>
                                <Spin ref={"imageText_" + index} spinning={this.state.deleteImageLoading} tip={this.state.tipImageTextSpinning}>
                                    <img src={item.pic} style={{width: '336px', minHeight: '10px', display: ((item.pic ==  undefined)?'none':'block')}} onError={this.onImgError.bind(this, item.pic)}/>
                                    {
                                      this.onlyDeletedImg(item.id, item.pic, index)
                                    }
                                    <div dangerouslySetInnerHTML={{__html: item.content}} className="divContent" style={{wordWrap: 'break-word'}}/>
                                    <div style={{textAlign: 'right'}}>
                                        <Popconfirm title="你确定要 删除 图文吗?" onConfirm={this.btnDeleteImage.bind(this, item.id, item.pic, index)} placement="topRight">
                                            <Button icon="delete" type="danger" className="deleteVideo">删除</Button>
                                        </Popconfirm>

                                        <Button icon="edit" type="primary" onClick={this.btnModifyImage.bind(this, item.id, item.order, item.content, item)}>修改图文</Button>
                                    </div>
                                </Spin>
                                {
                                    this.renderSortButton(item, index)
                                }
                            </div>)
                      })
                  }
                  <div className="divFooter" style={{paddingBottom: '10px'}}>
                    <Button icon="picture" type="primary" disabled={this.state.isAddImageAndTextLock} onClick={this.btnShowImage.bind(this, true)}>添加图文</Button>
                    <Button icon="mobile" type="primary" disabled={this.state.isAddImageAndTextLock} style={{marginLeft: '10px'}} onClick={this.btnShowPreviewArticle.bind(this, true)}>预览文章</Button>
                  </div>
              </div>
            </Spin>
        }
        //相关推荐（History）
        var historyElement = <Button icon="link" type="primary" onClick={()=>{this.state.isAddOrEdite = 'Add'; this.btnShowArticleList(true);}}>添加相关推荐</Button>;
        if(this.state.serviceVideoData != null && this.state.serviceVideoData.articleConnections.length >= 1){
            historyElement = <div className="divPastArticles">
                <Spin spinning={this.state.deleteReviewOfThePast} tip="删除中...">
                    {
                      this.state.serviceVideoData.articleConnections.map((item, index)=>{
                        return (<div className="divPastItem" key={index}>
                            <div className="divText" title={item.description} onClick={this.btnSelectAndEdit.bind(this, item)}>{item.description}</div>
                            <Popconfirm title="你确定要 删除 吗?" onConfirm={this.btnRemoveReviewOfThePast.bind(this, item.id)} placement="topRight">
                                <Button type="danger" size="small">删 除</Button>
                            </Popconfirm>
                        </div>)
                      })
                    }
                    <div style={{marginTop: '10px', textAlign: 'center'}}>
                        <Button icon="link" type="primary" onClick={()=>{this.state.isAddOrEdite = 'Add'; this.btnShowArticleList(true);}}>添加相关推荐</Button>
                    </div>
                </Spin>
            </div>
        }
        //参与报名（Sign Up）
        var signUpElement = null;
        if(this.state.serviceVideoData != null){
          signUpElement = <div className="divSignUp">
              <Button icon="team" type="primary" style={{display: 'inline-block', marginLeft: '239px'}} onClick={this.btnShowSignUpList.bind(this, this.state.serviceVideoData.article.id)}>报名列表</Button>
          </div>
        }
        //评论信息
        var commentInfo = null; var commentEmptyData = null;
        if (this.state.serviceVideoData != null) {
            if(this.state.serviceVideoData.comments == undefined || this.state.serviceVideoData.comments.length <= 0){
                commentEmptyData = <div className="divCommentEmptyData">当前没有评论</div>
            }
            commentInfo = <div className="divArticleItem">
                <div className="divTitle"><Icon type="message" className="iconComment"/>评论信息（{this.state.serviceVideoData.article.commentIds.length}）</div>
                {commentEmptyData}
                <TableView ref="tableView" data={this.state.serviceVideoData.comments}
                           renderRow={this.renderRow.bind(this)} pageNum={5}/>
            </div>
        }
        var btnEnableSorting = null;
        if (this.state.serviceVideoData != null) {
          if(this.state.serviceVideoData.imageTextList != undefined){
            if(this.state.serviceVideoData.imageTextList.length >= 2){
              btnEnableSorting = <Tooltip placement="topLeft" title="启用、关闭 排序">
                <Button id="btnEnableSorting" icon="swap" type="dashed" loading={this.state.orderUpdating} ghost={!this.state.sortIsEnabled} shape="circle" className="btnEnableSorting" onClick={this.enableSorting.bind(this)}></Button>
              </Tooltip>
            }
          }
        }
        var imgTitle = <label>预览文章（iPhone 6）</label>;
        if(this.state.serviceVideoData != null && this.state.serviceVideoData.article != undefined && this.state.serviceVideoData.article.coverPicture != undefined){
            imgTitle = <label><img src={this.state.serviceVideoData.article.coverPicture} className="imgTitle"/>预览文章（iPhone 6）</label>
        }
        return (
            <div>
                  <div className="divArticleContent">
                      <div className="divArticleItem">
                          <div className="divTitle"><Icon type="idcard" className="iconPlate" />基础信息</div>
                          <div className="divSelectType1" style={{float: (baseFlap == false)?'left':'initial'}}>
                            <Tooltip placement="bottomLeft" title="“修改基础信息” 里修改类型">
                                类型：<Select value={typeData[this.state.editTypeIndex]} style={{ minWidth: 120, maxWidth: 213 }} className="selectType" disabled={true}>
                                  {typeOptions}
                                </Select>
                                二级类型：<Select value={this.state.editSubTypeData} style={{ minWidth: 120 }} labelInValue={true} disabled={true}>
                                  {subTypeOptions}
                                </Select>
                            </Tooltip>
                          </div>
                          {baseElement}
                      </div>
                      <div className="divArticleItem">
                          <div className="divTitle"><Icon type="camera-o" className="iconPlate" />视频信息</div>
                          {videoElement}
                      </div>
                      <div className="divArticleItem">
                          <div className="divTitle"><Icon type="customer-service" className="iconPlate" />音频信息</div>
                          {audioElement}
                      </div>
                      <div className="divArticleItem" style={{paddingBottom: '0px'}}>
                          <div id="divImageTextTitle" className="divTitle" style={{marginBottom: '0px'}}>
                            <Icon type="solution" className="iconSolution"/>图文信息（{(this.state.serviceVideoData == null)?0:((this.state.serviceVideoData.imageTextList == undefined)?0:this.state.serviceVideoData.imageTextList.length)}）
                            {btnEnableSorting}
                          </div>
                          {imageElement}
                      </div>
                      <div className="divArticleItem" style={{display: ((this.state.serviceVideoData == null)?'none':'block')}}>
                          <div className="divTitle">
                            <Icon type="link" className="iconPlate" />相关推荐（{(this.state.serviceVideoData == null)?0:this.state.serviceVideoData.articleConnections.length}）
                          </div>
                          {historyElement}
                      </div>
                      <div className="divArticleItem" style={{display: ((this.state.serviceVideoData == null)?'none':'block')}}>
                          <div className="divTitle">
                            <Icon type="bulb" className="iconPlate" style={{marginLeft: "-248px"}} />活动预约报名
                          </div>
                          {signUpElement}
                      </div>
                      {commentInfo}
                  </div>
                  <Modal
                    title="添加：视频"
                    visible={this.state.updateVisible}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.btnShowVideo.bind(this, false)}
                    width="697px"
                    maskClosable={false}
                  >
                      <Spin spinning={this.state.updateVideoLoading} tip={this.state.updateVideoProgress}>
                          <div className="divVideo">
                              <div className="divItem">
                                  <span>作者：</span>
                                  <Tooltip placement="topRight" title="请输入作者（非必填）">
                                      <Input placeholder="请输入作者" value={this.state.videoData.author} className="iptAuthor" onChange={(val)=>this.onChangeText(val)}/>
                                  </Tooltip>
                              </div>
                              <div className="divItem">
                                  <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>输入内容（富文本）：</div>
                                  <Tooltip placement="bottomLeft" title="请输入内容（非必填）">
                                      <div id="divVideo" style={{height: '250px', width: '663px'}} className="wangEditor-txt"></div>
                                  </Tooltip>
                                  <div style={{color: 'red', marginTop: '7px', textAlign: 'left'}}>* 请勿输入 XAML 扩展标签，如：&lt;x:smart&gt; 这一类的标签。</div>
                              </div>
                              <Button icon="close" className="btnClose" onClick={this.btnShowVideo.bind(this, false)}>取 消</Button>
                              <Tooltip placement="bottomRight" title="请选择视频">
                                  <input id="fileVideo" type="file" className="inputFile" onChange={this.beforeUploadVideo.bind(this)}/>
                                  <Button icon="video-camera" type="primary" className="btnClose btnGreen">选择视频</Button>
                              </Tooltip>
                              <Tooltip placement="bottomRight" title="保存信息">
                                  <Button icon="upload" type="primary" onClick={this.btnSaveVideoInfo.bind(this)}>保 存</Button>
                              </Tooltip>
                          </div>
                      </Spin>
                  </Modal>
                  <Modal
                    title="添加：音频"
                    visible={this.state.updateAudioVisible}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.btnShowAudio.bind(this, false)}
                    width="697px"
                    maskClosable={false}
                  >
                      <Spin spinning={this.state.updateAudioLoading} tip={this.state.updateAudioProgress}>
                          <div className="divVideo">
                              <div className="divItem" style={{display: 'flex', alignItems: 'center'}}>
                                  <span>标题：</span>
                                  <Tooltip placement="topRight" title="请输入标题（必填）">
                                      <Input placeholder="请输入标题" value={this.state.audioData.title} className="iptAuthor" onChange={(val)=>this.onChangeAudioText(val, 'title')}/>
                                  </Tooltip>
                                  <span style={{paddingLeft: '10px'}}>作者：</span>
                                  <Tooltip placement="topRight" title="请输入作者（必填）">
                                      <Input placeholder="请输入作者" value={this.state.audioData.author} className="iptAuthor" onChange={(val)=>this.onChangeAudioText(val, 'author')}/>
                                  </Tooltip>
                              </div>
                              <div className="divItem">
                                  <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>输入摘要：</div>
                                  <Tooltip placement="bottomLeft" title="请输入摘要（非必填）">
                                      <Input type="textarea" rows={4} placeholder="请输入摘要..." style={{width: '663px',height: '116px'}} value={this.state.audioData.content} className="iptAuthor" onChange={(val)=>this.onChangeAudioText(val, 'content')}/>
                                  </Tooltip>
                              </div>
                              <Button icon="close" className="btnClose" onClick={this.btnShowAudio.bind(this, false)}>取 消</Button>
                              <Tooltip placement="bottomRight" title="请选择音频">
                                  <input id="fileAudio" type="file" className="inputFileAudio" onChange={this.beforeUploadAudio.bind(this)}/>
                                  <Button icon="customer-service" type="primary" className="btnClose btnGreen">选择音频（MP3）</Button>
                              </Tooltip>
                              <Tooltip placement="bottomRight" title="保存信息">
                                  <Button icon="upload" type="primary" onClick={this.btnSaveAudioInfo.bind(this)}>保 存</Button>
                              </Tooltip>
                          </div>
                      </Spin>
                  </Modal>
                  <Modal
                    title="上传封面图"
                    visible={this.state.updateThumbnailVisible}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={()=>{this.setState({updateThumbnailVisible: false})}}
                    width="400px"
                    maskClosable={false}
                  >
                      <Spin spinning={this.state.updateThumbnailLoading} tip={this.state.updateCoverPicProgress}>
                          <div className="divVideo">
                              <div className="divItem">
                                  <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>选择封面图：</div>
                                  <img id="imgThumbnail" src="/src/styles/images/disk.png" />
                              </div>
                              <Tooltip placement="bottomRight" title="请选择封面图">
                                  <input id="fileThumbnail" type="file" className="inputFileAudio" onChange={this.beforeUploadThumbnail.bind(this)}/>
                                  <Button icon="picture" type="primary" className="btnClose btnGreen">选择封面图</Button>
                              </Tooltip>
                              <Tooltip placement="bottomRight" title="保存信息">
                                  <Button icon="upload" type="primary" onClick={this.btnSaveThumbnailInfo.bind(this)}>保 存</Button>
                              </Tooltip>
                          </div>
                      </Spin>
                  </Modal>
                  <Modal
                    title="添加：图文"
                    visible={this.state.updateImageVisible}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.btnShowImage.bind(this, false)}
                    width="918px"
                    maskClosable={false}
                  >
                      <Spin spinning={this.state.updateImageLoading} tip={this.state.updateImageAndTextProgress}>
                          <div className="divVideo">
                              <div style={{display: 'flex'}}>
                                  <div className="divItem">
                                      <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>图片预览：</div>
                                      <img id="imgImageAndText" src="/src/styles/images/default.jpg" className="divImgDefault" />
                                  </div>
                                  {
                                    <div className="divItem">
                                        <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>输入内容（富文本）：</div>
                                        <Tooltip placement="bottomLeft" title="请输入内容（非必填）">
                                            <div id="divImgText" style={{height: '250px', width: '665px'}} className="wangEditor-txt"></div>
                                        </Tooltip>
                                        <div style={{color: 'red', marginTop: '7px', textAlign: 'left'}}>* 请勿输入 XAML 扩展标签，如：&lt;x:smart&gt; 这一类的标签。</div>
                                    </div>
                                  }
                              </div>

                              <Button icon="close" className="btnClose" onClick={this.btnShowImage.bind(this, false)}>取 消</Button>
                              <Tooltip placement="bottomRight" title="选择图片（可选）">
                                  <input id="filePic" type="file" className="inputFile" onChange={this.beforeUploadImage.bind(this)}/>
                                  <Button icon="picture" type="primary" className="btnClose btnGreen">选择图片</Button>
                              </Tooltip>
                              <Tooltip placement="bottomRight" title="保存信息">
                                  <Button icon="upload" type="primary" onClick={this.btnSaveRichText.bind(this)}>保 存</Button>
                              </Tooltip>
                          </div>
                      </Spin>
                  </Modal>
                  <Modal
                    title="添加：基础信息"
                    visible={this.state.updateBaseVisible}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.btnShowBaseInfo.bind(this, false)}
                    width="auto"
                    maskClosable={false}
                  >
                      <Spin spinning={this.state.updateBaseLoading} tip={this.state.updateBaseProgress}>
                          <div className="divVideo">
                              <div className="divFlexRow">
                                  <div className="divItem">
                                      <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>选择封面图：</div>
                                      <Tooltip placement="bottomLeft" title="封面图（必选）">
                                        <input id="basePic" type="file" className="inputFile inputFileNewWidth" onChange={this.verifyMsgAndImage.bind(this)} />
                                        <img id="imgArticleThumbnail" src="/src/styles/images/default.jpg" />
                                      </Tooltip>
                                  </div>
                                  <div className="divFlexColumn">
                                      <div className="divItem" style={{textAlign: 'left'}}>
                                          <span>类型：</span>
                                          <Select value={typeData[this.state.editTypeIndex]} style={{ minWidth: 120 }} className="selectType" onChange={this.onTypeChange.bind(this)}>
                                            {typeOptions}
                                          </Select>
                                          &nbsp;&nbsp;二级类型：<Select value={this.state.editSubTypeData} defaultActiveFirstOption={false} style={{ minWidth: 120 }} labelInValue={true} onChange={this.onSubTypeChange.bind(this)}>
                                            {editSubTypeOptions}
                                          </Select>
                                      </div>
                                      <div className="divItem">
                                          <span>标题：</span>
                                          <Tooltip placement="topRight" title="请输入标题（必填）">
                                              <Input placeholder="请输入标题" value={this.state.baseData.title} style={{width: '535px'}} className="iptAuthor" onChange={(val)=>this.onChangeBaseInfo(val, "title")}/>
                                          </Tooltip>
                                      </div>
                                      <div className="divItem">
                                          <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>输入摘要：</div>
                                          <Tooltip placement="bottomLeft" title="请输入内容（必填）">
                                              <Input placeholder="请输入摘要" value={this.state.baseData.summary} style={{width: '571px',height: '61px'}} className="iptAuthor" type="textarea" rows={4} onChange={(val)=>this.onChangeBaseInfo(val, "summary")}/>
                                          </Tooltip>
                                      </div>
                                  </div>
                              </div>

                              <Button icon="close" className="btnClose" onClick={this.btnShowBaseInfo.bind(this, false)}>取 消</Button>
                              <Tooltip placement="bottomRight" title="保存信息">
                                  <Button icon="upload" type="primary" className="btnClose" onClick={this.btnSaveBaseInfo.bind(this)}>保 存</Button>
                              </Tooltip>
                          </div>
                      </Spin>
                  </Modal>
                  <Modal
                    title={"回复：" + this.state.replayCommentName}
                    visible={this.state.showAdminReplay}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.showAdminReplay.bind(this, false, null)}
                    width="auto"
                    maskClosable={false}
                  >
                      <div className="divVideo">
                          <div className="divItem">
                              <div style={{textAlign: 'left', marginBottom: '6px', paddingLeft: '1px'}}>输入内容：</div>
                              <Tooltip placement="bottomLeft" title="请输入内容（必填）">
                                  <Input placeholder="请输入内容" value={this.state.replyContent} style={{width: '571px',height: '100px'}} className="iptAuthor" type="textarea" rows={4} onChange={(val)=>this.setReplyContent(val)}/>
                              </Tooltip>
                          </div>

                          <Button icon="close" className="btnClose" onClick={this.showAdminReplay.bind(this, false, null)}>取 消</Button>
                          <Button icon="export" type="primary" className="btnClose" onClick={this.btnReplayComment.bind(this)}>回复评论</Button>
                      </div>
                  </Modal>
                  <Modal
                    title={"评论详情" + this.state.commentStatus}
                    visible={this.state.showCommentDetails}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.showCommentDetails.bind(this, false, null)}
                    width="auto"
                    maskClosable={false}
                  >
                      <div className="divVideo" style={{backgroundColor: '#F7F7F7'}}>
                          {this.renderCommentDetail()}
                          <div style={{textAlign: 'right'}}>
                            <Button icon="close" className="btnClose" onClick={this.showCommentDetails.bind(this, false, null)}>取 消</Button>
                          </div>
                      </div>
                  </Modal>
                  <Modal
                    title={"文章点赞（" + ((this.state.articleThumbsUpsDetail == null)?"0":this.state.articleThumbsUpsDetail.length) + " 个）"}
                    visible={this.state.showArticleThumbsUps}
                    wrapClassName="vertical-center-modal"
                    footer={null}
                    onCancel={this.showArticleThumbsUps.bind(this, false)}
                    width="auto"
                    maskClosable={false}
                  >
                      <div className="divVideo">
                          {this.renderThumbsUpsByArticle()}
                          <div style={{textAlign: 'right'}}>
                            <Button icon="close" className="btnClose" onClick={this.showArticleThumbsUps.bind(this, false)}>取 消</Button>
                          </div>
                      </div>
                  </Modal>
                  <Modal
                      title="请选择关联文章"
                      visible={this.state.articleList}
                      wrapClassName="center-modal"
                      footer={null}
                      onCancel={this.btnShowArticleList.bind(this, false)}
                      width="100%"
                      maskClosable={false}
                      style={{margin: 'initial'}}
                  >
                      <Spin spinning={this.state.updateLoading} wrapperClassName="spinUpdateLoading" tip={this.state.relatedArticleTip}>
                          <div className="divType" style={{overflow: 'hidden'}}>
                              类型：<Select defaultValue={typeData[0]} style={{ width: 120 }} onChange={this.onTypeChangeAssociation.bind(this)} className="selectType">
                                {selectTypeOptions}
                              </Select>&nbsp;
                              二级类型：<Select defaultValue={this.state.subTypeData} style={{ width: 120 }} labelInValue={true} onChange={this.onSelectType.bind(this)}>
                                {selectSubTypeOptions}
                              </Select>&nbsp;&nbsp;&nbsp;
                              <span className="spanTextDesc">推荐链接描述：</span>
                              <Tooltip placement="bottomLeft" title="请输入描述，默认文章标题。">
                                <Input placeholder="请输入描述内容..." value={this.state.pastDescription} style={{maxWidth: '500px',width: this.state.width}} className="iptAuthor" onChange={(val)=>this.setPastDescription(val)}/>
                              </Tooltip>
                          </div>
                          {serverArticleList}
                          <TableView ref="articleListView" totalPageNum={this.totalPageNum.bind(this)}
                                 isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                                 isService={true} searchObject={this.state.searchObject} pageNum={10}
                                 url={this.state.findArticle} method="post" sort="commitTime" dir="DESC" onPageSelected={()=>{this.setState({updateLoading: true})}}/>
                      </Spin>
                      <div style={{ position: 'fixed', right: '18px', bottom: '18px'}}>
                          <Button icon="close" style={{marginRight: '15px'}} onClick={this.btnShowArticleList.bind(this, false)}>取 消</Button>
                          <Tooltip placement="bottomRight" title="选择：相关推荐">
                              <Button icon="link" type="primary" className="btnClose" onClick={this.btnRelatedArticle.bind(this)}>关联此文章</Button>
                          </Tooltip>
                      </div>
                  </Modal>
                  <Modal
                      title={"已选中家长（" + this.state.tempParentsData.length + "）"}
                      visible={this.state.showParentsList}
                      wrapClassName="center-modal"
                      footer={null}
                      onCancel={this.btnShowParentsList.bind(this, false)}
                      width="100%"
                      maskClosable={false}
                      style={{margin: 'initial'}}
                  >
                      <Spin spinning={this.state.sendingNotifications} wrapperClassName="spinUpdateLoading" tip="通知，发送中...">
                          <div style={{display: ((this.state.isSendNotifNext)?"block":"none")}}>
                            <div className="divType" style={{overflow: 'hidden', textAlign: 'right'}}>
                                是否全选：<Switch checkedChildren="开" unCheckedChildren="关" checked={((this.state.parentsListData.length <= 0)?false:this.state.parentsListData.length == this.state.tempParentsData.length)} onChange={this.onChangeBySwitch.bind(this)}/>
                            </div>
                            {serverParentsList}
                          </div>

                          <div style={{display: ((this.state.isSendNotifNext)?"none":"block")}}>
                            <div className="divNoticeTemplate">
                                <div className="divTitle">
                                  <img src="/src/styles/images/replyLogo.png"/> CambridgeNetwork家长汇
                                </div>
                                <div className="divContentTitle">
                                  <label>文章更新通知</label>
                                  <label className="labelTimer">{this.state.labelTimer}</label>
                                </div>
                                <div className="divContentFields">
                                  <div className="divItem">
                                    文章标题<Input style={{display: (this.state.isPreview)?"none":"inline-block"}} placeholder="请输入文章标题..." value={this.state.tempInfoData.title} onChange={this.onChangeByTempInfo.bind(this, 'title')} />

                                  </div>
                                  <div className="divItem">
                                    分类<Input style={{marginLeft: 34, display: (this.state.isPreview)?"none":"inline-block"}} placeholder="请输入分类..." value={this.state.tempInfoData.type} onChange={this.onChangeByTempInfo.bind(this, 'type')} />

                                  </div>
                                  <div className="divItem">
                                    文章概括<Input style={{display: (this.state.isPreview)?"none":"inline-block"}} placeholder="请输入文章概括..." type="textarea" rows={2} value={this.state.tempInfoData.summary} onChange={this.onChangeByTempInfo.bind(this, 'summary')}/>

                                  </div>
                                  <div className="divItem">
                                    备注<Input style={{marginLeft: 34, display: (this.state.isPreview)?"none":"inline-block"}} placeholder="请输入备注..." type="textarea" rows={3} value={this.state.tempInfoData.Remark} onChange={this.onChangeByTempInfo.bind(this, 'Remark')}/>
                                  </div>
                                </div>
                                <div className="divFooter">
                                  查看详情<Icon type="right" className="iconRight"/>
                                </div>
                            </div>
                            <div style={{marginTop: '20px'}}>
                              <Button icon="left" style={{marginRight: '20px'}} onClick={this.btnSendNotifNext.bind(this, true)}>返回</Button>
                              <Button icon="mail" type="primary" style={{display: ((this.state.isSendNotifNext)?"none":"inline-block")}} className="btnClose" onClick={this.btnSendNotification.bind(this)}>推送通知</Button>
                            </div>
                          </div>
                      </Spin>
                      <div style={{ position: 'fixed', right: '18px', bottom: '18px'}}>
                          <Button icon="close" style={{marginRight: '15px'}} onClick={this.btnShowParentsList.bind(this, false)}>取 消</Button>
                          <Tooltip placement="bottomRight" title="请选择家长">
                              <Button icon="arrow-right" type="primary" style={{display: ((this.state.isSendNotifNext)?"inline-block":"none")}} className="btnClose" onClick={this.btnSendNotifNext.bind(this, false)}>下一步</Button>
                          </Tooltip>
                      </div>
                  </Modal>
                  <Modal
                      title={imgTitle}
                      visible={this.state.showPreviewArticle}
                      wrapClassName="vertical-center-modal"
                      footer={null}
                      onCancel={this.btnShowPreviewArticle.bind(this, false)}
                      width="auto"
                      maskClosable={false}
                  >
                      <div className="divPreviewArticle">
                        <iframe id="previewIframe" src="" />
                      </div>
                  </Modal>
            </div>
        )
    }
}
