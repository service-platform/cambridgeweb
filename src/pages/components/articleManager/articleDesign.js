import React from 'react';
import { withRouter } from 'react-router';
import { Steps, Button, message } from 'antd';
const Step = Steps.Step;
import SelectType from './selectType';
import DesignArticle from './designArticle';

class articleDesign extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectIndex: 0,
            selectType: "1",
            id: null
        }
    }
    componentDidMount(){
        this.props.router.setRouteLeaveHook(this.props.route, this.routerWillLeave);

        if(this.props.routeParams.id != "add"){
            this.setState({
                id: this.props.routeParams.id.split(',')[0],
                selectIndex: 1,
                selectType: this.props.routeParams.id.split(',')[1]
            })
        }
    }
    routerWillLeave(nextLocation) {
        if(nextLocation.pathname.indexOf("/articleDesign") == -1){
          return '你确认要离开 文章设计 吗？';
        }
    }
    onSelectType(value){
        this.setState({
            selectIndex: (this.state.selectIndex + 1),
            selectType: (value.value == undefined)?value.key:value.value
        });
    }
    gotoFinish(){
        this.setState({
          id: this.props.routeParams.id.split(',')[0],
          selectIndex: 2,
          selectType: this.props.routeParams.id.split(',')[1]
        });
    }
    render() {
        if(this.props.params.articleId == undefined){
            var Component = null;
            if(this.state.selectIndex == 0){
                Component = <SelectType selectType={this.onSelectType.bind(this)}/>
            }else if(this.state.selectIndex == 1){
                Component = <DesignArticle routeParams={this.props.routeParams} selectType={this.state.selectType} id={this.state.id} gotoFinish={this.gotoFinish.bind(this)}/>
            }else if(this.state.selectIndex == 2){
                Component = <DesignArticle routeParams={this.props.routeParams} selectType={this.state.selectType} id={this.state.id} gotoFinish={this.gotoFinish.bind(this)}/>
            }
            return (
                <div>
                    <div className="divSteps">
                        <Steps current={this.state.selectIndex}>
                            <Step icon="bars" key="0" title="选择类型" />
                            <Step icon="edit" key="1" title="设计内容" />
                            <Step icon="check-circle-o" key="2" title="完成提交" />
                        </Steps>
                    </div>
                    <div className="">
                        {Component}
                    </div>
                </div>
            )
        }else {
            return(this.props.children)
        }
    }
}
export default withRouter(articleDesign);
