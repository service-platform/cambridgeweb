import React from 'react';
import { Select, Button, Form, Icon } from 'antd';
import { browserHistory } from 'react-router';
const { Option, OptGroup } = Select;
const FormItem = Form.Item;

const typeData = ['热门活动', '留学须知', '学生风采', '家长必读'];
const subTypeData = {
  '热门活动': [{key: '线上活动',value: "1"}, {key: '线下活动',value: "2"},{key: '剑桥快报',value: "3"}],
  '留学须知': [{key: '学业篇',value: "4"}, {key: '法律篇',value: "5"}, {key: '文化篇',value: "6"}, {key: '生活篇',value: "7"}],
  '学生风采': [{key: '每周播报',value: "8"}, {key: '美高捷报',value: "9"}],
  '家长必读': [{key: '在线培训',value: "10"}, {key: '避雷指南',value: "11"}, {key: 'CIIE洞察',value: "12"}]
};
const academicSupportTypeData = ['大学预备语言和文化（CPLC）线上课程', '大学预备语言和文化（CPLC）线下课程', '私教辅导'];
const academicSupportSubTypeData = {
  '大学预备语言和文化（CPLC）线上课程': [{key: '课程介绍',value: "16"}, {key: '教师简介',value: "17"}, {key: '学生成功案例',value: "18"}, {key: '常见问题与解答',value: "19"}],
  '大学预备语言和文化（CPLC）线下课程': [{key: '课程介绍',value: "20"}, {key: '教师简介',value: "21"}, {key: '学生成功案例',value: "22"}, {key: '常见问题与解答',value: "23"}],
  '私教辅导': [{key: '学科辅导介绍',value: "24"}, {key: '标准化考试备考 （托福，SAT, ACT）',value: "25"}, {key: '一对一辅导介绍',value: "26"}, {key: '教师简介',value: "27"}, {key: '成功案例',value: "28"}, {key: '常见问题与解答', value: "29"}]
};
const travelData = [{key: '走遍美国',value: "13"}, {key: '假期游学',value: "14"}, {key: '访校之旅',value: "30"}, {key: '其他服务',value: "15"}];

class selectType extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          typeData: subTypeData[typeData[0]],
          subTypeData: subTypeData[typeData[0]][0],
          travelData: travelData[0],
          isTravel: 0
      }
  }
  componentWillMount(){
    this.state.isTravel = location.href.split('/')[location.href.split('/').length-1];
    if(this.state.isTravel == "0"){
        this.setState({
          typeData: subTypeData[typeData[0]],
          subTypeData: subTypeData[typeData[0]][0]
        })
    }else if(this.state.isTravel == "1"){
        //不用处理
    }else if(this.state.isTravel == "2"){
        this.setState({
          typeData: academicSupportSubTypeData[academicSupportTypeData[0]],
          subTypeData: academicSupportSubTypeData[academicSupportTypeData[0]][0]
        })
    }
  }
  onTypeChange(value){
    if(this.state.isTravel == "0"){
        this.setState({
          typeData: subTypeData[value],
          subTypeData: subTypeData[value][0],
        });
    }else if(this.state.isTravel == "1"){
        //不用处理
    }else if(this.state.isTravel == "2"){
        this.setState({
          typeData: academicSupportSubTypeData[value],
          subTypeData: academicSupportSubTypeData[value][0]
        })
    }
  }
  formNextStep(e){
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
          if (!err) {
              this.props.selectType(values.subType);
          }
      });
  }
  gotoArticleList(){
      if(this.state.isTravel == "0" || this.state.isTravel == "1" || this.state.isTravel == "2"){
          browserHistory.push("/articleList/" + this.state.isTravel);
      }else {
          browserHistory.push("/articleList/1");
      }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const typeOptions = (this.state.isTravel == "2")?academicSupportTypeData.map(type => <Option key={type}>{type}</Option>):typeData.map(type => <Option key={type}>{type}</Option>);
    const subTypeOptions = this.state.typeData.map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);
    const travelOptions = travelData.map(item => <Option key={item.key} value={item.value}>{item.key}</Option>);
    const formItemLayout = {
        labelCol: { span: 11 },
        wrapperCol: { span: 9 }
    }
    var selectItems = null;
    if(this.state.isTravel == "1"){
        selectItems = <div>
          <FormItem label="旅游项目：" {...formItemLayout}>
              {getFieldDecorator("subType",{
                rules: [{ required: true, message: '请选类型!' }],
                initialValue: this.state.travelData
              })(
                  <Select style={{ width: 120 }} labelInValue={true}>
                    {travelOptions}
                  </Select>
              )}
          </FormItem>
        </div>
    }else if(this.state.isTravel == "0"){
        selectItems = <div>
          <FormItem label="选择类型：" {...formItemLayout}>
              {getFieldDecorator("type",{
                rules: [{ required: true, message: '请选择类型!' }],
                initialValue: typeData[0]
              })(
                  <Select style={{ width: 120 }} onChange={this.onTypeChange.bind(this)}>
                    {typeOptions}
                  </Select>
              )}
          </FormItem>
          <FormItem label="选择子类型：" {...formItemLayout}>
              {getFieldDecorator("subType",{
                rules: [{ required: true, message: '请选择子类型!' }],
                initialValue: this.state.subTypeData
              })(
                  <Select style={{ width: 120 }} allowClear={true} labelInValue={true}>
                    {subTypeOptions}
                  </Select>
              )}
          </FormItem>
        </div>
    }else if(this.state.isTravel == "2"){
      selectItems = <div>
        <FormItem label="选择类型：" {...formItemLayout}>
            {getFieldDecorator("type",{
              rules: [{ required: true, message: '请选择类型!' }],
              initialValue: academicSupportTypeData[0]
            })(
                <Select style={{ width: 239 }} onChange={this.onTypeChange.bind(this)}>
                  {typeOptions}
                </Select>
            )}
        </FormItem>
        <FormItem label="选择子类型：" {...formItemLayout}>
            {getFieldDecorator("subType",{
              rules: [{ required: true, message: '请选择子类型!' }],
              initialValue: this.state.subTypeData
            })(
                <Select style={{ width: 239 }} allowClear={true} labelInValue={true}>
                  {subTypeOptions}
                </Select>
            )}
        </FormItem>
      </div>
    }
    return (
      <div className="divArticleContent">
        <div className="divArticleItem">
            <div className="divTitle"><Icon type="bars" className="iconPlate" style={{marginLeft: '-241px'}} />请选择文章类型</div>
            <div className="divSelectType">
              <Form layout="vertical" onSubmit={this.formNextStep.bind(this)} className="formDiv">
                {selectItems}
                <div>
                    <Button icon="arrow-left" className="btnBackStep" onClick={this.gotoArticleList.bind(this)}>返 回</Button>
                    <Button type="primary" icon="arrow-down" htmlType="submit" className="btnNextStep">下一步</Button>
                </div>
              </Form>
            </div>
         </div>
       </div>
    );
  }
}
selectType = Form.create()(selectType)
export default selectType;
