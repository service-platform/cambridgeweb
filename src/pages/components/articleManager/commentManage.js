import React from 'react';
import { Modal, Spin, Button, Select, Input, Tooltip, Icon, notification, Popover } from 'antd';
import * as actions from './../../../actions/videoAction';
import TableView from './../TableView';
const { Option } = Select;

class commentManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findComments: GLOBAL.baseURL + "/webAPI/comment/findComments",
            searchObject: {},
            serviceData: [],
            sortStr: "commentDate",
            replayCommentName: '',
            replyContent: '',
            commentItem: null,
            content: <div><Icon type="loading" style={{marginRight: '10px'}} />加载中...</div>,
            replyContent: <div><Icon type="loading" style={{marginRight: '10px'}} />加载中...</div>,
            articleContent: <div><Icon type="loading" style={{marginRight: '10px'}} />加载中...</div>,
            spinningOpenDetails: false,

            commentStatus: "（状态：加载中）",
            showCommentDetails: false,
            commentDetail: null,
        }
    }
    showCommentDetails(val, cId){
        this.setState({showCommentDetails: val});
        if(cId != null){
            this.setState({commentDetail: null});
            actions.commentDetail(this, cId);
        }
    }
    setReplyContent(e){
        this.setState({replyContent: e.target.value});
    }
    onTypeChange(value){
        if(value == "1"){
            this.setState({sortStr: "commentDate"});
        }else if (value == "2") {
            this.setState({sortStr: "replyDate"});
        }
        var that = this;
        setTimeout(function(){
          that.refs["tableView"].refreshData();
        }, 500);
    }
    getServiceData(data) {
        this.setState({serviceData: data});
    }
    removeReplyComment(cId, e){
        e.stopPropagation();
        var that = this; var cId = cId;
        Modal.confirm({
          title: '提示：',
          content: '你确定要删除: 回复 吗？',
          onOk: function(){
            actions.removeReplyComment(that, cId);
          }
        });
    }
    btnReplayComment(e){
        e.stopPropagation();
        if(this.state.replyContent.trim() == ""){
            Modal.warning({
              title: "温馨提示：",
              content: "评论回复，不能为空。"
            });
        }else {
            var that = this;
            Modal.confirm({
              title: "提示：",
              content: "你确定要回复：" + this.state.replayCommentName + " 吗？",
              onOk: function(){
                that.state.commentItem.replyContent = that.state.replyContent;
                delete that.state.commentItem.whetherCommentThumbsUp;
                delete that.state.commentItem.whetherReplyThumbsUp;
                actions.replayComment(that, that.state.commentItem);
              }
            });
        }
    }
    showAdminReplay(value, item, e){
        e.stopPropagation();
        if(value == true){
            $("#div_" + item.id).css("display", "block");

            var replayCommentName = ((item.commentorName == undefined)?"匿名":item.commentorName);
            if(item.replyContent == undefined){
                this.setState({replayCommentName: replayCommentName, commentItem: item, replyContent: ''});
            }else {
                this.setState({replayCommentName: replayCommentName, commentItem: item, replyContent: item.replyContent});
            }
        }else if(value == false){
            $("#div_" + item.id).css("display", "none");
            this.setState({replyContent: '', replayCommentName: '', commentItem: null});
        }
    }
    changeCommentStatus(id, value, e){
        e.stopPropagation();
        var strTip = ""; var that = this;
        var id = id; var value = value;
        if(value == "active"){
            strTip = "你确定要 公开 评论吗？";
        }else{
            strTip = "你确定要 禁用 评论吗？";
        }
        Modal.confirm({
          title: '提示：',
          content: strTip,
          onOk: function(){
            actions.changeCommentStatus(that,{
              id: id,
              status: value
            });
          }
        });
    }
    onReplyVisibleChange(cId){
        this.setState({replyContent: <div><Icon type="loading" style={{marginRight: '10px'}} />加载中...</div>});
        actions.commentDetailByUserDetail(this, cId);
    }
    renderByUserDetail(item){
        this.setState({
          replyContent: <div>
            <div>姓名：{item.firstName + item.lastName}，性别：{(item.gender==1)?"男":"女"}，工作：{item.job}</div>
            <div>地址：{item.location}，{item.position}</div>
            <div>手机：{item.phoneNum}，电子邮件：{item.email}</div>
          </div>
        })
    }
    onVisibleChange(openId){
        this.setState({content: <div><Icon type="loading" style={{marginRight: '10px'}} />加载中...</div>});
        actions.findParentByOpenId(this, openId);
    }
    renderByOpenId(item){
        this.setState({
          content: <div>
            <div>姓名：{item.nickName}，性别：{(item.gender==1)?"男":"女"}，国家：{item.country}</div>
            <div>省份：{item.province}，城市：{item.city}</div>
            <div>关系：{item.relationship}，电子邮件：{item.email}</div>
            <div>手机：{(item.phones.length<=1)?item.phones[0]:item.phones[0] + " ..."}，授权日期：{new Date(item.joinTime).Format('yyyy-MM-dd hh:mm:ss')}</div>
          </div>
        })
    }
    onOpenArticleDetails(articleId, e){
        e.stopPropagation();
        this.setState({spinningOpenDetails: true});
        actions.findArticleByAId(this, articleId, false);
    }
    onArticleVisibleChange(articleId){
        this.setState({articleContent: <div><Icon type="loading" style={{marginRight: '10px'}} />加载中...</div>});
        actions.findArticleByAId(this, articleId, true);
    }
    renderByArticleDetail(item){
        this.setState({
          articleContent: <div style={{display: 'flex'}}>
            <img className="imgArticlePic" src={item.article.coverPicture} />
            <div>
              <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '296px', cursor: 'pointer'}} title={item.article.title}>标题：{item.article.title}</div>
              <div>作者：{item.createPeople.firstName + " " + item.createPeople.lastName}</div>
              <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '296px', cursor: 'pointer'}} title={item.article.summary}>摘要：{item.article.summary}</div>
              <div>浏览：{item.article.browseNumbers}，分享：{item.article.shareNumbers}，关注：{item.article.likeIds.length}</div>
              <div>创建时间：{new Date(item.article.commitTime).Format("yyyy-MM-dd hh:mm:ss")}</div>
            </div>
          </div>
        })
    }
    renderItem() {
      var trArray = [];
      var td1 = React.createElement('td', { className: 'tdName borderColor1', key: "td1"}, "留言");
      var td2 = React.createElement('td', { className: 'tdOperating borderColor1', key: "td2"}, "关联文章");
      var td3 = React.createElement('td', { className: 'tdOperating borderColor1', key: "td3"}, "留言时间");
      var td4 = React.createElement('td', { className: 'tdOperating borderColor1', key: "td4"}, "状态");
      var td5 = React.createElement('td', { className: 'tdOperating borderColor2', width: "62", key: "td5"}, "操作");
      var tr1 = React.createElement('tr', { className: 'trHeader', key: "tr1"}, [td1, td2, td3, td4, td5]);

      trArray.push(tr1);
      for (var i = 0; i < this.state.serviceData.length; i++) {
          var item = this.state.serviceData[i]; var index = i; var tr;
          var btnStatus; var imgHeader = null;

          if(item.avatarUrl == undefined){
              imgHeader = <Popover placement="topLeft" content={this.state.content} title="授权信息：" trigger="hover" onVisibleChange={this.onVisibleChange.bind(this, item.openId)}>
                <img src="/src/styles/images/default.jpg" className="imgAvatarUrl" />
              </Popover>
          }else{
              imgHeader = <Popover placement="topLeft" content={this.state.content} title="授权信息：" trigger="hover" onVisibleChange={this.onVisibleChange.bind(this, item.openId)}>
                <img src={item.avatarUrl} className="imgAvatarUrl" />
              </Popover>
          }
          if(item.status == "active"){
              btnStatus = <Tooltip placement="topLeft" title="点击：修改状态"><Button type="danger" className="btnStatus" onClick={this.changeCommentStatus.bind(this, item.id, "inactive")}>禁 用</Button></Tooltip>
          }else if(item.status == "inactive"){
              btnStatus = <Tooltip placement="topLeft" title="点击：修改状态"><Button type="primary" className="btnStatus" onClick={this.changeCommentStatus.bind(this, item.id, "active")}>公 开</Button></Tooltip>
          }
          if(item.replyId == undefined){
              tr = <tr key={item.id} className="trContent" onClick={this.showCommentDetails.bind(this, true, item.id)}>
                    <td className="tdName">
                      <div className="divComment">
                        {imgHeader}
                        <div className="divCommentContent">
                          <div title={item.commentorName} className="divText1">
                            {item.commentorName}
                          </div>
                          <div title={item.content} className="divText2">{item.content}</div>
                          <div title="评论点赞" className="spanArticleInfo" style={{marginRight: '10px', fontSize: '13px', float: 'right', marginTop: '-28px'}}><Icon type="like-o" style={{fontSize: '14px',marginRight:'3px', marginLeft: '5px'}}/>{item.commentThumbsUpIds.length}</div>
                        </div>
                      </div>
                      <div id={"div_" + item.id} className="divCommentFlex" style={{display: 'none'}}>
                          <Tooltip placement="bottomLeft" title="请输入内容（必填）">
                              <Input value={this.state.replyContent} onClick={(e)=>e.stopPropagation()} placeholder="请输入内容" style={{marginRight: '10px', height: '61px', resize: 'none'}} className="iptAuthor" type="textarea" rows={4} onChange={(val)=>this.setReplyContent(val)}/>
                              <div className="divFlexDisplay">
                                <Button className="btnCancel" type="primary" onClick={this.btnReplayComment.bind(this)}>确 定</Button>
                                <Button onClick={this.showAdminReplay.bind(this, false, item)}>取 消</Button>
                              </div>
                          </Tooltip>
                      </div>
                    </td>
                    <td className="tdOperating" style={{padding: '0', paddingTop: '17px'}}>
                      <Popover placement="top" content={this.state.articleContent} title="文章信息：" trigger="hover" onVisibleChange={this.onArticleVisibleChange.bind(this, item.articleId)}>
                        <div className="spanArticleInfo" title="点击查看详情" onClick={this.onOpenArticleDetails.bind(this, item.articleId)}><Icon type="file-text" />关联文章</div>
                      </Popover>
                    </td>
                    <td className="tdOperating" style={{paddingTop: '17px'}}>
                      <div title={new Date(item.commentDate).Format("yyyy-MM-dd hh:mm")} style={{color: '#8D8D8D',marginLeft: '-10px',marginRight: '-10px'}}>{new Date(item.commentDate).Format("yyyy-MM-dd hh:mm")}</div>
                    </td>
                    <td className="tdOperating" style={{paddingTop: '17px'}}>
                      <div>{btnStatus}</div>
                    </td>
                    <td className="tdOperating" style={{paddingTop: '17px'}}>
                      <Button type="primary" className="btnStatus" onClick={this.showAdminReplay.bind(this, true, item)}>回 复</Button>
                    </td>
                </tr>
                trArray.push(tr);
          }else {
                tr = <tr key={item.id} className="trContent" style={{borderBottom: 'none'}} onClick={this.showCommentDetails.bind(this, true, item.id)}>
                        <td className="tdName">
                          <div className="divComment">
                            {imgHeader}
                            <div className="divCommentContent">
                              <div title={item.commentorName} className="divText1">
                                {item.commentorName}
                              </div>
                              <div title={item.content} className="divText2">{item.content}</div>
                              <div title="评论点赞" className="spanArticleInfo" style={{marginRight: '10px', fontSize: '13px', float: 'right', marginTop: '-28px'}}><Icon type="like-o" style={{fontSize: '14px',marginRight:'3px', marginLeft: '5px'}}/>{item.commentThumbsUpIds.length}</div>
                            </div>
                          </div>
                        </td>
                        <td className="tdOperating" style={{padding: '0', paddingTop: '17px'}}>
                          <Popover placement="top" content={this.state.articleContent} title="文章信息：" trigger="hover" onVisibleChange={this.onArticleVisibleChange.bind(this, item.articleId)}>
                            <div className="spanArticleInfo" title="点击查看详情" onClick={this.onOpenArticleDetails.bind(this, item.articleId)}><Icon type="file-text" />关联文章</div>
                          </Popover>
                        </td>
                        <td className="tdOperating" style={{paddingTop: '17px'}}>
                          <div title={new Date(item.commentDate).Format("yyyy-MM-dd hh:mm")} style={{color: '#8D8D8D',marginLeft: '-10px',marginRight: '-10px'}}>{new Date(item.commentDate).Format("yyyy-MM-dd hh:mm")}</div>
                        </td>
                        <td className="tdOperating" style={{paddingTop: '17px'}}>
                          <div>{btnStatus}</div>
                        </td>
                        <td className="tdOperating" style={{paddingTop: '17px'}} width="62">
                          <span className="span">已回复</span>
                        </td>
                    </tr>
                trArray.push(tr);

                tr = <tr key={item.id + "_reply"} className="trContent" onClick={this.showCommentDetails.bind(this, true, item.id)}>
                    <td className="tdName">
                      <div className="divReply1">
                        <Popover placement="topLeft" content={this.state.replyContent} title="个人信息：" trigger="hover" onVisibleChange={this.onReplyVisibleChange.bind(this, item.id)}>
                          <img src="/src/styles/images/replyLogo.png" className="imgAvatarUrl" />
                        </Popover>
                        <div className="divCommentContent">
                          <div className="divText1">
                            你回复的内容（{new Date(item.replyDate).Format("yyyy-MM-dd hh:mm")}）
                            <label title="回复点赞" className="spanArticleInfo" style={{display:'none', fontSize: '13px',marginRight: '0px', marginLeft: 'initial', color: '#EF84DC'}}><Icon type="like-o" style={{fontSize: '14px',marginRight:'3px', marginLeft: '5px'}}/>{item.replyThumbsUpIds.length}</label>
                          </div>
                          <div title={item.replyContent} className="divText2">{item.replyContent}</div>
                        </div>
                      </div>
                    </td>
                    <td className="tdOperating">
                      <div className="divReply1 divReplyDelete" style={{marginTop: 0}}></div>
                    </td>
                    <td className="tdOperating">
                      <div className="divReply1 divReplyDelete" style={{marginTop: 0}}></div>
                    </td>
                    <td className="tdOperating">
                      <div className="divReply1 divReplyDelete" style={{marginTop: 0}}></div>
                    </td>
                    <td className="tdOperating" style={{paddingTop: '17px'}}>
                      <div className="divReply1 divReplyDelete">
                        <Button type="danger" className="btnStatus" onClick={this.removeReplyComment.bind(this, item.id)}>删 除</Button>
                      </div>
                    </td>
                </tr>
                trArray.push(tr);
            }
      }
      var tbody = React.createElement('tbody', null, trArray);
      var table = React.createElement('table', { className: 'tableRoles borderColor', style: {tableLayout: 'fixed', overflow: 'hidden'}, cellSpacing: '0', cellPadding: '0' }, tbody);
      return table;
    }
    renderCommentDetail(){
        var commentDetail = this.state.commentDetail;
        if(commentDetail == null){
          return(<div className="divLoading" style={{paddingBottom: '8px', width: '700px', backgroundColor: '#fff'}}><img src="/src/styles/images/timg.gif"/>数据加载中...</div>)
        }else if(commentDetail != null){
          var phoneLists = "无";
          if(commentDetail.commentParent.phones.length > 0){
            phoneLists = "";
            for (var i = 0; i < commentDetail.commentParent.phones.length; i++) {
                phoneLists += commentDetail.commentParent.phones[i] + "、";
            }
            phoneLists = phoneLists.substr(0, phoneLists.length - 1);
          }

          if(commentDetail.comment.replyId == undefined){
              var commentThumbsList = null; var commentThumbsUpCount = 0;
              if(commentDetail.commentThumbsUpParents == undefined){
                  commentThumbsList = <div className="divEmptData">无点赞</div>
                  commentThumbsUpCount = 0;
              }else {
                  commentThumbsUpCount = commentDetail.commentThumbsUpParents.length;
                  var colorIndex = 1;
                  commentThumbsList = commentDetail.commentThumbsUpParents.map((item, index)=>{
                      var avatar = null; var colorClass = "divItem divColor1";
                      if(item.avatarUrl == undefined || item.avatarUrl == ""){
                          avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                      }else {
                          avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                      }
                      if(colorIndex < 5){
                          colorIndex++
                          colorClass = "divItem divColor" + colorIndex;
                      }else {
                          colorIndex = 1;
                          colorClass = "divItem divColor" + colorIndex;
                      }
                      return(
                        <div key={index} className={colorClass}>
                            {avatar}
                            <div><span className="spanText">昵称：{item.nickName}，Email：{item.email}，性别：{(item.gender == 1)?"男":"女"}</span></div>
                            <div>关系：{item.relationship}，省份：{item.province}，城市：{item.city}</div>
                        </div>
                      )
                  });
              }
              return(
                  <div className="divCommentDetail">
                      <div className="divContainer">
                          <div className="divComment" style={{width: '530px'}}>
                              <img src="/src/styles/images/quotes.png" className="imgQuotes" />
                              <div className="divCommentContent">{commentDetail.comment.content}</div>
                              <div className="divOtherInfo">
                                {new Date(commentDetail.comment.commentDate).Format("yyyy-MM-dd")}
                              </div>
                              <img src="/src/styles/images/downArrow.png" className="imgDownArrow" />
                          </div>
                          <div className="divCommentParent">
                              <a href={"/parentsList/parentsDetail/" + commentDetail.commentParent.id} target="_blank" title="转到家长信息"><img src={commentDetail.commentParent.avatarUrl} /></a>
                              <div className="divContent">
                                <div>
                                  <span style={{color: '#3498DB', fontWeight: 'bold'}}>评论人</span>：{commentDetail.commentParent.nickName}， Email：{commentDetail.commentParent.email}， 性别：{(commentDetail.commentParent.gender == "1")?"男":"女"}
                                </div>
                                <div>
                                  国家：{commentDetail.commentParent.country}， 省份：{commentDetail.commentParent.province}， 城市：{commentDetail.commentParent.city}
                                </div>
                                <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
                                  最后登录时间：{((commentDetail.commentParent.lastLoginTime == undefined)?"":new Date(commentDetail.commentParent.lastLoginTime).Format("yyyy-MM-dd"))}，手机：<label style={{cursor: 'pointer'}} title={phoneLists}>{phoneLists}</label>
                                </div>
                              </div>
                          </div>
                          <div className="divThumbsList">
                              <div className="divTitle"><Icon type="message" style={{marginRight: '7px'}}/>评论：点赞（{commentThumbsUpCount} 个）</div>
                              <div className="divThumbsContent">
                                {commentThumbsList}
                              </div>
                          </div>
                      </div>
                </div>
              )
          }else {
              var commentThumbsList = null; var replyThumbsList = null; var commentThumbsUpCount = 0; var replyThumbsUpCount = 0;
              if(commentDetail.commentThumbsUpParents == undefined){
                  commentThumbsList = <div className="divEmptData">无点赞</div>
                  commentThumbsUpCount = 0;
              }else {
                  commentThumbsUpCount = commentDetail.commentThumbsUpParents.length;
                  var colorIndex = 1;
                  commentThumbsList = commentDetail.commentThumbsUpParents.map((item, index)=>{
                      var avatar = null; var colorClass = "divItem divColor1";
                      if(item.avatarUrl == undefined || item.avatarUrl == ""){
                          avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                      }else {
                          avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                      }
                      if(colorIndex < 5){
                          colorIndex++
                          colorClass = "divItem divColor" + colorIndex;
                      }else {
                          colorIndex = 1;
                          colorClass = "divItem divColor" + colorIndex;
                      }
                      return(
                        <div key={index} className={colorClass}>
                            {avatar}
                            <div><span className="spanText">昵称：{item.nickName}，Email：{item.email}，性别：{(item.gender == 1)?"男":"女"}</span></div>
                            <div>关系：{item.relationship}，省份：{item.province}，城市：{item.city}</div>
                        </div>
                      )
                  });
              }
              if(commentDetail.replyThumbsUpParents == undefined){
                  replyThumbsList = <div className="divEmptData">无点赞</div>
                  replyThumbsUpCount = 0;
              }else {
                  replyThumbsUpCount = commentDetail.replyThumbsUpParents.length;
                  replyThumbsList = commentDetail.replyThumbsUpParents.map((item, index)=>{
                      var avatar = null; var colorClass = "divItem divColor1";
                      if(item.avatarUrl == undefined || item.avatarUrl == ""){
                          avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                      }else {
                          avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                      }
                      if(colorIndex < 5){
                          colorIndex++
                          colorClass = "divItem divColor" + colorIndex;
                      }else {
                          colorIndex = 1;
                          colorClass = "divItem divColor" + colorIndex;
                      }
                      return(
                        <div key={index} className={colorClass}>
                            {avatar}
                            <div><span className="spanText">昵称：{item.nickName}，Email：{item.email}，性别：{(item.gender == 1)?"男":"女"}</span></div>
                            <div>关系：{item.relationship}，省份：{item.province}，城市：{item.city}</div>
                        </div>
                      )
                  });
              }
              return(
                  <div className="divCommentDetail">
                    <div className="divContainer">
                        <div className="divComment" style={{width: '530px'}}>
                            <img src="/src/styles/images/quotes.png" className="imgQuotes" />
                            <div className="divCommentContent">{commentDetail.comment.content}</div>
                            <div className="divOtherInfo">
                              {new Date(commentDetail.comment.commentDate).Format("yyyy-MM-dd")}
                            </div>
                            <img src="/src/styles/images/downArrow.png" className="imgDownArrow" />
                        </div>
                        <div className="divCommentParent">
                            <a href={"/parentsList/parentsDetail/" + commentDetail.commentParent.id} target="_blank" title="转到家长信息"><img src={commentDetail.commentParent.avatarUrl} /></a>
                            <div className="divContent">
                              <div>
                                <span style={{color: '#3498DB', fontWeight: 'bold'}}>评论人</span>：{commentDetail.commentParent.nickName}， Email：{commentDetail.commentParent.email}， 性别：{(commentDetail.commentParent.gender == "1")?"男":"女"}
                              </div>
                              <div>
                                国家：{commentDetail.commentParent.country}， 省份：{commentDetail.commentParent.province}， 城市：{commentDetail.commentParent.city}
                              </div>
                              <div style={{overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap'}}>
                                最后登录时间：{((commentDetail.commentParent.lastLoginTime == undefined)?"":new Date(commentDetail.commentParent.lastLoginTime).Format("yyyy-MM-dd"))}，手机：<label style={{cursor: 'pointer'}} title={phoneLists}>{phoneLists}</label>
                              </div>
                            </div>
                        </div>
                        <div className="divThumbsList">
                            <div className="divTitle"><Icon type="message" style={{marginRight: '7px'}}/>评论：点赞（{commentThumbsUpCount} 个）</div>
                            <div className="divThumbsContent">
                              {commentThumbsList}
                            </div>
                        </div>
                    </div>
                    <div className="divContainer" style={{marginLeft: '10px'}}>
                        <div className="divComment" style={{width: '530px'}}>
                            <img src="/src/styles/images/quotes.png" className="imgQuotes" />
                            <div className="divCommentContent">{commentDetail.comment.replyContent}</div>
                            <div className="divOtherInfo">
                              {new Date(commentDetail.comment.replyDate).Format("yyyy-MM-dd")}
                            </div>
                            <img src="/src/styles/images/downArrow.png" className="imgDownArrow" />
                        </div>
                        <div className="divCommentParent">
                            <img src={((commentDetail.replyUser.photoPic == undefined || commentDetail.replyUser.photoPic == "")?"/src/styles/images/defaultHeader.jpg":commentDetail.replyUser.photoPic)} />
                            <div className="divContent">
                              <div>
                                <span style={{color: '#E84C3C', fontWeight: 'bold'}}>回复人</span>：{((commentDetail.replyUser.firstName == undefined)?"":commentDetail.replyUser.firstName) + " " + ((commentDetail.replyUser.lastName == undefined)?"":commentDetail.replyUser.lastName)}， Email：{commentDetail.replyUser.email}， 性别：{(commentDetail.replyUser.gender == "1")?"男":"女"}
                              </div>
                              <div>
                                工作：{((commentDetail.replyUser.job == undefined)?"":commentDetail.replyUser.job)}， 手机号：{commentDetail.replyUser.phoneNum}
                              </div>
                              <div>
                                位置：{((commentDetail.replyUser.location == undefined)?"":commentDetail.replyUser.location) + " " + ((commentDetail.replyUser.position == undefined)?"":commentDetail.replyUser.position)}
                              </div>
                            </div>
                        </div>
                        <div className="divThumbsList">
                            <div className="divTitle"><Icon type="export" style={{marginRight: '7px'}}/>回复：点赞（{replyThumbsUpCount} 个）</div>
                            <div className="divThumbsContent">
                              {replyThumbsList}
                            </div>
                        </div>
                    </div>
                </div>
              )
          }
        }
    }
    render() {
      var serverList = null;
      if(this.state.serviceData.length != 0){
          serverList = <div>
            {
                this.renderItem()
            }
          </div>
      }else{
          serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
      }
      return (
          <div>
              <div style={{marginBottom: '10px'}}>
                排序：<Select defaultValue="1" style={{ width: 120 }} onChange={this.onTypeChange.bind(this)}>
                        <Option value="1">评论时间</Option>
                        <Option value="2">回复时间</Option>
                      </Select>
              </div>
              <Spin tip="请稍候，获取详情并跳转..." spinning={this.state.spinningOpenDetails} style={{height: '600px', maxHeight: '600px'}}>
                {serverList}
                <TableView ref="tableView"
                     getServiceData={this.getServiceData.bind(this)}
                     isService={true} searchObject={this.state.searchObject} pageNum={10}
                     url={this.state.findComments} method="post" sort={this.state.sortStr} dir="DESC"/>
              </Spin>
              <Modal
                title={"评论详情" + this.state.commentStatus}
                visible={this.state.showCommentDetails}
                wrapClassName="vertical-center-modal"
                footer={null}
                onCancel={this.showCommentDetails.bind(this, false, null)}
                width="auto"
                maskClosable={false}
              >
                  <div className="divVideo" style={{backgroundColor: '#F7F7F7'}}>
                      {this.renderCommentDetail()}
                      <div style={{textAlign: 'right'}}>
                        <Button icon="close" className="btnClose" onClick={this.showCommentDetails.bind(this, false, null)}>取 消</Button>
                      </div>
                  </div>
              </Modal>
          </div>
      )
    }
}
export default commentManage;
