import React from 'react';
import {Spin, Icon, Button, Radio} from 'antd';
const RadioGroup = Radio.Group;
import * as actions from './../../../actions/videoAction';

export default class articlePreview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceData: {},
            loading: true,
            fileFormat: 1,
            showFileFormat: false
        };
    }
    componentDidMount(){
        if(this.props.routeParams.id != null){
            actions.findArticleDetialForPreview(this, this.props.routeParams.id);
        }
    }
    componentDidUpdate(){
        //如果有链接，打开新页面
        $("a").attr("target", "_blank");
    }
    exportArticles(){
        //1.百度的方式导出（耗时：2 分钟以上）
        if(this.state.fileFormat == 1){
            XDoc.run("<html>" + document.getElementById("divArticlePreview").innerHTML + "</html>", "pdf", {}, "_blank");
        }else if(this.state.fileFormat == 2){
            XDoc.run("<html>" + document.getElementById("divArticlePreview").innerHTML + "</html>", "docx", {}, "_blank");
        }
        this.setState({showFileFormat: false});
    }
    showFileFormat(){
        this.setState({showFileFormat: true});
    }
    closeFileFormat(){
        this.setState({showFileFormat: false});
    }
    onSelectFormat(val){
        this.setState({fileFormat: val.target.value});
    }
    renderItem(){
        if(this.state.serviceData.imageTextList != undefined){
            return this.state.serviceData.imageTextList.map((item, index)=>{
                if(item.pic == undefined){
                    return <div key={index} className="divPreviewItem" dangerouslySetInnerHTML={{__html: item.content}}></div>;
                }else {
                    return (<div key={index} className="divPreviewItem" >
                      <img src={item.pic}/>
                      <div dangerouslySetInnerHTML={{__html: item.content}}></div>
                    </div>);
                }
            })
        }
    }
    render() {
        return (
            <Spin spinning={this.state.loading} tip="加载中">
              <div style={{display: ((this.state.showFileFormat)?"block":"none")}} className="divMask"></div>
              <div style={{display: ((this.state.showFileFormat)?"block":"none")}} className="divFileFormat">
                <div className="divTitle">请选择导出格式：</div>
                <RadioGroup className="radioGroup" onChange={this.onSelectFormat.bind(this)} value={this.state.fileFormat}>
                  <Radio value={1}>导出 pdf 文件格式</Radio>
                  <Radio value={2}>导出 word 文件格式</Radio>
                </RadioGroup>
                <div className="divFooter">
                    <div className="btnExport" onClick={this.exportArticles.bind(this)}>导 出</div>
                    <div className="btnClose" onClick={this.closeFileFormat.bind(this)}>取 消</div>
                </div>
              </div>
              <div id="divArticlePreview" className="divArticlePreview">
                <div className="divTitle">{(this.state.serviceData.article == undefined)?"":this.state.serviceData.article.title}</div>
                <div className="divOtherInfo">
                  日期：{(this.state.serviceData.article == undefined)?"":new Date(this.state.serviceData.article.commitTime).Format("yyyy-MM-dd")}
                  <span style={{display: ((this.state.showFileFormat)?"none":"inline")}}>
                    <Icon type="eye" style={{marginLeft: '5px', marginRight: '4px'}}/>{(this.state.serviceData.article == undefined)?"":this.state.serviceData.article.browseNumbers}
                    <Icon type="export" style={{marginLeft: '5px', marginRight: '4px'}}/>{(this.state.serviceData.article == undefined)?"":this.state.serviceData.article.shareNumbers}
                  </span>
                </div>
                {this.renderItem()}
              </div>
              <Button type="primary" style={{display: 'none'}} onClick={this.showFileFormat.bind(this)} className="btnShowExport">导出</Button>
            </Spin>
        )
    }
}
