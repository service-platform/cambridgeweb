import React from 'react';
import { Card, Button, Select, Icon, Input, notification, DatePicker } from 'antd';
import {browserHistory} from 'react-router';
import TableView from './../TableView';
import * as actions from './../../../actions/videoAction';
const { Option, OptGroup } = Select;
import moment from 'moment';
// 全局设置 locale 本地化
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
const RangePicker = DatePicker.RangePicker;

const typeData = ['全部','热门活动', '留学须知', '学生风采', '家长必读'];
const subTypeData = {
  '热门活动': [{key: '线上活动',value: "1"}, {key: '线下活动',value: "2"},{key: '剑桥快报',value: "3"}],
  '留学须知': [{key: '学业篇',value: "4"}, {key: '法律篇',value: "5"}, {key: '文化篇',value: "6"}, {key: '生活篇',value: "7"}],
  '学生风采': [{key: '每周播报',value: "8"}, {key: '美高捷报',value: "9"}],
  '家长必读': [{key: '在线培训',value: "10"}, {key: '避雷指南',value: "11"}, {key: 'CIIE洞察',value: "12"}]
};
const travelData = [{key: '走遍美国',value: "13"}, {key: '假期游学',value: "14"}, {key: '访校之旅',value: "30"}, {key: '其他服务',value: "15"}];

const academicSupportTypeData = ['大学预备语言和文化（CPLC）线上课程', '大学预备语言和文化（CPLC）线下课程', '私教辅导'];
const academicSupportSubTypeData = {
  '大学预备语言和文化（CPLC）线上课程': [{key: '课程介绍',value: "16"}, {key: '教师简介',value: "17"}, {key: '学生成功案例',value: "18"}, {key: '常见问题与解答',value: "19"}],
  '大学预备语言和文化（CPLC）线下课程': [{key: '课程介绍',value: "20"}, {key: '教师简介',value: "21"}, {key: '学生成功案例',value: "22"}, {key: '常见问题与解答',value: "23"}],
  '私教辅导': [{key: '学科辅导介绍',value: "24"}, {key: '标准化考试备考 （托福，SAT, ACT）',value: "25"}, {key: '一对一辅导介绍',value: "26"}, {key: '教师简介',value: "27"}, {key: '成功案例',value: "28"}, {key: '常见问题与解答', value: "29"}]
};

class articleList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            typeData: subTypeData[typeData[1]],
            subTypeData: subTypeData[typeData[1]][0],
            travelData: travelData[0],
            academicSupportTypeData: academicSupportSubTypeData[academicSupportTypeData[1]],
            academicSupportSubTypeData: academicSupportSubTypeData[academicSupportTypeData[1]][0],

            findArticle: GLOBAL.baseURL + "/webAPI/article/findArticles",
            totalPageNum: 0,
            ServiceData: [],
            searchObject: {},
            isAllItem: true,
            title: '',
            summary: '',
            strRangeObject: {},
            startTime: "",
            endTime: "",
            status: "",
            loading: false
        }
    }
    componentDidMount(){
      this.state.strRangeObject["今天"] = [moment(), moment()];
      this.state.strRangeObject["本周"] = [moment().startOf('week'), moment()];
      this.state.strRangeObject['本月（' + (new Date().getMonth()+1) + ' 月）'] = [moment().startOf('month'), moment()]
      this.state.strRangeObject['本季度（' + moment().startOf('quarter').format('MMM') + "起）"] = [moment().startOf('quarter'), moment()]
      this.state.strRangeObject['本年（' + new Date().getFullYear() + ' 年）'] = [moment().startOf('year'), moment()]

      if (this.props.routeParams.isTravel == "0"){
          this.props.route.breadcrumbName = "文章列表";
          setTimeout(function(){
            $($(".ant-breadcrumb-link")[1]).children().text("文章列表");
          }, 300);
      }else if (this.props.routeParams.isTravel == "1"){
          this.props.route.breadcrumbName = "旅游项目"; this.state.searchObject.type = 13;
          setTimeout(function(){
            $($(".ant-breadcrumb-link")[1]).children().text("旅游项目");
          }, 300);
          this.refs["tableView"].refreshData();
      }else if (this.props.routeParams.isTravel == "2"){
          this.props.route.breadcrumbName = "学术支持"; this.state.searchObject.type = 16;
          setTimeout(function(){
            $($(".ant-breadcrumb-link")[1]).children().text("学术支持");
          }, 300);
          this.refs["tableView"].refreshData();
      }
    }
    btnSearch(){
        this.settingConditions();
        this.setState({loading: true});
        this.refs["tableView"].refreshData();
    }
    inputText(flag, e){
        if(flag == 'title'){
          this.setState({title: e.target.value});
        }else if (flag == 'summary') {
          this.setState({summary: e.target.value});
        }
    }
    onStateChange(val){
        if(val == "-1"){
          this.setState({status: ''});
        }else {
          this.setState({status: val});
        }
    }
    onChangeTimer(dates, dateStrings){
        if(dateStrings[0] != "" && dateStrings[1] != ""){
            this.setState({
              startTime: dateStrings[0] + " 00:00:00",
              endTime: dateStrings[1] + " 23:59:59"
            });
        }else {
            this.setState({
              startTime: "",
              endTime: ""
            });
        }
    }
    getTimers(){
        var times = new Date(); var returnStr = [];
        var subTimes = new Date(times.getTime() - 1000*60*60*24*6);
        var startTime = subTimes.getFullYear() + "-" + (((subTimes.getMonth()+1) <10)?"0"+(subTimes.getMonth()+1): (subTimes.getMonth()+1))+ "-" + ((subTimes.getDate()<10)?"0"+subTimes.getDate():subTimes.getDate()) + " 00:00:00";
        var endTime = times.getFullYear() + "-" + (((times.getMonth()+1) <10)?"0"+(times.getMonth()+1): (times.getMonth()+1))+ "-" + ((times.getDate()<10)?"0"+times.getDate():times.getDate()) + " 23:59:59";
        returnStr.push(startTime); returnStr.push(endTime);
        return returnStr;
    }
    disabledEndDate(startValue){
        return startValue.valueOf() > new Date().getTime();
    }
    serviceError(err){
        this.setState({loading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示你：',
            description: '对不起，网络错误，请稍候重试。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    }
    onTypeChange(value){
        this.settingConditions();
        if(value == "全部"){
          this.setState({isAllItem: true});
          delete this.state.searchObject.type;
          this.refs["tableView"].refreshData();
        }else {
          this.setState({
              isAllItem: false,
              typeData: subTypeData[value],
              subTypeData: subTypeData[value][0],
          });
          if(value == "热门活动"){
              this.state.searchObject.type = 1;
          }else if (value == "留学须知") {
              this.state.searchObject.type = 4;
          }else if (value == "学生风采") {
              this.state.searchObject.type = 8;
          }else if (value == "家长必读") {
              this.state.searchObject.type = 10;
          }
          this.refs["tableView"].refreshData();
        }
    }
    onSelectType(e){
        this.settingConditions();
        if(e.label == "全部"){
          delete this.state.searchObject.type;
          this.refs["tableView"].refreshData();
        }else {
          this.setState({subTypeData: e});
          this.state.searchObject.type = e.key;
          this.refs["tableView"].refreshData();
        }
    }
    onAcademicSupportChange(value){
        this.settingConditions();
        if(value == "全部"){
          this.setState({isAllItem: true});
          delete this.state.searchObject.type;
          this.refs["tableView"].refreshData();
        }else {
          this.setState({
              isAllItem: false,
              academicSupportTypeData: academicSupportSubTypeData[value],
              academicSupportSubTypeData: academicSupportSubTypeData[value][0],
          });
          if(value == "大学预备语言和文化（CPLC）线上课程"){
              this.state.searchObject.type = 16;
          }else if (value == "大学预备语言和文化（CPLC）线下课程") {
              this.state.searchObject.type = 20;
          }else if (value == "私教辅导") {
              this.state.searchObject.type = 24;
          }
          this.refs["tableView"].refreshData();
        }
    }
    onAcademicSupportSelect(e){
        this.settingConditions();
        this.setState({academicSupportSubTypeData: e});
        this.state.searchObject.type = e.key;
        this.refs["tableView"].refreshData();
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            ServiceData: data,
            loading: false
        });
    }
    addArticle(){
        browserHistory.push("/articleDesign/add/" + this.props.routeParams.isTravel);
    }
    routerArticleDetail(id, type){
        browserHistory.push("/articleDesign/" + id + "," + type + "/" + this.props.routeParams.isTravel);
    }
    onChangeArticleStatus(id, flap, e){
        e.stopPropagation();
        if(flap == "lock"){
            if(confirm("是否确定发布该文章?")){
              actions.changeArticleStatusByList(this, {
                  id: id,
                  status: 'active',
                  currentTarget: e.currentTarget
              });
            }
        }else if(flap == "unLock"){
            if(confirm("是否确认要禁用该文章？")){
              actions.changeArticleStatusByList(this, {
                  id: id,
                  status: 'inactive',
                  currentTarget: e.currentTarget
              });
            }
        }
    }
    onImgLoad(e){
        $(e.currentTarget).css("backgroundImage", "none");
    }
    renderItem(item, index) {
        var imgCoverPicture = null;
        if(item.coverPicture != undefined){
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src={item.coverPicture} onLoad={this.onImgLoad.bind(this)}/>
              <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
            </div>
        }else{
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src="/src/styles/images/default.jpg"/>
              <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
            </div>
        }
        return (
            <Card key={index} onClick={this.routerArticleDetail.bind(this, item.id, item.type)} className="divItemLine" style={{width: '485px', marginLeft: '48px'}}>
                {imgCoverPicture}
                <div className="userInfo" style={{marginTop: '0px'}}>
                    <div className="divArticleTitle1" title={((item.title == undefined)?"":item.title)}>
                        <span>{'标题：' + ((item.title == undefined)?"":item.title)}</span>
                    </div>
                    <div className="divArticleContent2" title={((item.summary == undefined)?"":item.summary)}>
                        <span>{'摘要：' + ((item.summary == undefined)?"":item.summary)}</span>
                    </div>
                    <p className="pOverview">
                        <span style={{color: '#9a9a9a'}}>
                          <Icon type="like" className="iconLike" title="点赞" />{item.likeIds.length}
                          <Icon type="eye" className="iconEye" title="浏览"/>{item.browseNumbers}
                          <Icon type="share-alt" className="iconEye" title="转载"/>{item.shareNumbers}
                          <Icon type="message" className="iconEye" title="评论"/>{item.commentIds.length}
                        </span>
                        <Icon onClick={this.onChangeArticleStatus.bind(this, item.id, "lock")} type="lock" className="iconLock" style={{display: ((item.status == 'inactive')?"inline-block":'none')}} title="当前状态：未公开"/>
                        <Icon onClick={this.onChangeArticleStatus.bind(this, item.id, "unLock")} type="unlock" className="iconUnLock" style={{display: ((item.status == 'inactive')?"none":'inline-block')}} title="当前状态：公开"/>
                    </p>
                    <div className="divContent">
                        <span>{'创建：' + ((item.commitTime == undefined)?"":new Date(item.commitTime).Format("yyyy-MM-dd"))}</span>
                        <span>{'，修改：' + ((item.updateTime == undefined)?"":new Date(item.updateTime).Format("yyyy-MM-dd"))}</span>
                    </div>
                </div>
            </Card>
        );
    }
    componentWillReceiveProps(newRoute){
      if(newRoute.routeParams.isTravel == "0"){ //1
          delete this.state.searchObject.type;this.props.route.breadcrumbName = "文章列表";
          this.setState({isAllItem: true});
          setTimeout(function () {
            $($(".selectType").find(".ant-select-selection-selected-value")[0]).html("全部");
            $(".selectType").next().find(".ant-select-selection-selected-value").html("线上活动");
            $($(".ant-breadcrumb-link")[1]).children().text("文章列表");
          }, 300);
      }else if(newRoute.routeParams.isTravel == "1"){ //13
          delete this.state.searchObject.type;this.props.route.breadcrumbName = "旅游项目";
          this.setState({isAllItem: true}); this.state.searchObject.type = 13;
          setTimeout(function () {
            $($(".selectType").find(".ant-select-selection-selected-value")[0]).html("走遍美国");
            $($(".ant-breadcrumb-link")[1]).children().text("旅游项目");
          }, 300);
      }else if(newRoute.routeParams.isTravel == "2"){ //16
          delete this.state.searchObject.type;this.props.route.breadcrumbName = "学术支持";
          this.setState({isAllItem: false}); this.state.searchObject.type = 16;
          setTimeout(function () {
            $($(".selectType").find(".ant-select-selection-selected-value")[0]).html("大学预备语言和文化（CPLC）线上课程");
            $(".selectType").next().find(".ant-select-selection-selected-value").html("课程介绍");
            $($(".ant-breadcrumb-link")[1]).children().text("学术支持");
          }, 300);
      }
      this.refs["tableView"].refreshData();
    }
    settingConditions(){
        if(this.state.title.trim() != ''){
          this.state.searchObject.title = this.state.title.trim();
        }else {
          delete this.state.searchObject.title;
        }
        if(this.state.summary.trim() != ''){
          this.state.searchObject.summary = this.state.summary.trim();
        }else {
          delete this.state.searchObject.summary;
        }
        if(this.state.status != ''){
          this.state.searchObject.status = this.state.status;
        }else {
          delete this.state.searchObject.status;
        }
        if(this.state.startTime != ''){
          this.state.searchObject.startTime = this.state.startTime;
        }else {
          delete this.state.searchObject.startTime;
        }
        if(this.state.endTime != ''){
          this.state.searchObject.endTime = this.state.endTime;
        }else {
          delete this.state.searchObject.endTime;
        }
    }
    render() {
      const typeOptions = typeData.map(type => <Option key={type}>{type}</Option>);
      const subTypeOptions = this.state.typeData.map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);
      const academicSupportTypeOptions = academicSupportTypeData.map(type => <Option key={type}>{type}</Option>);
      const academicSupportSubTypeOptions = this.state.academicSupportTypeData.map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);
      const travelOptions = travelData.map(item => <Option key={item.key} value={item.value}>{item.key}</Option>);
      var serverList = null;
      if(this.state.ServiceData.length != 0){
          serverList = <div className="divFlexList">
            {
                this.state.ServiceData.map((item, index) => {
                    return this.renderItem(item, index);
                })
            }
          </div>
      }else{
          serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
      }
      var selectOptions = null;
      if(this.props.routeParams.isTravel == "0"){
        selectOptions = <div className="divType" style={{ marginLeft: '-7px', paddingLeft: 'initial'}}>
            类型：<Select id="selectType" defaultValue={typeData[0]} style={{ width: 120 }} onChange={this.onTypeChange.bind(this)} className="selectType">
              {typeOptions}
            </Select>
            <label style={{display: ((this.state.isAllItem)?'none':'inline')}}>二级类型：<Select value={this.state.subTypeData} style={{ width: 120, marginRight: 10 }} labelInValue={true} onChange={this.onSelectType.bind(this)}>
              {subTypeOptions}
            </Select></label>
        </div>
      }else if (this.props.routeParams.isTravel == "1") {
        selectOptions = <div className="divType" style={{ marginLeft: '-7px', paddingLeft: 'initial'}}>
            类型：<Select id="selectTravel" defaultValue={this.state.travelData} style={{ width: 120 }} labelInValue={true} onChange={this.onSelectType.bind(this)} className="selectType">
              {travelOptions}
            </Select>
        </div>
      }else if (this.props.routeParams.isTravel == "2") {
        selectOptions = <div className="divType" style={{ marginLeft: '-7px', paddingLeft: 'initial'}}>
            类型：<Select id="selectType" defaultValue={academicSupportTypeData[0]} style={{ minWidth: 120 }} onChange={this.onAcademicSupportChange.bind(this)} className="selectType">
              {academicSupportTypeOptions}
            </Select>
            <label>二级类型：<Select value={this.state.academicSupportSubTypeData} style={{ minWidth: 120, marginRight: 10 }} labelInValue={true} onChange={this.onAcademicSupportSelect.bind(this)}>
              {academicSupportSubTypeOptions}
            </Select></label>
        </div>
      }

      return (
          <div>
              <div className="divAdvancedSearch" style={{textAlign: 'right', height: '82px', minWidth: (this.state.isAllItem)?'834px':'1025px'}}>
                  {selectOptions}
                  <label style={{float: 'left'}}>
                    标题：<Input value={this.state.title} placeholder="请输入标题.." style={{width: '170px', marginRight: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'title')}/>
                    摘要：<Input type={(this.state.isAllItem)?'text':'textarea'} rows={3} value={this.state.summary} placeholder="请输入摘要.." style={{width: '200px', marginRight: '10px', resize: 'none', height: (this.state.isAllItem)?'initial':'65',position: (this.state.isAllItem)?'initial':'absolute'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'summary')}/>
                    <Button style={{marginLeft: (this.state.isAllItem)?'initial':'210' }} loading={this.state.loading} icon="search" type="primary" onClick={this.btnSearch.bind(this)}>查询</Button>
                  </label><br /><br />
                  <label style={{float: 'left', marginLeft: '-7px'}}>
                    时间：<RangePicker
                      disabledDate={this.disabledEndDate .bind(this)}
                      ranges={this.state.strRangeObject}
                      format="YYYY-MM-DD"
                      onChange={this.onChangeTimer.bind(this)}
                      style={{marginRight: '10px', width: '310px'}}
                    />
                    状态：<Select id="selectType" defaultValue="-1" style={{ minWidth: 170 }} onChange={this.onStateChange.bind(this)} className="selectType">
                      <Option value="-1">全部</Option>
                      <Option value="active"><Icon type="unlock" className="unlockIcon" />公开</Option>
                      <Option value="inactive"><Icon type="lock" className="lockIcon" />未公开</Option>
                    </Select>
                  </label>
                  <Button icon="plus-circle-o" style={{marginRight: '10px', marginTop: '-36px', float: 'right'}} type="primary" onClick={this.addArticle.bind(this)}>添加文章</Button>
              </div>
              {serverList}
              <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                     isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                     isService={true} serviceError={this.serviceError.bind(this)} searchObject={this.state.searchObject}
                     pageNum={10} url={this.state.findArticle} method="post" sort="commitTime" dir="DESC"/>
          </div>
      )
    }
}

export default articleList;
