import React from 'react';
import { Modal, Spin, Card, Button, Switch, Select, Input, Tooltip, Icon, Tabs, notification } from 'antd';
import * as actions from './../../../actions/carouselAction';
import TableView from './../TableView';
const { Option, OptGroup } = Select;
const TabPane = Tabs.TabPane;

const typeData = ['热门活动', '留学须知', '学生风采', '家长必读','旅游项目','大学预备语言和文化（CPLC）线上课程', '大学预备语言和文化（CPLC）线下课程', '私教辅导'];
const subTypeData = {
  '热门活动': [{key: '线上活动',value: "1"}, {key: '线下活动',value: "2"},{key: '剑桥快报',value: "3"}],
  '留学须知': [{key: '学业篇',value: "4"}, {key: '法律篇',value: "5"}, {key: '文化篇',value: "6"}, {key: '生活篇',value: "7"}],
  '学生风采': [{key: '每周播报',value: "8"}, {key: '美高捷报',value: "9"}],
  '家长必读': [{key: '在线培训',value: "10"}, {key: '避雷指南',value: "11"}, {key: 'CIIE洞察',value: "12"}],
  '旅游项目': [{key: '走遍美国',value: "13"}, {key: '假期游学',value: "14"}, {key: '访校之旅',value: "30"}, {key: '其他服务',value: "15"}],
  '大学预备语言和文化（CPLC）线上课程': [{key: '课程介绍',value: "16"}, {key: '教师简介',value: "17"}, {key: '学生成功案例',value: "18"}, {key: '常见问题与解答',value: "19"}],
  '大学预备语言和文化（CPLC）线下课程': [{key: '课程介绍',value: "20"}, {key: '教师简介',value: "21"}, {key: '学生成功案例',value: "22"}, {key: '常见问题与解答',value: "23"}],
  '私教辅导': [{key: '学科辅导介绍',value: "24"}, {key: '标准化考试备考 （托福，SAT, ACT）',value: "25"}, {key: '一对一辅导介绍',value: "26"}, {key: '教师简介',value: "27"}, {key: '成功案例',value: "28"}, {key: '常见问题与解答', value: "29"}]
};

class carouselManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findArticle: GLOBAL.baseURL + "/webAPI/article/findArticles",
            typeData: subTypeData[typeData[0]],
            subTypeData: subTypeData[typeData[0]][0],
            updateCarouselLoading: false,
            updateLoading: false,
            addCarouselImg: false,
            articleList: false,
            totalPageNum: 0,
            serverData: [],
            requestData: {},
            articleListData: [],
            tempArticleData: null,
            articleData: null,
            isHaveArticle: false,
            visibleDeleteAlert: false,
            visibleStatusAlert: false,
            searchObject: {
                app: "app",
                type: "1"
            },
            delId: '',
            delPic: '',
            txtStatus: '',
            articleFlap: 'add',
            btnSaveDisabled: true,
            modalInfo: '您确定要改为: 禁用 吗？'
        }
    }
    componentDidMount(){
        this.onRefreshData();
        var that = this;
        window.onresize = function(){
          if($(".center-modal").length > 0){
              var scrollbarHeight = that.getSizeWithScrollbar();
              $(".divFlexListMini").css("maxHeight",(scrollbarHeight.height-213)+"px")
              $(".center-modal").find(".ant-modal-content").css("height", scrollbarHeight.height + "px");
          }
          $("#leftMenu").find(".ant-layout-sider-children").css("height", (document.body.clientHeight-48)+"px");
          $("#leftMenu").find(".ant-layout-sider-children").mCustomScrollbar({theme:"minimal"});
        }
    }
    getSizeWithScrollbar(){
        if(window.innerWidth){
            return {
              width : window.innerWidth,
              height: window.innerHeight
            }
        }else if(document.documentElement.offsetWidth == document.documentElement.clientWidth){
            return {
              width : document.documentElement.offsetWidth,
              height: document.documentElement.offsetHeight
            }
        }else{
            return {
              width : document.documentElement.clientWidth + getScrollWith(),
              height: document.documentElement.clientHeight + getScrollWith()
            }
        }
    }
    onRefreshData(){
        actions.findCarousels(this, {app: ""});
    }
    onTypeChange(value){
        this.setState({
            typeData: subTypeData[value],
            subTypeData: subTypeData[value][0],
        });
    }
    onSelectType(e){
        this.state.searchObject.type = e.key;
        this.refs["articleListView"].refreshData();
        this.setState({updateLoading: true });
    }
    isHaveArticle(val){
        if(val == true){
            if(this.state.articleData != null){
                this.state.requestData.aId = this.state.articleData.id;
            }
        }else if(val == false){
            delete this.state.requestData.aId;
        }
        this.setState({isHaveArticle: val});
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            articleListData: data,
            updateLoading: false
        });
    }
    addCarouselImg(){
        this.state.fileVerify = false;
        this.setState({addCarouselImg: true, articleFlap: 'add', requestData: {}, btnSaveDisabled: false});
    }
    btnShowCarousel(val){
        this.setState({addCarouselImg: val});
        if(val == false){
            $("#carouselPic").val('');
            document.getElementById("imgPreview").src = "/src/styles/images/defaultCarousel.jpg";
            this.setState({
              tempArticleData: null,
              articleData: null,
              isHaveArticle: false
            });
        }
    }
    btnShowArticleList(val){
        var that = this;
        this.setState({articleList: val});
        if(val == true){
          that.setState({updateLoading: true});
          setInterval(function(){
            window.onresize();
          }, 300);
          setTimeout(function(){
            that.refs["articleListView"].refreshData();
            window.onresize();
          }, 800);
        }
    }
    //尺寸：530 x 270（px）
    getImgSize(imgFile){
        var that = this;
        var reader = new FileReader();
        reader.readAsDataURL(imgFile.files[0])
        reader.onload = function (e) {
           var image = new Image();
           image.src = e.target.result;
           image.onload= () => {
             if(image.naturalWidth != 530 || image.naturalHeight != 270){
                 notification.warn({
                     placement: 'bottomRight',
                     message: '提示：',
                     description: '照片尺寸：必须为 530 * 270 像素!',
                     style: {
                       borderLeft: '4px solid rgb(255, 191, 0)'
                     }
                 });
                 that.state.fileVerify = false;
                 document.getElementById("imgPreview").src = "/src/styles/images/defaultCarousel.jpg";
             }else{
                 that.state.fileVerify = true;
                 document.getElementById("imgPreview").src = e.target.result;
             }
   	      }
       }
    }
    verifyMsgAndImage(){
        if($("#carouselPic")[0].files.length >= 1){
            var file = $("#carouselPic")[0].files[0];
            if (!(file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/gif' || file.type === 'image/bmp')) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '您只能上传 JPG、PNG、GIF 和 BMP 文件！',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              this.state.fileVerify = false;
              document.getElementById("imgPreview").src = "/src/styles/images/defaultCarousel.jpg";
              return;
            }
            const isLt2M = file.size < 819200; // 大小：800k
            if (!isLt2M) {
              notification.warn({
                  placement: 'bottomRight',
                  message: '提示：',
                  description: '照片必须小于 800KB!',
                  style: {
                    borderLeft: '4px solid rgb(255, 191, 0)'
                  }
              });
              this.state.fileVerify = false;
              document.getElementById("imgPreview").src = "/src/styles/images/defaultCarousel.jpg";
              return;
            }
            this.getImgSize($("#carouselPic")[0]);
        }
    }
    btnSaveCarouselImg(){
        if(this.state.articleFlap == 'add'){
            if($("#carouselPic")[0].files.length >= 1){
                if(this.state.fileVerify == true){
                    if(this.state.isHaveArticle == true && this.state.articleData != null){
                      this.state.requestData.aId = this.state.articleData.id;
                    }
                    actions.addOrEditCarousel(this, this.state.requestData);
                }else{
                    notification.warn({
                        placement: 'bottomRight',
                        message: '提示：',
                        description: '文件类型、大小、尺寸(530 * 270)，不正确!',
                        style: {
                          borderLeft: '4px solid rgb(255, 191, 0)'
                        }
                    });
                }
            }else{
                notification.warn({
                    placement: 'bottomRight',
                    message: '提示：',
                    description: '轮播图，必须选择!',
                    style: {
                      borderLeft: '4px solid rgb(255, 191, 0)'
                    }
                });
            }
        }else {
            if($("#carouselPic")[0].files.length >= 1){
                if(this.state.fileVerify == true){
                    if(this.state.isHaveArticle == true && this.state.articleData != null){
                      this.state.requestData.aId = this.state.articleData.id;
                    }
                    actions.addOrEditCarousel(this, this.state.requestData);
                }else{
                    notification.warn({
                        placement: 'bottomRight',
                        message: '提示：',
                        description: '文件类型、大小、尺寸(530 * 270)，不正确!',
                        style: {
                          borderLeft: '4px solid rgb(255, 191, 0)'
                        }
                    });
                }
            }else{
                if(this.state.isHaveArticle == true && this.state.articleData != null){
                  this.state.requestData.aId = this.state.articleData.id;
                }
                actions.addOrEditCarousel(this, this.state.requestData);
            }
        }
    }
    onChangeDescribe(val){
        this.setState({
          requestData: {
              ...this.state.requestData,
              describe: val.target.value
          }
        });

    }
    onImgLoad(e){
        $(e.currentTarget).css("backgroundImage", "none");
    }
    selectArticle(item){
        this.setState({tempArticleData: item});
    }
    btnConfirmArticle(){
        this.setState({articleData: this.state.tempArticleData, articleList: false});
    }
    deleteCarousel(id, pic){
        this.setState({
          visibleDeleteAlert: true,
          delId: id,
          delPic: pic
        });
    }
    onConfirmDelete(){
        actions.removeCarousel(this, {
          id: this.state.delId,
          pic: this.state.delPic
        });
    }
    editCarousel(item){
        var that = this;
        var tempData = {
          cId: item.id,
          describe: item.describe
        }
        this.setState({requestData: tempData, articleFlap:'edite', addCarouselImg: true, btnSaveDisabled: true});
        setTimeout(function(){
            document.getElementById("imgPreview").src = item.pic;
            if(item.articleId != undefined){
                actions.findArticleDetial(that, item.articleId);
            }else{
                that.setState({btnSaveDisabled: false});
            }
        }, 1000);
    }
    changeStatus(cId, status){
        if(status == "inactive"){
            this.setState({
              visibleStatusAlert: true,
              delId: cId,
              txtStatus: status,
              modalInfo: <span>您确定要改为<span style={{color:'#da1010'}}>禁用</span>吗？</span>
            });
        }else {
            this.setState({
              visibleStatusAlert: true,
              delId: cId,
              txtStatus: status,
              modalInfo: <span>您确定要改为<span style={{color:'#148dd6'}}>启用</span>吗？</span>
            });
        }
    }
    onConfirmStatus(){
        actions.changeStatus(this, {
          cId: this.state.delId,
          status: this.state.txtStatus
        });
    }
    renderItem(item, index) {
        var imgCoverPicture = null;
        if(item.coverPicture != undefined){
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src={item.coverPicture} onLoad={this.onImgLoad.bind(this)}/>
              <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
            </div>
        }else{
            imgCoverPicture = <div className="divUserHead">
              <img className="imgCoverPicture" src="/src/styles/images/default.jpg"/>
              <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
            </div>
        }
        var divSelected = null;
        if(this.state.tempArticleData != null){
            if(item.id == this.state.tempArticleData.id){
                divSelected = <div className="divSelected" style={{display: 'block'}}></div>
            }else{
                divSelected = <div className="divSelected"></div>
            }
        }
        return (
            <Card key={index} onClick={this.selectArticle.bind(this, item)} className="divItemLineMini" style={{width: '485px', marginBottom: '16px'}}>
                {imgCoverPicture}
                <div className="userInfo" style={{marginTop: '0px'}}>
                    <div className="divArticleTitle1" title={((item.title == undefined)?"":item.title)}>
                        <span>{'标题：' + ((item.title == undefined)?"":item.title)}</span>
                    </div>
                    <div className="divArticleContent2" title={((item.summary == undefined)?"":item.summary)}>
                        <span>{'摘要：' + ((item.summary == undefined)?"":item.summary)}</span>
                    </div>
                    <p className="pOverview">
                        <span style={{color: '#9a9a9a'}}>
                          <Icon type="like" className="iconLike" title="点赞" />{item.likeIds.length}
                          <Icon type="eye" className="iconEye" title="浏览"/>{item.browseNumbers}
                          <Icon type="share-alt" className="iconEye" title="转载"/>{item.shareNumbers}
                          <Icon type="message" className="iconEye" title="评论"/>{item.commentIds.length}
                        </span>
                    </p>
                    <div className="divContent">
                        <span>{'创建：' + ((item.commitTime == undefined)?"":new Date(item.commitTime).Format("yyyy-MM-dd"))}</span>
                        <span>{'，修改：' + ((item.updateTime == undefined)?"":new Date(item.updateTime).Format("yyyy-MM-dd"))}</span>
                    </div>
                    {divSelected}
                </div>
            </Card>
        );
    }
    renderImageItem(item, index){
        // unlock、lock
        var btnStatus = null;
        if(item.status == 'inactive'){
            btnStatus = <Button type="primary" shape="circle" title="当前状态：禁用" icon="lock" style={{marginRight: '5px'}} onClick={this.changeStatus.bind(this, item.id, 'active')}/>
        }else{
            btnStatus = <Button type="primary" shape="circle" title="当前状态：启用" icon="unlock" style={{marginRight: '5px'}} onClick={this.changeStatus.bind(this, item.id, 'inactive')}/>
        }
        return <div key={index} className="divCarouselItem">
            <div className="divOperate">
                {btnStatus}
                <Button type="danger" shape="circle" icon="delete" onClick={this.deleteCarousel.bind(this, item.id, item.pic)}/>
            </div>
            <img src={item.pic} onClick={this.editCarousel.bind(this, item)} onLoad={this.onImgLoad.bind(this)}/>
            <div className="divDescribe" title={item.describe} onClick={this.editCarousel.bind(this, item)}>
            {"创建时间：" + new Date(item.createTime).Format("yyyy-MM-dd hh:mm")  + "，修改时间：" + ((item.updateTime == undefined)?"":new Date(item.updateTime).Format("yyyy-MM-dd hh:mm"))}<br />
            {"备注：" + ((item.describe == undefined)?"无":item.describe)}
            </div>
        </div>
    }
    render() {
      const typeOptions = typeData.map(type => <Option key={type}>{type}</Option>);
      const subTypeOptions = this.state.typeData.map(subType => <Option key={subType.key} value={subType.value}>{subType.key}</Option>);
      var serverArticleList = null; var articleInfo = null;
      if(this.state.articleListData.length != 0){
          serverArticleList = <div className="divFlexListMini">
            {
                this.state.articleListData.map((item, index) => {
                    return this.renderItem(item, index);
                })
            }
          </div>
      }else{
          serverArticleList = <div className="divEmptyDataMini">对不起，没有相关数据。</div>
      }
      if(this.state.isHaveArticle == true){
          if(this.state.articleData == null){
              var articleInfo = <div style={{marginLeft: '4px',minHeight: '52px'}}>
                  <Tooltip placement="bottomRight" title="请选择文章">
                      <Button icon="plus-circle-o" type="primary" className="btnClose btnGreen" onClick={this.btnShowArticleList.bind(this, true)}>选择文章</Button>
                  </Tooltip>
              </div>
          }else{
              var imgCoverPicture = null; var item = this.state.articleData;
              if(item.coverPicture != undefined){
                  imgCoverPicture = <div className="divUserHead">
                    <img className="imgCoverPicture" src={item.coverPicture} onLoad={this.onImgLoad.bind(this)}/>
                    <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
                  </div>
              }else{
                  imgCoverPicture = <div className="divUserHead">
                    <img className="imgCoverPicture" src="/src/styles/images/default.jpg"/>
                    <img className="imgPromotion" style={{display: ((item.extension == true)?'block':'none')}} src="/src/styles/images/Promotion.png"/>
                  </div>
              }
              var articleInfo = <Card className="divItemLineMini" onClick={this.btnShowArticleList.bind(this, true)} style={{width: '593px', marginTop: '6px', marginBottom: '6px', marginLeft: '7px'}}>
                  {imgCoverPicture}
                  <div className="userInfo" style={{marginTop: '0px'}}>
                      <div className="divArticleTitle1" style={{width: '485px'}} title={((item.title == undefined)?"":item.title)}>
                          <span>{'标题：' + ((item.title == undefined)?"":item.title)}</span>
                      </div>
                      <div className="divArticleContent2" style={{width: '485px'}} title={((item.summary == undefined)?"":item.summary)}>
                          <span>{'摘要：' + ((item.summary == undefined)?"":item.summary)}</span>
                      </div>
                      <p className="pOverview">
                          <span style={{color: '#9a9a9a'}}>
                            <Icon type="like" className="iconLike" title="点赞" />{item.likeIds.length}
                            <Icon type="eye" className="iconEye" title="浏览"/>{item.browseNumbers}
                            <Icon type="share-alt" className="iconEye" title="转载"/>{item.shareNumbers}
                            <Icon type="message" className="iconEye" title="评论"/>{item.commentIds.length}
                          </span>
                      </p>
                      <div className="divContent">
                          <span>{'创建：' + ((item.commitTime == undefined)?"":new Date(item.commitTime).Format("yyyy-MM-dd"))}</span>
                          <span>{'，修改：' + ((item.updateTime == undefined)?"":new Date(item.updateTime).Format("yyyy-MM-dd"))}</span>
                      </div>
                  </div>
              </Card>
          }
      }else if(this.state.isHaveArticle == false){
          var articleInfo = <div className="divDisableAssociation">禁用文章关联</div>
      }
      var serverList = null;
      if(this.state.serverData.length != 0){
          serverList = <div style={{marginTop: '10px'}}>
            {
                this.state.serverData.map((item, index) => {
                    return this.renderImageItem(item, index);
                })
            }
          </div>
      }else{
          serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
      }
      var btnAddCarouselImg = null;
      if(this.state.serverData.length < 10){
          btnAddCarouselImg = <Button icon="plus-circle-o" type="primary" onClick={this.addCarouselImg.bind(this)}>添加轮播图</Button>
      }else{
          btnAddCarouselImg = <span style={{color: '#ec1e1e', fontSize: '14px'}}>轮播图，最多只能添加 10 条。</span>
      }
      return (
          <div>
              <div className="divUpdateVideo">
                  {btnAddCarouselImg}
              </div>
              {serverList}
              <Modal
                  title="添加轮播图"
                  visible={this.state.addCarouselImg}
                  wrapClassName="vertical-center-modal"
                  footer={null}
                  onCancel={this.btnShowCarousel.bind(this, false)}
                  width="auto"
                  maskClosable={false}
              >
                  <Spin spinning={this.state.updateCarouselLoading} tip="上传图片中...">
                      <Tabs defaultActiveKey="1">
                        <TabPane tab="轮播图" key="1">
                            <div style={{height: '95px', marginBottom: '5px'}}>
                                <div style={{marginTop: '36px', marginLeft: '4px', display: 'inline-block',}}>
                                    <Tooltip placement="bottomRight" title="请选择轮播图">
                                        <input id="carouselPic" type="file" className="inputFile" onChange={this.verifyMsgAndImage.bind(this)} />
                                        <Button icon="picture" type="primary" className="btnClose btnGreen">选择轮播图</Button>
                                    </Tooltip>
                                </div>
                                <img id="imgPreview" style={{verticalAlign: 'middle', width: '179px', marginLeft: '311px', position: 'absolute'}} src="/src/styles/images/defaultCarousel.jpg" title="尺寸（像素）：530 * 270" />
                            </div>
                        </TabPane>
                      </Tabs>

                      <Tabs defaultActiveKey="1" tabBarExtraContent={<Switch checkedChildren={'开'} checked={this.state.isHaveArticle} unCheckedChildren={'关'} onChange={this.isHaveArticle.bind(this)}/>}>
                        <TabPane tab="关联文章" key="1">
                            {articleInfo}
                        </TabPane>
                      </Tabs>
                      <Tabs defaultActiveKey="1">
                        <TabPane tab="备注消息" key="1">
                            <div className="divItem">
                                <Tooltip placement="bottomLeft" title="请输入备注（必填）">
                                    <Input placeholder="请输入备注" value={this.state.requestData.describe} style={{width: '600px',height: '90px', margin: '2px', resize: 'none'}} className="iptAuthor" type="textarea" rows={4} onChange={(val)=>this.onChangeDescribe(val)}/>
                                </Tooltip>
                            </div>
                        </TabPane>
                      </Tabs>

                      <div style={{textAlign: 'right', marginTop: '10px'}}>
                          <Button icon="close" style={{marginRight: '15px'}} onClick={this.btnShowCarousel.bind(this, false)}>取 消</Button>
                          <Tooltip placement="bottomRight" title="保存轮播图">
                              <Button icon="save" type="primary" className="btnClose" onClick={this.btnSaveCarouselImg.bind(this)} disabled={this.state.btnSaveDisabled}>保 存</Button>
                          </Tooltip>
                      </div>
                  </Spin>
              </Modal>
              <Modal
                  title="请选择文章"
                  visible={this.state.articleList}
                  wrapClassName="center-modal"
                  footer={null}
                  onCancel={this.btnShowArticleList.bind(this, false)}
                  width="100%"
                  maskClosable={false}
              >
                  <Spin spinning={this.state.updateLoading} wrapperClassName="spinUpdateLoading" tip="加载中...">
                      <div className="divType">
                          类型：<Select defaultValue={typeData[0]} style={{ minWidth: 120 }} onChange={this.onTypeChange.bind(this)} className="selectType">
                            {typeOptions}
                          </Select>&nbsp;
                          二级类型：<Select defaultValue={this.state.subTypeData} style={{ minWidth: 120 }} labelInValue={true} onChange={this.onSelectType.bind(this)}>
                            {subTypeOptions}
                          </Select>
                      </div>
                      {serverArticleList}
                      <TableView ref="articleListView" totalPageNum={this.totalPageNum.bind(this)}
                             isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                             isService={true} searchObject={this.state.searchObject} pageNum={10}
                             url={this.state.findArticle} method="post" sort="commitTime" dir="DESC" onPageSelected={()=>{this.setState({updateLoading: true})}}/>


                  </Spin>
                  <div style={{ position: 'fixed', right: '18px', bottom: '18px'}}>
                      <Button icon="close" style={{marginRight: '15px'}} onClick={this.btnShowArticleList.bind(this, false)}>取 消</Button>
                      <Tooltip placement="bottomRight" title="选择文章">
                          <Button icon="check-circle-o" type="primary" className="btnClose" onClick={this.btnConfirmArticle.bind(this)}>确定文章</Button>
                      </Tooltip>
                  </div>
              </Modal>
              <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleDeleteAlert} onOk={this.onConfirmDelete.bind(this)} onCancel={()=>{this.setState({visibleDeleteAlert: false})}}>
                  您确定要 删除 轮播图吗？
              </Modal>
              <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleStatusAlert} onOk={this.onConfirmStatus.bind(this)} onCancel={()=>{this.setState({visibleStatusAlert: false})}}>
                  {this.state.modalInfo}
              </Modal>
          </div>
      )
    }
}

export default carouselManage;
