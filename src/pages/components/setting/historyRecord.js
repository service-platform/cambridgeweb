import React from 'react';
import { Select, Tooltip } from 'antd';
const { Option, OptGroup } = Select;
import TableView from './../TableView';
import * as actions from './../../../actions/purviewAndRolesAction';

export default class historyRecord extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          findSensitiveWords: GLOBAL.baseURL + "/webAPI/history/findHistoryByPagination",
          searchObject: {
            type: 'role'
          },
          serviceData: []
        }
    }
    componentDidMount() {

    }
    onSelectType(val){
        this.state.searchObject.type = val;
        this.refs["tableView"].refreshData();
    }
    getServiceData(data) {
        this.setState({serviceData: data});
    }
    renderItem(item, index) {
      return (
        <tr key={item.id} className="trContent">
            <td className="tdName">
              <Tooltip placement="bottomLeft" title={item.username + ((item.email == undefined || item.email == "")?"":"（" + item.email + "）")}>
                <div style={{width: '192px'}}>{item.username + ((item.email == undefined || item.email == "")?"":"（" + item.email + "）")}</div>
              </Tooltip>
            </td>
            <td className="tdType">
              <div title={item.type}>{item.type}</div>
            </td>
            <td className="tdStatus">
              <div title={item.status}>{item.status}</div>
            </td>
            <td className="tdTimer">
              <div title={new Date(item.traceTime).Format('yyyy-MM-dd hh:mm:ss')}>{new Date(item.traceTime).Format('yyyy-MM-dd hh:mm:ss')}</div>
            </td>

            <td className="tdDescription">
              <Tooltip placement="bottomLeft" title={item.beforeUpdate}>
                <div className="divBigText">{(item.beforeUpdate == undefined)?"无":item.beforeUpdate}</div>
              </Tooltip>
            </td>
            <td className="tdDescription">
              <Tooltip placement="bottomLeft" title={item.afterUpdate}>
                <div className="divBigText">{(item.afterUpdate == undefined)?"无":item.afterUpdate}</div>
              </Tooltip>
            </td>
        </tr>
      )
    }
    render() {
        var serverList = null;
        if(this.state.serviceData.length != 0){
            serverList = <div>
              <table className="tableRoles" cellSpacing="1" cellPadding="0">
                <tbody>
                  <tr className="trHeader">
                    <td className="tdName">账户</td>
                    <td className="tdType">类型</td>
                    <td className="tdStatus">状态</td>
                    <td className="tdTimer">时间</td>

                    <td className="tdDescription">修改前</td>
                    <td className="tdDescription">修改后</td>
                  </tr>
                  {
                      this.state.serviceData.map((item, index) => {
                          return this.renderItem(item, index);
                      })
                  }
                </tbody>
              </table>
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }
        return (
          <div className="divTag">
              <div className="divModuleType">
                  类型：
                  <Select defaultValue="role" style={{ width: 150 }} onChange={this.onSelectType.bind(this)} className="selectType">
                      {
                        // <OptGroup label="人员管理">
                        //     <Option key="user">工作人员记录</Option>
                        // </OptGroup>
                      }
                      <OptGroup label="系统设置">
                          <Option key="role">角色管理</Option>
                          <Option key="permission">权限管理</Option>
                      </OptGroup>
                  </Select>
              </div>
              <div className="divTagContent">
                  {serverList}
                  <TableView ref="tableView"
                             getServiceData={this.getServiceData.bind(this)}
                             isService={true} searchObject={this.state.searchObject} pageNum={20}
                             url={this.state.findSensitiveWords} method="post"/>
              </div>
          </div>
        )
    }
}
