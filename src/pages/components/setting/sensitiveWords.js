import React from 'react';
import { Tag, Form, Input, Select, Button, Modal, Icon, Checkbox, Spin } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;
import TableView from './../TableView';
import * as actions from './../../../actions/sensitiveWordsAction';
var clors = ['pink','pink-inverse','red','red-inverse','orange','orange-inverse','yellow','yellow-inverse','cyan','cyan-inverse','green','green-inverse','blue','blue-inverse','purple','purple-inverse'];

class sensitiveWords extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          uId: '',
          delId: '',
          delSensitiveWord: '',
          updateSensitiveWord: '',
          findSensitiveWords: GLOBAL.baseURL + "/webAPI/ settings/findSensitiveWords",
          visibleDeleteAlert: false,
          visibleAddAlert: false,
          addSensitiveWords: false,
          updateLoading: false,
          selectData: {},
          serviceData: [],
          searchObject: {},
          alertTitle: '添加',
          createUser: null,
          updateUser: null,
          sensitiveWord: null
        }
    }
    componentDidMount() {
        actions.findUserProfile(this);
    }
    // 提交表单
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              values.sensitiveWord = values.sensitiveWord.toLowerCase();
              if(this.state.alertTitle == '添加'){
                  this.setState({
                    visibleAddAlert: true,
                    updateSensitiveWord: values.sensitiveWord,
                    selectData: values
                  });
              }else if(this.state.alertTitle == '修改'){
                  this.state.selectData.sensitiveWord = values.sensitiveWord;
                  delete this.state.selectData.colorIndex;
                  this.setState({
                    visibleAddAlert: true
                  });
              }
          }
        });
    }
    onConfirmAdd(){
        this.setState({visibleAddAlert: false, updateLoading: true});
        actions.addSensitiveWords(this, this.state.selectData);
    }
    deleteSensitiveWords(id, sensitiveWord){
        this.setState({visibleDeleteAlert: true, delId: id, delSensitiveWord: sensitiveWord});
    }
    cloneObj(obj){
        var str, newobj = obj.constructor === Array ? [] : {};
        if(typeof obj !== 'object'){
            return;
        } else if(window.JSON){
            str = JSON.stringify(obj), //序列化对象
            newobj = JSON.parse(str); //还原
        } else {
            for(var i in obj){
                newobj[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i];
            }
        }
        return newobj;
    }
    modifySensitiveWords(item){
        this.setState({
          alertTitle: '修改',
          updateSensitiveWord: item.sensitiveWord,
          addSensitiveWords: true,
          selectData: this.cloneObj(item),
          createUser: null,
          updateUser: null,
          sensitiveWord: null
        });
        actions.sensitiveWordDetail(this, item.id);
    }
    onConfirmDelete(){
        actions.removeSensitiveWord(this, this.state.delId);
    }
    getServiceData(data) {
        for (var i = 0; i < data.length; i++) {
          try{
            if(this.state.serviceData[i].colorIndex != undefined){
              data[i].colorIndex = this.state.serviceData[i].colorIndex
            }
          }catch(e){
              data[i].colorIndex = parseInt(Math.random()*17);
          }
        }
        this.setState({serviceData: data});
    }
    renderItem(item, index) {
      return (
        <Tag key={item.id} style={{marginBottom: '10px'}} color={clors[item.colorIndex]}>
            <div style={{margin: '5px'}}>
              <span title="点击：修改" style={{marginRight: '5px'}} onClick={this.modifySensitiveWords.bind(this, item)}>{item.sensitiveWord}</span>
              <Icon type="close" onClick={this.deleteSensitiveWords.bind(this, item.id, item.sensitiveWord)}/>
            </div>
        </Tag>
      )
    }
    onIsMySensitiveWords(val){
      if(val.target.checked == true){
        if(this.state.uId != ''){
          this.state.searchObject.createUId = this.state.uId;
        }
        this.refs["tableView"].refreshData();
      }else{
        delete this.state.searchObject.createUId;
        this.refs["tableView"].refreshData();
      }
    }
    resetFields(){
      this.setState({addSensitiveWords: false});
      this.props.form.resetFields();
    }
    bindAddSensitiveWords(){
      this.setState({
        selectData: {},
        addSensitiveWords: true,
        alertTitle: '添加',
        createUser: null,
        updateUser: null,
        sensitiveWord: null
      });
    }
    render() {
        var serverList = null; var createUser = null; var updateUser = null;
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 16 }
        }

        if(this.state.serviceData.length != 0){
            serverList = <div>
              {
                  this.state.serviceData.map((item, index) => {
                      return this.renderItem(item, index);
                  })
              }
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }

        if(this.state.createUser != null){
          createUser = <div>
            <FormItem
                label="创建者："
                {...formItemLayout}>
                  <Input style={{ width: 260 }} value={this.state.createUser.firstName + this.state.createUser.lastName} disabled={true}/>
            </FormItem>
            <FormItem
                label="创建时间："
                {...formItemLayout}>
                  <Input style={{ width: 260 }} value={this.state.sensitiveWord.createTime} disabled={true}/>
            </FormItem>
          </div>
        }
        if(this.state.updateUser != null){
          updateUser = <div>
            <FormItem
                label="修改者："
                {...formItemLayout}>
                  <Input style={{ width: 260 }} value={this.state.updateUser.firstName + this.state.updateUser.lastName} disabled={true}/>
            </FormItem>
            <FormItem
                label="修改时间："
                {...formItemLayout}>
                  <Input style={{ width: 260 }} value={this.state.sensitiveWord.updateTime} disabled={true}/>
            </FormItem>
          </div>
        }
        return (
          <div className="divTagS">
                <div style={{textAlign: 'right', marginBottom: '10px'}}>
                    <Checkbox onChange={this.onIsMySensitiveWords.bind(this)}>查询我的敏感词</Checkbox>
                    <Button type="primary" icon="plus" onClick={this.bindAddSensitiveWords.bind(this)}>添加敏感词</Button>
                </div>
                <div className="divTagContent">
                    {serverList}
                    <TableView ref="tableView"
                               getServiceData={this.getServiceData.bind(this)}
                               isService={true} searchObject={this.state.searchObject} pageNum={50}
                               url={this.state.findSensitiveWords} method="post"/>
                </div>
                <Modal
                  visible={this.state.addSensitiveWords}
                  title={this.state.alertTitle + "：敏感词"}
                  width="390px"
                  maskClosable={false}
                  footer={null}
                  wrapClassName="vertical-center-modal"
                  onCancel={this.resetFields.bind(this)}
                >
                  <Spin spinning={this.state.updateLoading} tip="提交中...">
                    <Form onSubmit={this.handleSubmit.bind(this)} style={{width: '100%'}}>
                        {createUser}
                        {updateUser}
                        <FormItem
                            label="敏感词"
                            {...formItemLayout}>
                            {getFieldDecorator('sensitiveWord', {
                              rules: [{ required: true, message: '敏感词必填。'}],
                              initialValue: this.state.selectData.sensitiveWord
                            })(
                              <Input style={{ width: 260 }} placeholder="请输入 敏感词..." />
                            )}
                        </FormItem>

                        <FormItem wrapperCol={{ span: 25, offset: 0 }} style={{ marginTop: 24,textAlign: 'center' }}>
                            <Button type="primary" className="btnPersonalClose" htmlType="submit" icon="plus">{this.state.alertTitle + "：敏感词"}</Button>
                            <Button icon="close" onClick={this.resetFields.bind(this)}>取 消</Button>
                        </FormItem>
                    </Form>
                  </Spin>
                </Modal>

                <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleDeleteAlert} onOk={this.onConfirmDelete.bind(this)} onCancel={()=>{this.setState({visibleDeleteAlert: false})}}>
                    您确定要删除：{this.state.delSensitiveWord} 吗？
                </Modal>
                <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleAddAlert} onOk={this.onConfirmAdd.bind(this)} onCancel={()=>{this.setState({visibleAddAlert: false})}}>
                    您确定要{this.state.alertTitle}：{this.state.updateSensitiveWord} 吗？
                </Modal>
            </div>
        )
    }
}

sensitiveWords = Form.create()(sensitiveWords)
export default sensitiveWords;
