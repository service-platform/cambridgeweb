import React from 'react';
import { Form, Input, Button, Modal, message, Icon } from 'antd';
const FormItem = Form.Item;
import * as actions from './../../../actions/loginAction';

class changePwd extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            fieldsValue: {},
            lblStrength: '空',
            classNameWeak: 'divPwdWeak divPwdNormal',
            classNameMiddle: 'divPwdMiddle divPwdNormal',
            classNameStrong: 'divPwdStrong divPwdNormal',
        }
    }
    // 提交表单
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              this.state.fieldsValue = this.props.form.getFieldsValue();
              if(this.state.fieldsValue.newPassword != this.state.fieldsValue.confirmPassword){
                  message.warning('对不起，新密码 不等于 确认密码！'); return;
              }
              this.setState({ visible: true });
          }
        });
    }
    onCancel() {
        this.setState({ visible: false })
    }
    onConfirmModify(){
        actions.updatePassword(this,{
            oldPassword: this.state.fieldsValue.oldPassword,
            newPassword: this.state.fieldsValue.newPassword
        });
    }
    checkStrength (val){
        var aLvTxt = ['','低','中','高'];
        var lv = 0;
        if(val.match(/[a-z]/g)){lv++;}
        if(val.match(/[0-9]/g)){lv++;}
        if(val.match(/(.[^a-z0-9])/g)){lv++;}
        if(val.length <= 0){lv=0;}
        return aLvTxt[lv];
    }
    inputNewPwdChange(e){
      var strength = this.checkStrength(e.target.value);

      if(strength == ''){
          this.setState({
            lblStrength: '空',
            classNameWeak: 'divPwdWeak divPwdNormal',
            classNameMiddle: 'divPwdMiddle divPwdNormal',
            classNameStrong: 'divPwdStrong divPwdNormal',
          });
      }else if(strength == '低'){
          this.setState({
            lblStrength: '低',
            classNameWeak: 'divPwdWeak',
            classNameMiddle: 'divPwdMiddle divPwdNormal',
            classNameStrong: 'divPwdStrong divPwdNormal',
          });
      }else if(strength == '中'){
          this.setState({
            lblStrength: '中',
            classNameWeak: 'divPwdWeak',
            classNameMiddle: 'divPwdMiddle',
            classNameStrong: 'divPwdStrong divPwdNormal',
          });
      }else if(strength == '高'){
          this.setState({
            lblStrength: '高',
            classNameWeak: 'divPwdWeak',
            classNameMiddle: 'divPwdMiddle',
            classNameStrong: 'divPwdStrong',
          });
      }
    }
    render() {
        const { getFieldDecorator } = this.props.form

        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 15 }
        }
        return (
          <div className="divArticleContent" style={{width: '450px', minWidth: '450px', textAlign: 'initial'}}>
            <div className="divArticleItem" style={{height: '311px'}}>
                <div className="divTitle" style={{marginBottom: '25px', textAlign: 'center'}}><Icon type="edit" className="iconPlate" style={{marginLeft: '-187px'}}/>修改密码</div>
                <Form layout="horizontal" onSubmit={this.handleSubmit.bind(this)}>
                    <FormItem
                        label="旧密码"
                        {...formItemLayout}>
                        {getFieldDecorator('oldPassword', {
                          rules: [{ required: true, message: '请输入 旧 密码!' }],
                        })(
                          <Input placeholder="请输入旧密码..." type="password"/>
                        )}
                    </FormItem>
                    <FormItem
                        label="新密码"
                        {...formItemLayout}>
                        {getFieldDecorator('newPassword', {
                          rules: [{ required: true, message: '请输入 新 密码!' }],
                        })(
                          <Input placeholder="请输入新密码..." type="password" onChange={this.inputNewPwdChange.bind(this)} />
                        )}
                    </FormItem>
                    <FormItem
                        label="确认密码"
                        {...formItemLayout}>
                        {getFieldDecorator('confirmPassword', {
                          rules: [{ required: true, message: '请输入 确认 密码!' }],
                        })(
                          <Input placeholder="请输入确认密码..." type="password"/>
                        )}
                    </FormItem>

                    <FormItem style={{ marginTop: 24, textAlign:'center' }}>
                        <Button type="primary" htmlType="submit" style={{width: '338px'}}>修改密码</Button>
                    </FormItem>

                    <Modal title="提示：" visible={this.state.visible} onOk={this.onConfirmModify.bind(this)} onCancel={this.onCancel.bind(this)}>
                        您确定要修改密码吗？
                    </Modal>
                </Form>
              </div>
              <div className="divPwdStrength">
                  <div className={this.state.classNameWeak}></div>
                  <div className={this.state.classNameMiddle}></div>
                  <div className={this.state.classNameStrong}></div>
                  <label>{this.state.lblStrength}</label>
              </div>
            </div>
        )
    }
}

changePwd = Form.create()(changePwd)
export default changePwd;
