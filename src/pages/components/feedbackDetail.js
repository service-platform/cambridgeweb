import React from 'react';
import {Card, Row, Col, Button, Modal, Input, Tooltip} from 'antd';
import {browserHistory} from 'react-router';
import * as actions from './../../actions/feedbackAction';

export default class feedbackDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          feedbackVisible: false,
          isDisable: true,
          replyContent: '',
          parent: {},
          feedback: {},
          students: [],
          isLoading: 0
        };
    }
    componentDidMount(){
        this.refreshData();
    }
    refreshData(){
        this.setState({isLoading: 0});
        actions.feedbackDetail(this, this.props.routeParams.id);
    }
    closeModal(){
        this.setState({feedbackVisible: false});
    }
    btnShowReply(){
        this.setState({feedbackVisible: true});
    }
    onChangeValue(e){
        this.setState({replyContent: e.target.value});
    }
    btnReply(){
        this.state.feedback.replyContent = this.state.replyContent;
        this.state.feedback.status = '1';
        actions.replyFeedback(this, this.state.feedback);
    }
    btnShowImage(value){
        $("#viewerImg").find("img").click();
        if($("#viewerImg").length >= 1){
            if($(".viewer-canvas").attr("data-flag") == undefined){
                $(".viewer-canvas").click((e)=>{
                	if(e.target.tagName == "DIV"){
                		$(".viewer-close").click();
                	}
                });
                $(".viewer-canvas").attr("data-flag", "true");
            }
        }
    }
    showStudentList(students){
        if(this.state.isLoading == 0){
            return (<div className="divLoading" style={{paddingBottom: '8px'}}><img src="/src/styles/images/timg.gif"/>数据加载中...</div>);
        }else if (this.state.isLoading == 1) {
            return (<div className="divLoading" style={{paddingBottom: '8px'}}><img src="/src/styles/images/exclamation.jpg" style={{width: '30px', marginRight: '6px'}}/>网络链接错误...</div>);
        }else if (this.state.isLoading == 2) {
            if(students.length <= 0){
                return (<div className="divLoading" style={{backgroundColor: '#fbfbfb'}}>还未绑定学生</div>);
            }else {
                return students.map((item, index)=>{
                  return(<Row key={index} type="flex" className="rowLineStudnet">
                    <Col order={2}>姓名：</Col>
                    <Col order={3}>{((item.firstName == undefined)?"":item.firstName) + " " + ((item.lastName == undefined)?"":item.lastName)}</Col>
                    <Col order={4}>，生日：</Col>
                    <Col order={5}>{((item.dateOfBirth == undefined)?"":item.dateOfBirth)}</Col>
                    <Col order={6}>，绑定日期：</Col>
                    <Col order={7}>{((item.bindTime == undefined)?"":item.bindTime)}</Col>
                  </Row>)
                });
            }
        }
    }
    render() {
        var showBtnImage = null; var viewerImg = null;
        if(this.state.feedback.picUrl != undefined){
          showBtnImage = <Button type="primary" ghost className="btnReply" icon="laptop" onClick={this.btnShowImage.bind(this, true)}>显示图片</Button>
        }
        var arrScore = new Array();
        if (this.state.feedback.score != undefined){
            var score = parseInt(this.state.feedback.score);
            for (var i = 0; i < score; i++) {
                arrScore.push(i);
            }
        }
        if(this.state.feedback.picUrl != undefined){
            viewerImg = <ul id="viewerImg" style={{display: 'none'}}>
              <li><img src={this.state.feedback.picUrl} data-original={this.state.feedback.picUrl} alt="图片" /></li>
            </ul>
        }
        return (
            <div>
                <div className="divBaseInfo">
                    <div className="divTitle">反馈人信息</div>
                    <Row type="flex" className="rowLine">
                      <Col order={1}><img src={((this.state.parent.avatarUrl == undefined)?"/src/styles/images/defaultHeader.jpg":this.state.parent.avatarUrl)} className="imgHeader" /></Col>
                      <Col order={2}>姓名：</Col>
                      <Col order={3}>{(this.state.parent.nickName == undefined)?"":this.state.parent.nickName}</Col>
                      <Col order={4}>，性别：</Col>
                      <Col order={5}>{(this.state.parent.gender==1)?"男":"女"}</Col>
                      <Col order={6}>，国家：</Col>
                      <Col order={7}>{(this.state.parent.country == undefined)?"":this.state.parent.country}</Col>
                      <Col order={8} style={{float: 'left', position: 'absolute', marginLeft: '107px'}}><br />省份：{((this.state.parent.province == undefined)?"":this.state.parent.province) + "，城市：" + ((this.state.parent.city == undefined)?"":this.state.parent.city)}</Col>
                      <Col order={9} style={{float: 'left', position: 'absolute', marginLeft: '107px', marginTop: '19px'}}><br />关系：{((this.state.parent.relationship == undefined)?"":this.state.parent.relationship) + "，授权日期：" + ((this.state.parent.joinTime == undefined)?"":this.state.parent.joinTime)}</Col>
                      <Col order={10} style={{float: 'left', position: 'absolute', marginLeft: '107px', marginTop: '38px'}}><br />
                        手机号码：{this.state.parent.phones}
                      </Col>
                    </Row>
                    <div className="divTitle">学生列表（{this.state.students.length}）</div>
                    {this.showStudentList(this.state.students)}

                    <div className="divTitle">
                      反馈内容
                      {
                          arrScore.map((item, index)=>{
                              return (<img key={index} src="/src/styles/images/star.png"/>)
                          })
                      }
                      <Button type="primary" className="btnReply" icon="message" onClick={this.btnShowReply.bind(this)} disabled={this.state.isDisable}>回复</Button>
                      {showBtnImage}
                    </div>
                      <div className="divContent">内容：{(this.state.feedback.feedbackContent == undefined)?"":this.state.feedback.feedbackContent}</div>
                      <div className="divTimer">反馈时间：{(this.state.feedback.feedbackTimeStr == undefined)?"":this.state.feedback.feedbackTimeStr}</div>
                      <div className="divReply" hidden={!this.state.isDisable}>
                          内容：{(this.state.feedback.replyContent == undefined)?"":this.state.feedback.replyContent}<br />
                          回复时间：{(this.state.feedback.replyTime == undefined)?"":this.state.feedback.replyTime}
                      </div>
                    </div>
                    <Modal
                      title="回复反馈"
                      wrapClassName="vertical-center-modal"
                      visible={this.state.feedbackVisible}
                      onOk={this.btnReply.bind(this)}
                      onCancel={() => this.closeModal()}
                    >
                        <Tooltip
                          trigger={['focus']}
                          title="回复家长的反馈意见。"
                          placement="bottomLeft"
                        >
                          <Input type="textarea" style={{resize: 'none'}} placeholder="请输入回复文字..." rows={4} value={this.state.replyContent} onChange={this.onChangeValue.bind(this)}/>
                        </Tooltip>
                    </Modal>
                    {viewerImg}
            </div>
        )
    }
}
