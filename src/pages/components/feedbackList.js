import React from 'react';
import { Card, Select } from 'antd';
import {browserHistory} from 'react-router';
import TableView from './TableView';
const Option = Select.Option;

export default class feedbackList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            findParents: GLOBAL.baseURL + "/webAPI/Feedback/findFeedbacks",
            totalPageNum: 0,
            ServiceData: [],
            searchObject: {},
            sortStr: "feedbackTime"
        }
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            ServiceData: data
        });
    }
    routerParentsDetail(id){
      browserHistory.push("/feedbackList/feedbackDetail/" + id);
    }
    renderItem(item, index) {
        var imgFeedback = null;
        if(item.picUrl != undefined){
          imgFeedback = <div className="divUserHead">
            <img className="imgFeedback" src={item.picUrl}/>
          </div>
        }else{
          imgFeedback = <div className="divUserHead">
            <img className="imgFeedback" src="/src/styles/images/NoImage.png"/>
          </div>
        }
        var score = parseInt(item.score); var arrScore = new Array();
        for (var i = 0; i < score; i++) {
            arrScore.push(i);
        }
        item.feedbackTime = new Date(item.feedbackTime).Format('yyyy-MM-dd hh:mm:ss');
        return (
            <Card key={index} onClick={this.routerParentsDetail.bind(this, item.id)} className="divItemLine">
                {imgFeedback}
                <div className="userInfo">
                    <div className="divContent">
                        <span>{'反馈内容：' + ((item.feedbackContent == undefined)?"":item.feedbackContent)}</span>
                    </div>
                    <div className="divContent">
                        <span>{'反馈时间：' + ((item.feedbackTime == undefined)?"":item.feedbackTime)}</span>
                    </div>
                    <p className="pOverview">
                        <span>{'状态：' + ((item.status == '0')?"未评论":((item.score == undefined || item.score <= 0)?"已评论":''))}</span>
                        {
                            arrScore.map((item, index)=>{
                                return (<img key={index} src="/src/styles/images/star.png"/>)
                            })
                        }
                    </p>
                </div>
            </Card>
        );
    }
    onSelectStatus(val){
        if(val == -1){
            delete this.state.searchObject.status;
            this.refs["tableView"].refreshData();
        }else{
            this.state.searchObject.status = val
            this.refs["tableView"].refreshData();
        }
    }
    onSelectSorting(val){
        var that = this;
        if(val == "1"){
          this.setState({sortStr: "feedbackTime"});
          setTimeout(function () {
            that.refs["tableView"].refreshData();
          },500);
        }else if(val == "2"){
          this.setState({sortStr: "score"});
          setTimeout(function () {
            that.refs["tableView"].refreshData();
          },500);
        }
    }
    render() {
        if(this.props.params.id == undefined){
            var serverList = null;
            if(this.state.ServiceData.length != 0){
                serverList = <div style={{marginBottom: '20px'}}>
                  {
                      this.state.ServiceData.map((item, index) => {
                          return this.renderItem(item, index);
                      })
                  }
                </div>
            }else{
                serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
            }
            return (
              <div>
                  <div className="divStatus">
                    状态：
                    <Select defaultValue="-1" style={{ width: 120 }} onChange={this.onSelectStatus.bind(this)}>
                        <Option value="-1">全部</Option>
                        <Option value="0">未评论</Option>
                        <Option value="1">已评论</Option>
                    </Select>
                    &nbsp;&nbsp;排序：
                    <Select defaultValue="1" style={{ width: 120 }} onChange={this.onSelectSorting.bind(this)}>
                        <Option value="1">发布日期</Option>
                        <Option value="2">评分</Option>
                    </Select>
                  </div>
                  {serverList}
                  <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                             isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                             isService={true} searchObject={this.state.searchObject} pageNum={10}
                             url={this.state.findParents} method="post" sort={this.state.sortStr} dir="DESC"/>
              </div>
            )
        }else{
            return(this.props.children)
        }
    }
}
