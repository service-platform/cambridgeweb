import React from 'react';
import { Link, browserHistory } from 'react-router';
// 引入Antd的导航组件
import { Menu, Icon, Layout, Breadcrumb, Badge } from 'antd';
const SubMenu = Menu.SubMenu;
const { Header, Content, Footer, Sider } = Layout;

// 配置导航
export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            theme: 'dark',
            username: '',
            collapsed: false,
            mode: 'inline'
        };
    }
    componentWillMount() {
        window.onresize = () => {
            $("#leftMenu").find(".ant-layout-sider-children").css("height", (document.body.clientHeight-48)+"px");
            $("#leftMenu").find(".ant-layout-sider-children").mCustomScrollbar({theme:"minimal"});
        }
    }
    componentDidMount() {
        if(sessionStorage.getItem("username") == null){
            browserHistory.push("/login");
        }else{
            this.setState({
                username: sessionStorage.getItem("username")
            });
        }
    }
    onCollapse(collapsed){
        this.setState({
          collapsed,
          mode: collapsed ? 'vertical' : 'inline',
        });
        if(collapsed){
            $("#logo").attr("src", "/src/styles/images/logoImg.png");
            $("#logo").attr("width", "48");
            if($("#mCSB_1").length > 0){
              $("#mCSB_1").css("overflow", "initial");
              $("#mCSB_1_container").css("overflow", "initial");
            }
        }else{
            setTimeout(function() {
              $("#logo").attr("src", "/src/styles/images/logo.png");
              $("#logo").attr("width", "182");
            }, 300);

            if($("#mCSB_1").length > 0){
              $("#mCSB_1").css("overflow", "hidden");
              $("#mCSB_1_container").css("overflow", "hidden");
            }
        }
        setTimeout(function(){
            window.onresize();
        },400);
    }
    logout(){
        sessionStorage.removeItem("userAuth");
        sessionStorage.removeItem("username");
        sessionStorage.removeItem("loginData");
    }
    itemRender(route, params, routes, paths){
        const last = routes.indexOf(route) === routes.length - 1;
        return last ? <span>{route.breadcrumbName}</span> : <Link to={"/"+paths.join('/')}>{route.breadcrumbName}</Link>;
    }
    switchTheme(){
        if(this.state.theme == "dark"){
            this.setState({theme: "light"});
            $("#leftMenu").css("backgroundColor","#f7f7f7");
            $("#leftMenu").css("borderRight","1px solid #e9e9e9");
            $(".ant-layout-sider-trigger").css("backgroundColor","#fff");
            $(".ant-layout-sider-trigger").css("color","#595959");
            $("#mCSB_1_scrollbar_vertical").find(".mCSB_dragger_bar").css("backgroundColor", "rgba(0,0,0,0.2)");
        }else{
            this.setState({theme: "dark"});
            $("#leftMenu").css("borderRight","none");
            $("#leftMenu").css("backgroundColor","#333");
            $(".ant-layout-sider-trigger").css("backgroundColor","#3E3E3E");
            $(".ant-layout-sider-trigger").css("color","white");
            $("#mCSB_1_scrollbar_vertical").find(".mCSB_dragger_bar").css("backgroundColor", "rgba(255,255,255,0.2)");
        }
    }
    bindResize(){
        window.onresize();
    }
    render() {
        var permissiones = null; var permissioneName = new Array(); var photoPic = "/src/styles/images/defaultHeader.jpg";
        if(sessionStorage.getItem("loginData") != null){
            permissiones = JSON.parse(sessionStorage.getItem("loginData")).permissionList;
            for (var i = 0; i < permissiones.length; i++) {
                permissioneName.push(permissiones[i].pName);
            }
            var userProfile = JSON.parse(sessionStorage.getItem("loginData")).userProfile
            if(!(userProfile.photoPic == undefined || userProfile.photoPic == "")){
                photoPic = userProfile.photoPic;
            }
        }
        return (
            <Layout>
                <Sider id="leftMenu" collapsible
                    collapsed={this.state.collapsed}
                    onCollapse={this.onCollapse.bind(this)}>
                    <img src='/src/styles/images/logo.png' width="182" id="logo" onClick={this.switchTheme.bind(this)} title="菜单主题"/>
                    <Menu theme={this.state.theme}
                        mode={this.state.mode}
                        style={{borderRight: "none"}}
                    >
                        <Menu.Item key="1" title="仪表盘" disabled={permissioneName.contains('仪表盘')}>
                          <Link to="/home">
                            <Icon type="pie-chart" /><span className="nav-text">仪表盘</span>
                          </Link>
                        </Menu.Item>

                        <SubMenu key="sub2" onTitleClick={this.bindResize.bind(this)} title={<span><Icon type="bars" /><span className="nav-text">功能导航</span></span>}>
                            <Menu.Item key="2" disabled={permissioneName.contains('授权绑定')}><Link to="/authorizationList">授权绑定</Link></Menu.Item>
                            <Menu.Item key="3" disabled={permissioneName.contains('家长列表')}><Link to="/parentsList">家长列表</Link></Menu.Item>
                            <Menu.Item key="4" disabled={permissioneName.contains('意见反馈')}><Link to="/feedbackList">意见反馈<Badge count={0} className="badgeCount"/></Link></Menu.Item>
                            <Menu.Item key="5" disabled={permissioneName.contains('视频列表')}><Link to="/videoList">视频列表</Link></Menu.Item>
                            <Menu.Item key="6" disabled={permissioneName.contains('在线客服')}><Link to="/onlineCustomers">在线客服</Link></Menu.Item>
                        </SubMenu>

                        <SubMenu key="sub3" onTitleClick={this.bindResize.bind(this)} title={<span><Icon type="area-chart" /><span className="nav-text">统计分析</span></span>}>
                            <Menu.Item key="7" disabled={permissioneName.contains('登入统计')}><Link to="/loginStatistics">登入统计</Link></Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub4" onTitleClick={this.bindResize.bind(this)} title={<span><Icon type="user" /><span className="nav-text">人员管理</span></span>}>
                            <Menu.Item key="8" disabled={permissioneName.contains('工作人员列表')}><Link to="/staffMember">工作人员列表</Link></Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub5" onTitleClick={this.bindResize.bind(this)} title={<span><Icon type="laptop" /><span className="nav-text">文章管理</span></span>}>
                            <Menu.Item key="9" disabled={permissioneName.contains('文章列表')}><Link to="/articleList/0">文章列表</Link></Menu.Item>
                            <Menu.Item key="10" disabled={permissioneName.contains('旅游项目')}><Link to="/articleList/1">旅游项目</Link></Menu.Item>
                            <Menu.Item key="11" disabled={permissioneName.contains('学术支持')}><Link to="/articleList/2">学术支持</Link></Menu.Item>
                            <Menu.Item key="12" disabled={permissioneName.contains('轮播管理')}><Link to="/carouselManage">轮播管理</Link></Menu.Item>
                            <Menu.Item key="19" disabled={permissioneName.contains('留言管理')}><Link to="/commentManage">留言管理</Link></Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub6" onTitleClick={this.bindResize.bind(this)} title={<span><Icon type="setting" /><span className="nav-text">系统设置</span></span>}>
                            <Menu.Item key="13" disabled={permissioneName.contains('角色管理')}><Link to="/rolesList">角色管理</Link></Menu.Item>
                            <Menu.Item key="14" disabled={permissioneName.contains('权限管理')}><Link to="/purviewList">权限管理</Link></Menu.Item>
                            <Menu.Item key="15" disabled={permissioneName.contains('敏感词库')}><Link to="/sensitiveWords">敏感词库</Link></Menu.Item>
                            <Menu.Item key="16" disabled={permissioneName.contains('历史记录')}><Link to="/historyRecord">历史记录</Link></Menu.Item>
                            <Menu.Item key="17" disabled={permissioneName.contains('个人信息')}><Link to="/personalInfo">个人信息</Link></Menu.Item>
                            <Menu.Item key="18" disabled={permissioneName.contains('修改密码')}><Link to="/changePwd">修改密码</Link></Menu.Item>
                        </SubMenu>
                    </Menu>
                </Sider>

                <div id="rightWrap" style={{left: (this.state.mode != "vertical")?"200px":"64px"}}>
                    <Menu mode="horizontal">
                        <SubMenu title={<span><img src={photoPic} title="头像" className="imgTopHeader"/>{ this.state.username }</span>}>
                            <Menu.Item key="setting:1"><Link to="/login" onClick={this.logout.bind(this)}>退出</Link></Menu.Item>
                        </SubMenu>

                        <Breadcrumb routes={this.props.routes} params={this.props.params} separator=">" itemRender={this.itemRender.bind(this)}/>
                    </Menu>

                    <div className="right-box">
                        { this.props.children }
                    </div>
                </div>
            </Layout>
        )
    }
}
