import React from 'react';
import {Card, Icon, Modal, Button, Collapse, Input, DatePicker, Select, Spin, message, Popconfirm, Tooltip, notification} from 'antd';
const Panel = Collapse.Panel;
const Option = Select.Option;
import moment from 'moment';
// 全局设置 locale 本地化
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
import TableView from './TableView';
import * as actions from './../../actions/parentsAction';

export default class authorizationList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findParents: GLOBAL.baseURL + "/webAPI/parent/authorizationBindingList",
            totalPageNum: 0,
            ServiceData: [],
            searchObject: {
                whetherBind: false
            },
            sort: "authorizationTime",
            dir: "ASC",
            showAuthorizedInfo: false,
            authorizedInfo: null,
            spinTip: '保存中，请稍候...',
            isSpinning: false,
            msgLoading: null,
            isEdit: false,
            btnStatus: 0,
            isConfirmVisible: false,
            marginTop: '12px',
            nickName: '',
            lastName: '',
            firstName: '',
            cellphone: '',
            relationship: '',
            loading: false
        };
    }
    componentDidMount(){
        if(window.navigator.userAgent.toLowerCase().indexOf('firefox') != -1){
          this.setState({marginTop: '-24px'});
        }
    }
    btnSearch(){
        //昵称
        if(this.state.nickName != ''){
          this.state.searchObject.nickName = this.state.nickName;
        }else {
          delete this.state.searchObject.nickName;
        }
        //姓氏
        if(this.state.firstName != ''){
          this.state.searchObject.firstName = this.state.firstName;
        }else {
          delete this.state.searchObject.firstName;
        }
        //名字
        if(this.state.lastName != ''){
          this.state.searchObject.lastName = this.state.lastName;
        }else {
          delete this.state.searchObject.lastName;
        }
        //电话
        if(this.state.cellphone != ''){
          this.state.searchObject.cellphone = this.state.cellphone;
        }else {
          delete this.state.searchObject.cellphone;
        }
        //关系
        if(this.state.relationship != ''){
          this.state.searchObject.relationship = this.state.relationship;
        }else {
          delete this.state.searchObject.relationship;
        }

        this.setState({loading: true});
        this.refs["tableView"].refreshData();
    }
    inputText(flag, e){
        if(flag == 'nickName'){
          this.setState({nickName: e.target.value});
        }else if (flag == 'firstName') {
          this.setState({firstName: e.target.value});
        }else if (flag == 'lastName') {
          this.setState({lastName: e.target.value});
        }else if (flag == 'cellphone') {
          this.setState({cellphone: e.target.value});
        }
    }
    serviceError(err){
        this.setState({loading: false});
        notification.error({
            placement: 'bottomRight',
            message: '提示你：',
            description: '对不起，网络错误，请稍候重试。',
            style: {
              borderLeft: '4px solid rgb(240, 70, 52)'
            }
        });
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            loading: false,
            ServiceData: data
        });
    }
    authorizationDetail(item, val){
        if(item.whetherBind == false){
            this.setState({isEdit: false, btnStatus: 0});
        }else {
            this.setState({isEdit: false, btnStatus: 1});
        }

        if(this.state.msgLoading != null){this.state.msgLoading();}
        this.state.msgLoading = message.loading('加载详情，请稍等...', 0);
        actions.authorizationBindingDetail(this, item.id);
    }
    showAuthorizedInfo(val){
        this.setState({showAuthorizedInfo: val});
    }
    renderItem(item, index) {
        var imgHeader = null; var isWhetherBind = null;
        if (item.avatarUrl == undefined || item.avatarUrl == "") {
            imgHeader = <img className="userHead" src='/src/styles/images/defaultHeader.jpg'/>
        } else {
            imgHeader = <img className="userHead" src={item.avatarUrl} title={item.nickName}/>
        }
        if(item.whetherBind == true){
            if(item.parentsThemselvesId == undefined){
              isWhetherBind = <div className="processedFlapRed">已处理</div>
            }else {
              isWhetherBind = <div className="processedFlapRed" style={{backgroundColor: '#489FDF'}}>家长处理</div>
            }
        }else {
            isWhetherBind = <div className="untreatedFlapRed">待处理</div>
        }
        return (
              <Card key={index} onClick={this.authorizationDetail.bind(this, item)} className="divItem">
                  {isWhetherBind}
                  <div className="divUserHead">
                      {imgHeader}
                  </div>
                  <div className="userInfo" style={{marginTop: '-3px'}}>
                      <div className="divContent">
                          <span className="spanRoseGold">{'家长：' + ((item.nickName == undefined)?"":item.nickName)}</span>
                      </div>
                      <p className="divContent">
                        <span>{'关系：' + ((item.relationship == undefined)?"":item.relationship)}</span>
                        <span>{'，性别：' + ((item.gender==1)?"男":"女")}</span>
                      </p>
                      <div className="divContent">
                          <span>{'省份：' + ((item.province == undefined)?"":item.province)}</span>
                          <span>{'，城市：' + ((item.city == undefined)?"":item.city)}</span>
                      </div>
                      <div className="divContent">
                          <span className="spanRoseGold">{'学生姓名：' + ((item.firstName == undefined)?"":item.firstName) + ' ' + ((item.lastName == undefined)?"":item.lastName)}</span>
                      </div>
                  </div>
              </Card>
        );
    }
    inputInfo(flap, e){
        if(flap == 'firstName'){
            this.setState({
              authorizedInfo: {
                ...this.state.authorizedInfo,
                authorizationBinding: {
                  ...this.state.authorizedInfo.authorizationBinding,
                  firstName: e.target.value
                }
              }
            });
        }else if (flap == 'lastName') {
            this.setState({
              authorizedInfo: {
                ...this.state.authorizedInfo,
                authorizationBinding: {
                  ...this.state.authorizedInfo.authorizationBinding,
                  lastName: e.target.value
                }
              }
            });
        }
    }
    inputInfoByPhone(e,a,b){
        //只能包含数字 和 中杠
        if (/^[\d\s-+]*$/.test(e.target.value) == false) {
          return;
        }
        this.setState({
          authorizedInfo: {
            ...this.state.authorizedInfo,
            authorizationBinding: {
              ...this.state.authorizedInfo.authorizationBinding,
              cellphone: e.target.value
            }
          }
        });
    }
    onChangeDate(moment, dateString){
        this.setState({
          authorizedInfo: {
            ...this.state.authorizedInfo,
            authorizationBinding: {
              ...this.state.authorizedInfo.authorizationBinding,
              dateOfBirth: dateString
            }
          }
        });
    }
    onChangeRelationship(value){
        this.setState({
          authorizedInfo: {
            ...this.state.authorizedInfo,
            authorizationBinding: {
                ...this.state.authorizedInfo.authorizationBinding,
                relationship: value
            }
          }
        });
    }
    onChangeRelationShip(value){
        this.setState({relationship: value});
    }
    onChangeSelectType(value){
        if(value == "true"){
            this.setState({
              sort: "bindTime",
              dir: "DESC",
              searchObject: {
                  whetherBind: true
              }
            });
        }else {
            this.setState({
              sort: "authorizationTime",
              dir: "ASC",
              searchObject: {
                  whetherBind: false
              }
            });
        }
    }
    btnBindParent(){
        this.setState({spinTip: '绑定中，请稍候...', isSpinning: true, isConfirmVisible: false});
        actions.staffHelpBind(this, this.state.authorizedInfo.authorizationBinding.id);
    }
    btnSaveStudent(){
        this.setState({spinTip: '保存中，请稍候...', isSpinning: true});
        actions.updateAuthorizationBindingDetail(this, this.state.authorizedInfo.authorizationBinding);
    }
    btnDeleteStudent(){
        var that = this;
        Modal.confirm({
          title: '提示：',
          content: '你确定要删除此 授权信息 吗？',
          onOk: function(){
              that.setState({spinTip: '删除中，请稍候...', isSpinning: true});
              actions.removeAuthorizationBinding(that, that.state.authorizedInfo.authorizationBinding.id);
          }
        });

    }
    setIsEdit(){
        if(this.state.isEdit == true){
            this.setState({isEdit: false});
        }else {
            this.setState({isEdit: true});
        }
    }
    translateToLabel(val){
        switch (val) {
          case "father":
            return "父亲Father"
            break;
          case "mother":
            return "母亲Mother"
            break;
          case "sibling":
            return "兄弟姐妹Sibling"
            break;
          case "grandparents":
            return "祖父母Grandparents"
            break;
          default:
            return "父亲Father"
            break;
        }
    }
    bindStudent(){
        if(this.state.isEdit == true){
            Modal.warning({
              title: '提示：',
              content: '请保存信息后才可绑定。'
            });
        }else {
            this.setState({isConfirmVisible: true});
        }
    }
    render() {
        var serverList = null;
        if(this.state.ServiceData.length != 0){
            serverList = <div style={{marginBottom: '20px'}}>
              {
                  this.state.ServiceData.map((item, index) => {
                      return this.renderItem(item, index);
                  })
              }
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }
        var authorizedInfoElement = null; var baseElement = null; var authorizedElement = null; var bindAndParentElement = null; var updateUserElement = null;
        if(this.state.authorizedInfo == null){
            baseElement = <div>对不起，数据为空</div>
            authorizedElement = <div>对不起，数据为空</div>
        }else {
            var authorizationBinding = this.state.authorizedInfo.authorizationBinding;
            baseElement = <div className="divItem">
                <div className="divUserHead">
                    <img className="userHead" src={(authorizationBinding.avatarUrl == undefined || authorizationBinding.avatarUrl == "")?"/src/styles/images/defaultHeader.jpg":authorizationBinding.avatarUrl}/>
                </div>
                <div className="userInfo">
                    <div className="divContentInfo">
                        <span>{'姓名：' + authorizationBinding.nickName}</span>
                        <span>{'，性别：' + ((authorizationBinding.gender==1)?"男":"女")}</span>
                    </div>
                    <div className="divContentInfo">
                        <span>{'国家：' + authorizationBinding.country}</span>
                    </div>
                    <div className="divContentInfo">
                        <span>{'省份：' + authorizationBinding.province}</span>
                        <span>{'，城市：' + authorizationBinding.city}</span>
                    </div>
                </div>
            </div>

            authorizedElement = <div className="divItem" style={{height: '138px', paddingTop: '0', paddingBottom: '0'}}>
                <div className="userInfo">
                    <table  className="divContent">
                      <tbody>
                        <tr style={{lineHeight: '36px'}}>
                          <td style={{textAlign: 'right'}}>姓氏：</td>
                          <td style={{width: '160px', fontSize: '12px'}}>
                              {
                                (this.state.isEdit == false)?authorizationBinding.firstName: <Input placeholder="输入姓氏" style={{width: '150px', marginRight: '10px'}} value={authorizationBinding.firstName} onChange={this.inputInfo.bind(this, 'firstName')} disabled={!this.state.isEdit}/>
                              }
                          </td>
                          <td style={{textAlign: 'right'}}>名字：</td>
                          <td style={{fontSize: '12px'}}>
                              {
                                (this.state.isEdit == false)?authorizationBinding.lastName:<Input placeholder="输入名字" style={{width: '150px'}} value={authorizationBinding.lastName} onChange={this.inputInfo.bind(this, 'lastName')} disabled={!this.state.isEdit}/>
                              }
                          </td>
                        </tr>
                        <tr style={{lineHeight: '36px'}}>
                          <td style={{textAlign: 'right'}}>手机：</td>
                          <td colSpan="3" style={{fontSize: '12px'}}>
                              {
                                (this.state.isEdit == false)?authorizationBinding.cellphone:<Input prefix={<Icon type="mobile" />} style={{width: '150px'}} placeholder="输入手机号码" value={authorizationBinding.cellphone} onChange={this.inputInfoByPhone.bind(this)} disabled={!this.state.isEdit}/>
                              }
                          </td>
                        </tr>
                        <tr style={{lineHeight: '36px'}}>
                          <td style={{textAlign: 'right'}}>生日：</td>
                          <td style={{fontSize: '12px'}}>
                              {
                                (this.state.isEdit == false)?authorizationBinding.dateOfBirth:<DatePicker placeholder="请选择生日" value={((authorizationBinding.dateOfBirth == undefined || authorizationBinding.dateOfBirth == "")?undefined: moment(authorizationBinding.dateOfBirth))} onChange={this.onChangeDate.bind(this)} disabled={!this.state.isEdit}/>
                              }
                          </td>
                          <td style={{width: '50px', textAlign: 'right'}}>关系：</td>
                          <td style={{fontSize: '12px'}}>
                              {
                                (this.state.isEdit == false)?this.translateToLabel(authorizationBinding.relationship):<Select defaultValue="lucy" style={{ width: '150px' }} value={authorizationBinding.relationship} onChange={this.onChangeRelationship.bind(this)} disabled={!this.state.isEdit}>
                                  <Option value="father">父亲Father</Option>
                                  <Option value="mother">母亲Mother</Option>
                                  <Option value="sibling">兄弟姐妹Sibling</Option>
                                  <Option value="grandparents">祖父母Grandparents</Option>
                                </Select>
                              }
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </div>
            </div>
            var editTip = (this.state.btnStatus != 0)?"":"（修改后，点击保存）";
            var btnModify = <div>
                学生信息{editTip}
                <i className="setIsEdit fa fa-wrench" style={{marginTop: this.state.marginTop, display: ((this.state.btnStatus == 0)?'inline-block':'none')}} onClick={(e)=>{e.stopPropagation();this.setIsEdit(this)}} title="修改"></i>
            </div>
            authorizedInfoElement = <div className="divContainer" style={{height: 'auto'}}>
                <Collapse bordered={false} defaultActiveKey={['1', '2']}>
                  <Panel header={"家长授权信息（授权日期：" + new Date(this.state.authorizedInfo.authorizationBinding.authorizationTime).Format("yyyy-MM-dd") + "）"} key="1">
                      {baseElement}
                  </Panel>
                  <Panel header={btnModify} key="2">
                      {authorizedElement}
                  </Panel>
                </Collapse>
            </div>
            if(this.state.authorizedInfo.bindUserProfile != undefined){
                var bindUserProfile = this.state.authorizedInfo.bindUserProfile;
                var parentProfile = this.state.authorizedInfo.parent;

                var bindUserProfileElement = <div className="divItem">
                    <div className="divUserHead">
                        <img className="userHead" src={(bindUserProfile.photoPic == undefined || bindUserProfile.photoPic == "")?"/src/styles/images/defaultHeader.jpg":bindUserProfile.photoPic}/>
                    </div>
                    <div className="userInfo">
                        <div className="divContentInfo">
                            <span>{'姓名：' + ((bindUserProfile.firstName == undefined)?"":bindUserProfile.firstName) + " " + ((bindUserProfile.lastName == undefined)?"":bindUserProfile.lastName)}</span>
                            <span>{'，性别：' + ((bindUserProfile.gender==1)?"男":"女")}</span>
                            <span>{'，年龄：' + ((bindUserProfile.age == undefined)?"":bindUserProfile.age)}</span>
                            <span>{'，工作：' + ((bindUserProfile.job == undefined)?"":bindUserProfile.job)}</span>
                        </div>
                        <div className="divContentInfo">
                            <span>{'手机号码：' + ((bindUserProfile.phoneNum == undefined)?"":bindUserProfile.phoneNum)}</span>
                            <span>{'，邮箱：' + ((bindUserProfile.email == undefined)?"":bindUserProfile.email)}</span>
                        </div>
                        <div className="divContentInfo">
                            <span>{'地址：' + ((bindUserProfile.location == undefined)?"":bindUserProfile.location) + " " + ((bindUserProfile.position == undefined)?"":bindUserProfile.position)}</span>
                        </div>
                    </div>
                </div>
                var phoneNums = "";
                for (var i = 0; i < parentProfile.phones.length; i++) {
                    phoneNums += parentProfile.phones[i] + "、";
                }
                phoneNums = phoneNums.substr(0, phoneNums.length-1);

                var parentElement = <div className="divItem" style={{height: '138px', paddingTop: '6px'}}>
                    <div className="divUserHead">
                        <img className="userHead" style={{marginTop: '14px', marginBottom: '10px'}} src={(parentProfile.avatarUrl == undefined || parentProfile.avatarUrl == "")?"/src/styles/images/defaultHeader.jpg":parentProfile.avatarUrl}/>
                    </div>
                    <div className="userInfo">
                        <div className="divContentInfo">
                            <span>{'姓名：' + ((parentProfile.nickName == undefined)?"":parentProfile.nickName)}</span>
                            <span>{'，性别：' + ((parentProfile.gender==1)?"男":"女")}</span>
                            <span>{'，关系：' + ((parentProfile.relationship == undefined)?"":parentProfile.relationship)}</span>
                        </div>
                        <div className="divContentInfo">
                            <span>{'国家：' + parentProfile.country}</span>
                            <span>{'，省份：' + parentProfile.province}</span>
                            <span>{'，城市：' + parentProfile.city}</span>
                        </div>
                        <div className="divContentInfo" style={{cursor: 'pointer'}} title={'手机号码：' + phoneNums}>
                            <span>{'手机号码：' + phoneNums}</span>
                        </div>
                        <div className="divContentInfo">
                            <span>{'邮箱：' + ((parentProfile.email == undefined)?"":parentProfile.email)}</span>
                        </div>
                        <div className="divContentInfo">
                            <span>{'地址：' + ((bindUserProfile.location == undefined)?"":bindUserProfile.location) + " " + ((bindUserProfile.position == undefined)?"":bindUserProfile.position)}</span>
                        </div>
                    </div>
                </div>
                bindAndParentElement = <div className="divContainer" style={{height: 'auto'}}>
                    <Collapse bordered={false} defaultActiveKey={['1', '2']}>
                      <Panel header="绑定操作人" key="1">
                          {bindUserProfileElement}
                      </Panel>
                      <Panel header="家长详情" key="2">
                          {parentElement}
                      </Panel>
                    </Collapse>
                </div>
            }

            if(this.state.authorizedInfo.updateUserProfile != undefined){
                var updateUserProfile = this.state.authorizedInfo.updateUserProfile;
                updateUserElement = <Collapse bordered={false}>
                  <Panel header="修改人信息" key="1">
                    <div className="divItem">
                        <div className="divUserHead">
                            <img className="userHead" src={(updateUserProfile.photoPic == undefined || updateUserProfile.photoPic == "")?"/src/styles/images/defaultHeader.jpg":updateUserProfile.photoPic}/>
                        </div>
                        <div className="userInfo">
                            <div className="divContentInfo">
                                <span>{'姓名：' + ((updateUserProfile.firstName == undefined)?"":updateUserProfile.firstName) + " " + ((updateUserProfile.lastName == undefined)?"":updateUserProfile.lastName)}</span>
                                <span>{'，性别：' + ((updateUserProfile.gender==1)?"男":"女")}</span>
                                <span>{'，年龄：' + ((updateUserProfile.age == undefined)?"":updateUserProfile.age)}</span>
                                <span>{'，工作：' + ((updateUserProfile.job == undefined)?"":updateUserProfile.job)}</span>
                            </div>
                            <div className="divContentInfo">
                                <span>{'手机号码：' + ((updateUserProfile.phoneNum == undefined)?"":updateUserProfile.phoneNum)}</span>
                                <span>{'，邮箱：' + ((updateUserProfile.email == undefined)?"":updateUserProfile.email)}</span>
                            </div>
                            <div className="divContentInfo">
                                <span>{'地址：' + ((updateUserProfile.location == undefined)?"":updateUserProfile.location) + " " + ((updateUserProfile.position == undefined)?"":updateUserProfile.position)}</span>
                            </div>
                        </div>
                    </div>
                  </Panel>
                </Collapse>
            }
        }
        return (
            <div>
                <div className="divAdvancedSearch">
                    学生（姓）：<Tooltip trigger="click" placement="topLeft" title="学生姓氏"><Input value={this.state.firstName} placeholder="请输入姓氏.." style={{width: '99px', marginRight: '10px', marginBottom: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'firstName')}/></Tooltip>
                    学生（名）：<Tooltip trigger="click" placement="topLeft" title="学生名字"><Input value={this.state.lastName} placeholder="请输入名字.." style={{width: '99px', marginRight: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'lastName')}/></Tooltip>
                    状态：<Select value={this.state.searchObject.whetherBind.toString()} style={{ width: '138px',marginRight: '10px', marginBottom: '10px' }} onChange={this.onChangeSelectType.bind(this)}>
                      <Option value="false">待处理</Option>
                      <Option value="true">已处理</Option>
                    </Select>
                    <Button loading={this.state.loading} icon="search" type="primary" onClick={this.btnSearch.bind(this)}>查询</Button>
                    <br />
                    <label style={{marginLeft: '36px'}}>
                      家长：<Tooltip trigger="click" placement="topLeft" title="家长姓名"><Input value={this.state.nickName} placeholder="请输入姓名.." style={{width: '99px', marginRight: '10px', marginBottom: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'nickName')}/></Tooltip>
                    </label>
                    <label style={{marginLeft: '36px'}}>
                      电话：<Input value={this.state.cellphone} placeholder="请输入电话.." style={{width: '99px', marginRight: '10px'}} onPressEnter={this.btnSearch.bind(this)} onChange={this.inputText.bind(this, 'cellphone')}/>
                    </label>
                    关系：<Select value={this.state.relationship} style={{ width: '138px',marginRight: '10px' }} onChange={this.onChangeRelationShip.bind(this)}>
                      <Option value="">全部</Option>
                      <Option value="father">父亲Father</Option>
                      <Option value="mother">母亲Mother</Option>
                      <Option value="sibling">兄弟姐妹Sibling</Option>
                      <Option value="grandparents">祖父母Grandparents</Option>
                    </Select>
                </div>
                {serverList}
                <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                           isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                           isService={true} serviceError={this.serviceError.bind(this)} searchObject={this.state.searchObject} pageNum={10}
                           url={this.state.findParents} method="post" sort={this.state.sort} dir={this.state.dir}/>
                 <Modal
                   title={"详情信息（状态："+ ((this.state.authorizedInfo == null)?"待处理":((this.state.authorizedInfo.authorizationBinding.whetherBind == true)?((this.state.authorizedInfo.authorizationBinding.parentsThemselvesId == undefined)?"已处理":"家长处理"):"待处理")) +"）"}
                   visible={this.state.showAuthorizedInfo}
                   wrapClassName="vertical-center-modal"
                   footer={null}
                   onCancel={this.showAuthorizedInfo.bind(this, false)}
                   width="auto"
                   maskClosable={false}
                 >
                    <Spin tip={this.state.spinTip} spinning={this.state.isSpinning}>
                       <div className="divAuthorized">
                           <div className="divCommentDetail" style={{marginBottom: '0'}}>
                                {authorizedInfoElement}
                                {bindAndParentElement}
                           </div>
                           {updateUserElement}
                           <div style={{textAlign: 'right', marginTop: '10px'}}>
                             <Button type="danger" icon="delete" className="btnBindGroup" onClick={this.btnDeleteStudent.bind(this, false)} style={{marginLeft: '16px', float: 'left',display: ((this.state.btnStatus == 0)?'inline-block':'none')}}>删 除</Button>
                             <Button type="primary" icon="save" className="btnBindGroup" disabled={!this.state.isEdit} onClick={this.btnSaveStudent.bind(this, false)} style={{display: ((this.state.btnStatus == 0)?'inline-block':'none')}}>保 存</Button>
                             <Button type="primary" icon="safety" className="btnBindGroup" style={{display: ((this.state.btnStatus == 0)?'inline-block':'none')}} onClick={this.bindStudent.bind(this)}>绑定学生</Button>
                             <Button icon="close" className="btnBindGroup" onClick={this.showAuthorizedInfo.bind(this, false)} style={{marginRight: '16px'}}>取 消</Button>
                           </div>
                       </div>
                    </Spin>
               </Modal>
               <Modal
                  title="提示："
                  visible={this.state.isConfirmVisible}
                  wrapClassName="vertical-center-modal"
                  onOk={this.btnBindParent.bind(this, false)}
                  onCancel={()=>this.setState({isConfirmVisible: false})}
                >
                  你确定要 绑定学生 吗？
                </Modal>
            </div>
        )
    }
}
