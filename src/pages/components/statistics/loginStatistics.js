import React from 'react'
import {Icon, DatePicker, Button, Modal } from 'antd';
import { browserHistory } from 'react-router';
import * as dashBoardAction from './../../../actions/dashBoardAction'
import moment from 'moment';
// 全局设置 locale 本地化
import 'moment/locale/zh-cn';
moment.locale('zh-cn');
const RangePicker = DatePicker.RangePicker;

export default class loginStatistics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLoginList: false,
            actions: dashBoardAction,
            loginList: null,
            unloginList: null,
            startTime: "",
            endTime: "",
            loginTotal: 0,
            strRangeObject: {},
            myChart: null,
            txtPieAndBar: '饼状图',
            iconPieAndBar: 'pie-chart',
            seriesName: '登入',
            count: 0
        }
    }
    componentDidMount(){
        this.state.strRangeObject["今天"] = [moment(), moment()];
        this.state.strRangeObject["本周"] = [moment().startOf('week'), moment()];
        this.state.strRangeObject['本月（' + (new Date().getMonth()+1) + ' 月）'] = [moment().startOf('month'), moment()]
        this.state.strRangeObject['本季度（' + moment().startOf('quarter').format('MMM') + "起）"] = [moment().startOf('quarter'), moment()]
        this.state.strRangeObject['本年（' + new Date().getFullYear() + ' 年）'] = [moment().startOf('year'), moment()]
        this.setState({
          startTime: this.getTimers()[0],
          endTime: this.getTimers()[1]
        })
        dashBoardAction.findParentsLoginTimes(this, {
          startTime: this.getTimers()[0],
          endTime: this.getTimers()[1]
        }, false);
        var that = this;
        window.onresize = function(){
          if(that.state.myChart != undefined){
              that.state.myChart.resize();
          }
          $("#leftMenu").find(".ant-layout-sider-children").css("height", (document.body.clientHeight-48)+"px");
          $("#leftMenu").find(".ant-layout-sider-children").mCustomScrollbar({theme:"minimal"});
        }
    }
    getTimers(){
        var times = new Date(); var returnStr = [];
        var subTimes = new Date(times.getTime() - 1000*60*60*24*6);
        var startTime = subTimes.getFullYear() + "-" + (((subTimes.getMonth()+1) <10)?"0"+(subTimes.getMonth()+1): (subTimes.getMonth()+1))+ "-" + ((subTimes.getDate()<10)?"0"+subTimes.getDate():subTimes.getDate()) + " 00:00:00";
        var endTime = times.getFullYear() + "-" + (((times.getMonth()+1) <10)?"0"+(times.getMonth()+1): (times.getMonth()+1))+ "-" + ((times.getDate()<10)?"0"+times.getDate():times.getDate()) + " 23:59:59";
        returnStr.push(startTime); returnStr.push(endTime);
        return returnStr;
    }
    showLoginList(val){
        this.setState({showLoginList: val});
    }
    renderLoginList(){
      var loginList = ((this.state.seriesName == '登入')?this.state.loginList:this.state.unloginList);
      if(loginList == null){
        return(<div className="divLoading" style={{paddingBottom: '8px', width: '563px', backgroundColor: '#fff', borderStyle: 'dashed'}}><img src="/src/styles/images/timg.gif"/>数据加载中...</div>)
      }else{
        if(loginList.length <= 0){
            return(<div className="divLoading" style={{paddingBottom: '8px', width: '563px', backgroundColor: '#fff', borderStyle: 'dashed', padding: '14px'}}>无登录数据</div>)
        }else {
            var loginListComponent = loginList.map((item, index)=>{
                var avatar = null;
                if(item.avatarUrl == undefined || item.avatarUrl == ""){
                    avatar = <img src="/src/styles/images/defaultHeader.jpg" className="imgAvatar"/>
                }else {
                    avatar = <img src={item.avatarUrl} className="imgAvatar"/>
                }
                return(
                  <div key={index} className="divItemThumbsUps">
                      {avatar}
                      <div className="divNickName">{item.nickName}</div>
                      <div className="divText" style={{marginBottom: '10px', fontSize: '13px'}}>{item.email}</div>
                      <div className="divText">关系：{item.relationship}，性别：{(item.gender == 1)?"男":"女"}</div>
                      <div className="divText">省份：{item.province}，城市：{item.city}</div>
                  </div>
                )
            });
            return(
                <div className="divArticleThumbsUps">
                    {loginListComponent}
                </div>
            )
        }
      }
    }
    onChange(dates, dateStrings){
        if(dateStrings[0] != "" && dateStrings[1] != ""){
            this.setState({
              startTime: dateStrings[0] + " 00:00:00",
              endTime: dateStrings[1] + " 23:59:59"
            });
        }
    }
    searchLoginList(){
        dashBoardAction.findParentsLoginTimes(this, {
          startTime: this.state.startTime,
          endTime: this.state.endTime
        }, true);
    }
    disabledEndDate(startValue){
        return startValue.valueOf() > new Date().getTime();
    }
    showPieAndBar(){
        if(this.state.txtPieAndBar == '饼状图'){
            this.setState({txtPieAndBar: '柱状图', iconPieAndBar: 'bar-chart'});
            this.state.myChart.clear();
            this.state.myChart.setOption(this.state.pieOption);
        }else {
            this.setState({txtPieAndBar: '饼状图', iconPieAndBar: 'pie-chart'});
            this.state.myChart.clear();
            this.state.myChart.setOption(this.state.barOption);
        }
    }
    render() {
        return (
            <div className="ani-box">
                <div className="divUpdateVideo" style={{textAlign: 'left', marginBottom: '20px'}}>
                    请选择：<RangePicker
                      disabledDate={this.disabledEndDate .bind(this)}
                      defaultValue={[moment(this.getTimers()[0].split(' ')[0], 'YYYY-MM-DD'),moment()]}
                      ranges={this.state.strRangeObject}
                      format="YYYY-MM-DD"
                      onChange={this.onChange.bind(this)}
                      style={{marginRight: '10px'}}
                    />
                    <Button icon="search" type="primary" className="btnSearch" onClick={this.searchLoginList.bind(this)}>查询</Button>
                    <Button icon={this.state.iconPieAndBar} className="btnInfo" onClick={this.showPieAndBar.bind(this)}>{this.state.txtPieAndBar}</Button>
                    <span className="spanLoginTotal">总共：{this.state.loginTotal} 人</span>
                </div>
                <div id="divBars"  className="divBars" style={{width: '100%', height: '500px', overflow: 'hidden'}}></div>
                <Modal
                  title={this.state.seriesName + " 列表（" + ((this.state.seriesName == '登入')?((this.state.loginList == null)?"0":this.state.loginList.length):((this.state.unloginList == null)?"0":this.state.unloginList.length)) + " 人）"}
                  visible={this.state.showLoginList}
                  wrapClassName="vertical-center-modal"
                  footer={null}
                  onCancel={this.showLoginList.bind(this, false)}
                  width="auto"
                >
                    <div className="divVideo">
                        {
                          this.renderLoginList()
                        }
                        <div style={{textAlign: 'right'}}>
                          <Button icon="close" className="btnClose" onClick={this.showLoginList.bind(this, false)}>取 消</Button>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}
