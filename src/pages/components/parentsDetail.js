import React from 'react';
import {Card, Row, Col, Modal, Input, Button, Spin, Collapse, Tabs} from 'antd';
const Panel = Collapse.Panel;
const TabPane = Tabs.TabPane;
import * as actions from './../../actions/parentsAction';

export default class parentsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          parent: {},
          students: [],
          showStudentDetail: false,
          findStuDetial: null,
          loading: false
        };
    }
    componentDidMount(){
        actions.findParentById(this, this.props.routeParams.id);
    }
    findStudentDetail(item){
        this.setState({showStudentDetail: true, findStuDetial: null, loading: true});
        actions.findStuDetial(this, item.id);
    }
    showStudentDetail(val){
        this.setState({showStudentDetail: val});
    }
    render() {
        return (
            <div>
                <div className="divBaseInfo">
                    <div className="divTitle">家长基础信息</div>
                    <Row type="flex" className="rowLine" style={{height: '141px'}}>
                      <Col order={1}><img src={((this.state.parent.avatarUrl == undefined || this.state.parent.avatarUrl=="")?"/src/styles/images/defaultHeader.jpg":this.state.parent.avatarUrl)} className="imgHeader" /></Col>
                      <Col order={2}>姓名：</Col>
                      <Col order={3}>{(this.state.parent.nickName == undefined)?"":this.state.parent.nickName}</Col>
                      <Col order={4}>，性别：</Col>
                      <Col order={5}>{(this.state.parent.gender==1)?"男":"女"}</Col>
                      <Col order={6}>，国家：</Col>
                      <Col order={7}>{(this.state.parent.country == undefined)?"":this.state.parent.country}</Col>
                      <Col order={8} style={{float: 'left', position: 'absolute', marginLeft: '107px'}}><br />省份：{((this.state.parent.province == undefined)?"":this.state.parent.province) + "，城市：" + ((this.state.parent.city == undefined)?"":this.state.parent.city)}</Col>
                      <Col order={9} style={{float: 'left', position: 'absolute', marginLeft: '107px', marginTop: '19px'}}><br />关系：{((this.state.parent.relationship == undefined)?"":this.state.parent.relationship) + "，电子邮件：" + ((this.state.parent.email == undefined)?"":this.state.parent.email)}</Col>
                      <Col order={10} style={{float: 'left', position: 'absolute', marginLeft: '107px', marginTop: '38px'}}><br />
                        授权日期：{((this.state.parent.joinTime == undefined)?"":this.state.parent.joinTime)}
                      </Col>
                      <Col order={11} style={{float: 'left', position: 'absolute', marginLeft: '107px', marginTop: '59px'}}><br />
                        手机号码：{this.state.parent.phones}
                      </Col>
                      <Col order={12} style={{float: 'left', position: 'absolute', marginLeft: '107px', marginTop: '80px'}}><br />
                        上次登录时间：{((this.state.parent.lastLoginTime == undefined)?"":new Date(this.state.parent.lastLoginTime).Format("yyyy-MM-dd hh:mm:ss"))}
                      </Col>
                    </Row>
                    <div className="divTitle">学生列表（{this.state.students.length}）</div>
                    {
                      this.state.students.map((item, index)=>{
                        return(<Row key={index} type="flex" className="rowLineStudnet" onClick={this.findStudentDetail.bind(this, item)}>
                          <Col order={2}>姓名：</Col>
                          <Col order={3}>{((item.firstName == undefined)?"":item.firstName) + " " + ((item.lastName == undefined)?"":item.lastName)}</Col>
                          <Col order={4}>，生日：</Col>
                          <Col order={5}>{((item.dateOfBirth == undefined)?"":item.dateOfBirth)}</Col>
                          <Col order={6}>，绑定日期：</Col>
                          <Col order={7}>{((item.bindTime == undefined)?"":item.bindTime)}</Col>
                        </Row>)
                      })
                    }
                </div>
                <Modal
                  title={"学生详情" + ((this.state.findStuDetial == null)?"（加载中...）":"")}
                  visible={this.state.showStudentDetail}
                  wrapClassName="vertical-center-modal"
                  footer={null}
                  onCancel={this.showStudentDetail.bind(this, false)}
                  width="auto"
                  maskClosable={false}
                >
                    <Spin spinning={this.state.loading} tip="加载中，请稍候...">
                        <div style={{minWidth: '538px'}}>
                            <Tabs defaultActiveKey="1">
                              <TabPane tab="SSC 信息" key="1">
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={4} style={{textAlign: 'right'}}>SSC 姓名：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.ssc == undefined)?"":this.state.findStuDetial.ssc.fullName}</Col>
                                  <Col span={4} style={{textAlign: 'right'}}>SSC 电话：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.ssc == undefined)?"":this.state.findStuDetial.ssc.phoneNum}</Col>
                                </Row>
                              </TabPane>
                              <TabPane tab="FEA 信息" key="2">
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={4} style={{textAlign: 'right'}}>FEA 姓名：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.fea == undefined)?"":this.state.findStuDetial.fea.fullName}</Col>
                                  <Col span={4} style={{textAlign: 'right'}}>FEA 电话：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.fea == undefined)?"":this.state.findStuDetial.fea.phoneNum}</Col>
                                </Row>
                              </TabPane>
                            </Tabs>
                            <Collapse bordered={false} defaultActiveKey={['1', '2', '3', '4']}>
                              <Panel header="学校信息" key="1">
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={4} style={{textAlign: 'right'}}>学校名：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.enrolledSchool == undefined)?"":this.state.findStuDetial.enrolledSchool.basics.fullName}</Col>
                                  <Col span={4} style={{textAlign: 'right'}}>学校缩写：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.enrolledSchool == undefined)?"":this.state.findStuDetial.enrolledSchool.masterInfo.schoolName}</Col>
                                </Row>
                              </Panel>
                              <Panel header="学生信息" key="2">
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={4} style={{textAlign: 'right'}}>ID：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.student == undefined)?"":this.state.findStuDetial.student.id}</Col>
                                  <Col span={4} style={{textAlign: 'right'}}>姓名：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.student == undefined)?"":this.state.findStuDetial.student.passportInfo.familyName +" "+ this.state.findStuDetial.student.passportInfo.givenNames}</Col>
                                </Row>
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={4} style={{textAlign: 'right'}}>生日：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.student == undefined)?"":this.state.findStuDetial.student.passportInfo.dateOfBirth}</Col>
                                  <Col span={4} style={{textAlign: 'right'}}>年级：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.currentEnrolledGrade == undefined)?"":this.state.findStuDetial.currentEnrolledGrade}</Col>
                                </Row>
                              </Panel>
                              <Panel header="Agency 信息" key="3">
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={7} style={{textAlign: 'right'}}>Agency Id：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.agencyInfo == undefined)?"":this.state.findStuDetial.agencyInfo.angecyId}</Col>
                                </Row>
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={7} style={{textAlign: 'right'}}>Agency Name：</Col>
                                  <Col span={8} title={(this.state.findStuDetial == null || this.state.findStuDetial.agencyInfo == undefined)?"":this.state.findStuDetial.agencyInfo.agencyName} style={{cursor: 'pointer',overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap', width: '365px'}}>{(this.state.findStuDetial == null || this.state.findStuDetial.agencyInfo == undefined)?"":this.state.findStuDetial.agencyInfo.agencyName}</Col>
                                </Row>
                                <Row type="flex" gutter={10} align="middle" style={{marginBottom: '10px'}}>
                                  <Col span={7} style={{textAlign: 'right'}}>Agency Branch Name：</Col>
                                  <Col span={8}>{(this.state.findStuDetial == null || this.state.findStuDetial.agencyInfo == undefined)?"":this.state.findStuDetial.agencyInfo.city}</Col>
                                </Row>
                              </Panel>
                            </Collapse>
                            <div style={{textAlign: 'right', marginTop: '10px'}}>
                              <Button icon="close" className="btnClose" onClick={this.showStudentDetail.bind(this, false)}>取 消</Button>
                            </div>
                        </div>
                    </Spin>
                </Modal>
            </div>
        )
    }
}
