import React from 'react'
import {Icon, Progress, Button } from 'antd';
import { browserHistory } from 'react-router';
import * as dashBoardAction from './../../actions/dashBoardAction'
// 仪表盘
export default class dashBoard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serverData: {},
            replyAvgScore: '0',
            percent: 0
        }
    }
    componentDidMount(){
        if(sessionStorage.getItem("username") != null){
            dashBoardAction.findUserProfile(this);
            dashBoardAction.replyAvgScore(this);
        }
        $("li[title=仪表盘]").click();
        setInterval(function () {
          $(".ant-progress-status-success").find(".ant-progress-bg").css("backgroundColor", "white");
          $(".ant-progress-status-success").find(".ant-progress-text").css("color", "white");
          $(".ant-progress-status-normal").find(".ant-progress-bg").css("backgroundColor", "#eac354");
        }, 500);
    }
    gotoModifyInfo(){
        browserHistory.push("/personalInfo");
    }
    render() {
        var imgHeaderPicture = null;
        if(this.state.serverData.photoPic != undefined && this.state.serverData.photoPic != ""){
            imgHeaderPicture = <img src={this.state.serverData.photoPic}/>
        }else{
            imgHeaderPicture = <img src="/src/styles/images/default.jpg"/>
        }
        return (
            <div className="ani-box">
                <div className="divDashBoard">
                    <Button className="btnModifyInfo" onClick={this.gotoModifyInfo.bind(this)}>修 改</Button>
                    <div className="divTitle">
                        {imgHeaderPicture}
                        <div className="divName">{((this.state.serverData.firstName == undefined)?"":this.state.serverData.firstName)+" "+((this.state.serverData.lastName == undefined)?"":this.state.serverData.lastName)}</div>
                        <div><Icon type="environment-o" className="iconLocation" />{((this.state.serverData.location == undefined)?"":this.state.serverData.location) +" "+ ((this.state.serverData.position == undefined)?"":this.state.serverData.position)}</div>
                        <div><Icon type="phone" className="iconLocation" />{((this.state.serverData.phoneNum == undefined)?"":this.state.serverData.phoneNum)}</div>
                        <div><Icon type="idcard" className="iconLocation" />年龄：{((this.state.serverData.age == undefined)?"":this.state.serverData.age)}，性别：{(this.state.serverData.gender == 1)?"男":"女"}</div>
                    </div>
                    <div className="divOverview">描述：{((this.state.serverData.overview == undefined)?"":this.state.serverData.overview)}</div>
                    <div className="divOtherInfo">
                        <div title={this.state.serverData.job}><Icon type="tag" className="iconLocation" title="职位"/>{((this.state.serverData.job == undefined)?"":this.state.serverData.job)}</div>
                        <div title={this.state.serverData.email} style={{flex: 2, textAlign: 'center'}}><Icon type="global" className="iconLocation" title="电子邮箱"/>{((this.state.serverData.email == undefined)?"":this.state.serverData.email)}</div>
                        <div title={this.state.serverData.replyAvgScore} style={{textAlign: 'right'}}><Icon type="star" className="iconLocation" title="服务平均分"/>{this.state.replyAvgScore}</div>
                    </div>
                    <div className="divOtherInfo1">
                        <div>资料完善程度：</div>
                        <Progress percent={this.state.percent} format={percent => `${percent}%`}/>
                    </div>
                </div>
            </div>
        )
    }
}
