import React from 'react';
import {Card, Icon, Button, Modal, Spin, Form, Input, InputNumber, Collapse, Radio, AutoComplete, message, Row, Col, Select, Tooltip} from 'antd';
const FormItem = Form.Item;
const Panel = Collapse.Panel;
const RadioGroup = Radio.Group;
const Option = AutoComplete.Option;
import TableView from './../TableView';
import * as actions from './../../../actions/purviewAndRolesAction';

class staffMember extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            findParents: GLOBAL.baseURL + "/webAPI/user/findUserList",
            totalPageNum: 0,
            ServiceData: [],
            searchObject: {},
            addAccount: false,
            baseData: {
              gender: "1"
            },
            alertTitle: '添加',
            updateLoading: false,
            updateRoleLoading: false,
            result: [],
            rolesOptions: [],
            helpStr: <div style={{marginTop: '3px', marginLeft: '8px'}}>请选择一个角色！</div>,
            visibleAddAlert: false,
            editAccountRole: false,
            isFirstRequest: true
        };
    }
    componentDidMount() {
        actions.findRolesAll(this);
    }
    totalPageNum(num) {
        this.setState({
            totalPageNum: num
        })
    }
    getServiceData(data) {
        this.setState({
            ServiceData: data
        });
    }
    openParentsDetail(item){
        if(this.state.isFirstRequest == false){
            if(item.roles.length >= 1){
                this.setState({helpStr: <div style={{marginTop: '3px', marginLeft: '8px'}}><Icon type="loading" style={{marginRight: '10px'}}/>加载中，请稍候...</div>});
                actions.findUserDetail(this, item.id);
            }
        }
        this.state.baseData = item;
        this.setState({editAccountRole: true});
    }
    bindAddPeople(){
        this.setState({addAccount: true});
    }
    onConfirmAdd(){
        this.setState({visibleAddAlert: false, updateLoading: true});
        actions.createUser(this, this.state.baseData);
    }
    resetFields(){
        this.setState({addAccount: false, baseData: {gender: "1"}, helpStr: <div style={{marginTop: '3px', marginLeft: '8px'}}>请选择一个角色！</div>});
        this.props.form.resetFields();
    }
    resetFieldsByRole(){
        this.setState({editAccountRole: false, baseData: {gender: "1"},helpStr: <div style={{marginTop: '3px', marginLeft: '8px'}}>请选择一个角色！</div>});
        this.props.form.resetFields();
    }
    editAccountRoleSubmit(e){
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (values.roles.length > 0) {
            this.state.baseData.roles = new Array(values.roles);
            this.setState({updateRoleLoading: true});
            actions.updateUserRole(this, this.state.baseData);
          }
        })
    }
    // 提交表单
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              var baseData = {};
              baseData.email = values.email;
              baseData.username = values.username.toLowerCase();

              baseData.email = values.email
              baseData.firstName = values.firstName;
              baseData.lastName = values.lastName;
              baseData.gender = values.gender;
              baseData.roles = values.roles;
              baseData.phoneNum = values.phoneNum

              this.setState({
                visibleAddAlert: true,
                baseData: baseData
              });
          }
        });
    }
    showCommonMailbox(value){
        let result;
        if (!value || value.indexOf('@') >= 0) {
          result = [];
        } else {
          result = ['hotmail.com', 'gmail.com', 'sina.com', '163.com', '126.com', 'qq.com'].map(domain => `${value}@${domain}`);
        }
        this.setState({ result });
    }
    onSelectChange(val){
        this.setState({helpStr: <div style={{marginTop: '3px', marginLeft: '8px'}}><Icon type="loading" style={{marginRight: '10px'}}/>加载中，请稍候...</div>});
        actions.findRoleDetailBySelect(this, val);
    }
    strCallback(val){
        this.setState({helpStr: <Tooltip placement="bottomLeft" title={val}><div className="divHelpStr">{val}</div></Tooltip>})
    }
    renderItem(item, index) {
        var imgHeader = null;
        if (item.photoPic == undefined || item.photoPic == "") {
            imgHeader = <img className="userHead" src='/src/styles/images/defaultHeader.jpg'/>
        } else {
            imgHeader = <img className="userHead" src={item.photoPic}/>
        }
        return (
              <Card key={index} onClick={this.openParentsDetail.bind(this, item)} className="divItem">
                  <div className="divUserHead">
                      {imgHeader}
                  </div>
                  <div className="userInfo" style={{marginTop: '-3px'}}>
                      <div className="divContent">
                          <span>{'姓名：' + ((item.firstName == undefined)?"":item.firstName) + " " + ((item.lastName == undefined)?"":item.lastName)}</span>
                          <span>{'，性别：' + ((item.gender==1)?"男":"女")}</span>
                      </div>
                      <div className="divContent">
                          <span>{'地址：' + ((item.location == undefined)?"":item.location)}</span>
                          <span>{' ' + ((item.position == undefined)?"":item.position)}</span>
                      </div>
                      <p className="pOverview">
                        <span title={((item.email == undefined)?"":item.email)}>{'电子邮件：' + ((item.email == undefined)?"":item.email)}</span>
                      </p>
                      <p className="pOverview">
                        <span>{'手机号码：' + ((item.phoneNum == undefined)?"": item.phoneNum)}</span>
                      </p>
                  </div>
              </Card>
        );
    }
    render() {
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 16 }
        }
        const baseFormItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 12 }
        }
        var serverList = null;
        if(this.state.ServiceData.length != 0){
            serverList = <div style={{marginBottom: '20px'}}>
              {
                  this.state.ServiceData.map((item, index) => {
                      return this.renderItem(item, index);
                  })
              }
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }
        const { result } = this.state;
        const children = result.map((email) => {
          return <Option key={email}>{email}</Option>;
        });
        return (
            <div>
                <div style={{textAlign: 'right', marginBottom: '10px'}}>
                    <Button type="primary" icon="plus" onClick={this.bindAddPeople.bind(this)}>添加：工作人员</Button>
                </div>
                {serverList}
                <TableView ref="tableView" totalPageNum={this.totalPageNum.bind(this)}
                           isInternalRendering={false} getServiceData={this.getServiceData.bind(this)}
                           isService={true} searchObject={this.state.searchObject} pageNum={10}
                           url={this.state.findParents} method="post" sort="joinTime" dir="DESC"/>
                 <Modal
                   visible={this.state.addAccount}
                   title={this.state.alertTitle + "：工作人员"}
                   width="600px"
                   maskClosable={false}
                   footer={null}
                   wrapClassName="vertical-center-modal"
                   onCancel={this.resetFields.bind(this)}
                 >
                     <Spin spinning={this.state.updateLoading} tip="账户创建中，请稍候...">
                       <Form onSubmit={this.handleSubmit.bind(this)} style={{width: '100%'}}>
                            <div className="divContainer">
                                <Collapse bordered={false} defaultActiveKey={['1','2']}>
                                  <Panel header="基础信息" key="1">
                                      <FormItem
                                          label="用户名"
                                          {...baseFormItemLayout} hasFeedback={true}>
                                          {getFieldDecorator('username', {
                                            rules: [
                                              { required: true, message: '用户名 必填。'},
                                              { pattern: /^\w{6,}$/, message: '至少 6 位，且只能是 数字，字母 或 下划线。'}
                                            ],
                                            initialValue: this.state.baseData.username
                                          })(
                                            <Input placeholder="请输入 用户名..." />
                                          )}
                                      </FormItem>
                                      <FormItem
                                          label="电子邮件"
                                          {...baseFormItemLayout} hasFeedback={true}>
                                          {getFieldDecorator('email', {
                                            rules: [{ type: "email", required: true, message: '电子邮件 为空 或 格式不正确。'}],
                                            initialValue: this.state.baseData.email
                                          })(

                                            <AutoComplete
                                                onSearch={this.showCommonMailbox.bind(this)}
                                                placeholder="请输入 电子邮箱..."
                                              >
                                                {children}
                                              </AutoComplete>
                                          )}
                                      </FormItem>
                                      <FormItem
                                        {...baseFormItemLayout}
                                        label="选择角色" hasFeedback={true} help={this.state.helpStr}>
                                        {getFieldDecorator('roles', {
                                          rules: [{ required: true, message: '请选择 角色!' }],
                                          initialValue: this.state.baseData.roles
                                        })(
                                          <Select placeholder="请选择 角色!" onChange={this.onSelectChange.bind(this)}>
                                            {
                                              this.state.rolesOptions.map((item, index)=>{
                                                return (<Option key={index} value={item.id}>{item.roleName}</Option>);
                                              })
                                            }
                                          </Select>
                                        )}
                                      </FormItem>
                                  </Panel>
                                  <Panel header="个人信息" key="2">
                                      <Row gutter={8} style={{marginLeft: '58px'}}>
                                          <Col span={12}>
                                            <FormItem
                                                label="姓氏"
                                                {...formItemLayout} hasFeedback={true}>
                                                  {getFieldDecorator('firstName', {
                                                    rules: [{ required: true, message: '请输入 姓氏!' }],
                                                    initialValue: this.state.baseData.firstName
                                                  })(<Input placeholder="请输入 姓氏..." type="text"/>)}
                                            </FormItem>
                                          </Col>
                                          <Col span={12}>
                                            <FormItem
                                                label="名字"
                                                {...formItemLayout} hasFeedback={true}>
                                                  {getFieldDecorator('lastName', {
                                                    rules: [{ required: true, message: '请输入 名字!' }],
                                                    initialValue: this.state.baseData.lastName
                                                  })(<Input placeholder="请输入 名字..." type="text"/>)}
                                            </FormItem>
                                          </Col>
                                      </Row>
                                      <Row gutter={8} style={{marginLeft: '58px'}}>
                                          <Col span={12}>
                                              <FormItem
                                                  label="手机"
                                                  {...formItemLayout} hasFeedback={true}>
                                                  {getFieldDecorator('phoneNum', {
                                                    rules: [{ required: true , message: '请输入 手机号码!'}],
                                                    initialValue: this.state.baseData.phoneNum
                                                  })(<Input placeholder="请输入 手机..." type="text"/>)}
                                              </FormItem>
                                          </Col>
                                          <Col span={12}>
                                              <FormItem
                                                  label="性别"
                                                  {...formItemLayout}>
                                                  {getFieldDecorator('gender', {
                                                    rules: [{ required: true, message: '请选择 性别!' }],
                                                    initialValue: this.state.baseData.gender
                                                  })(
                                                    <Radio.Group>
                                                      <Radio value="1">男</Radio>
                                                      <Radio value="2">女</Radio>
                                                    </Radio.Group>
                                                  )}
                                              </FormItem>
                                          </Col>
                                      </Row>
                                  </Panel>
                               </Collapse>
                           </div>
                           <FormItem wrapperCol={{ span: 25, offset: 0 }} style={{ marginTop: 24,textAlign: 'center' }}>
                               <Button type="primary" className="btnPersonalClose" htmlType="submit" icon="user-add">{this.state.alertTitle + "：工作人员"}</Button>
                               <Button icon="close" onClick={this.resetFields.bind(this)}>取 消</Button>
                           </FormItem>
                       </Form>
                     </Spin>
                 </Modal>
                 <Modal
                   visible={this.state.editAccountRole}
                   title="修改账户角色"
                   width="450px"
                   maskClosable={false}
                   footer={null}
                   wrapClassName="vertical-center-modal"
                   onCancel={this.resetFieldsByRole.bind(this)}
                 >
                     <Spin spinning={this.state.updateRoleLoading} tip="提交中...">
                       <Form onSubmit={this.editAccountRoleSubmit.bind(this)} style={{width: '100%'}}>
                            <FormItem
                              label="选择角色" hasFeedback={true} help={this.state.helpStr}>
                              {getFieldDecorator('roles', {
                                rules: [{ required: true, message: '请选择 角色!' }],
                                initialValue: this.state.baseData.roles
                              })(
                                <Select placeholder="请选择 角色!" onChange={this.onSelectChange.bind(this)}>
                                  {
                                    this.state.rolesOptions.map((item, index)=>{
                                      return (<Option key={index} value={item.id}>{item.roleName}</Option>);
                                    })
                                  }
                                </Select>
                              )}
                            </FormItem>
                           <FormItem wrapperCol={{ span: 25, offset: 0 }} style={{ marginTop: 24,textAlign: 'center' }}>
                               <Button type="primary" className="btnPersonalClose" htmlType="submit" icon="edit">修改角色</Button>
                               <Button icon="close" onClick={this.resetFieldsByRole.bind(this)}>取 消</Button>
                           </FormItem>
                       </Form>
                     </Spin>
                 </Modal>
                 <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleAddAlert} onOk={this.onConfirmAdd.bind(this)} onCancel={()=>{this.setState({visibleAddAlert: false})}}>
                     您确定要 {this.state.alertTitle}工作人员 吗？
                 </Modal>
            </div>
        )
    }
}
staffMember = Form.create()(staffMember)
export default staffMember;
