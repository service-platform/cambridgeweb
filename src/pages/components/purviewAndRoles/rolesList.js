import React from 'react';
import { Tag, Form, Input, Select, Button, Modal, Icon, Checkbox, Spin } from 'antd';
const CheckboxGroup = Checkbox.Group;
const Option = Select.Option;
const FormItem = Form.Item;
import TableView from './../TableView';
import * as actions from './../../../actions/purviewAndRolesAction';

class rolesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          uId: '',
          delId: '',
          delSensitiveWord: '',
          findSensitiveWords: GLOBAL.baseURL + "/webAPI/role/findRolesByPagination",
          visibleDeleteAlert: false,
          visibleAddAlert: false,
          addSensitiveWords: false,
          updateLoading: false,
          selectData: {},
          serviceData: [],
          searchObject: {},
          alertTitle: '添加',
          createTimer: null,
          updateTimer: null,
          sensitiveWord: null,
          purviewOptions: []
        }
    }
    componentDidMount() {
        actions.findPermissonsAll(this);
    }
    // 提交表单
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              if(values.description == undefined){values.description = "";}
              if(this.state.alertTitle == '添加'){
                  this.setState({
                    visibleAddAlert: true,
                    selectData: values
                  });

              }else if(this.state.alertTitle == '修改'){
                  this.state.selectData.roleName = values.roleName;
                  this.state.selectData.description = values.description;
                  this.state.selectData.permissiones = values.permissiones;
                  this.setState({
                    visibleAddAlert: true
                  });
              }
          }
        });
    }
    onConfirmAdd(){
        this.setState({visibleAddAlert: false, updateLoading: true});
        actions.creatOrUpdateRole(this, this.state.selectData);
    }
    deleteSensitiveWords(id, sensitiveWord){
        this.setState({visibleDeleteAlert: true, delId: id, delSensitiveWord: sensitiveWord});
    }
    cloneObj(obj){
        var str, newobj = obj.constructor === Array ? [] : {};
        if(typeof obj !== 'object'){
            return;
        } else if(window.JSON){
            str = JSON.stringify(obj), //序列化对象
            newobj = JSON.parse(str); //还原
        } else {
            for(var i in obj){
                newobj[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i];
            }
        }
        return newobj;
    }
    modifySensitiveWords(item){
        actions.findRoleDetail(this, item.id);
        this.setState({
          alertTitle: '修改',
          addSensitiveWords: true,
          selectData: this.cloneObj(item),
          createTimer: ((item.createTime == undefined)?null:new Date(item.createTime).Format("yyyy-MM-dd hh:mm:ss")),
          updateTimer: ((item.updateTime == undefined)?null:new Date(item.updateTime).Format("yyyy-MM-dd hh:mm:ss")),
          sensitiveWord: null
        });
    }
    onConfirmDelete(){
        actions.removeSensitiveWord(this, this.state.delId);
    }
    getServiceData(data) {
        this.setState({serviceData: data});
    }
    resetFields(){
      this.setState({addSensitiveWords: false});
      this.props.form.resetFields();
    }
    bindAddRole(){
      actions.findPermissonsAll(this,{pageSize: 100, pageNumber: 1, conditions: {}});
      this.setState({
        selectData: {},
        addSensitiveWords: true,
        alertTitle: '添加',
        createTimer: null,
        updateTimer: null,
        sensitiveWord: null
      });
    }
    renderItem(item, index) {
      // <Icon type="close" onClick={this.deleteSensitiveWords.bind(this, item.id, item.roleName)}/>

      return (
        <tr key={item.id} className="trContent" onClick={this.modifySensitiveWords.bind(this, item)}>
            <td className="tdName">
              <div title={item.roleName} style={{maxWidth: 160}}>{item.roleName}</div>
            </td>
            <td className="tdDescription">
              <div title={item.description} style={{maxWidth: '70%'}}>{item.description}</div>
            </td>
        </tr>
      )
    }
    render() {
        var serverList = null; var createTimer = null; var updateTimer = null;
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 16 }
        }

        if(this.state.serviceData.length != 0){
            serverList = <div>
              <table className="tableRoles" cellSpacing="1" cellPadding="0">
                <tbody>
                  <tr className="trHeader">
                    <td className="tdName">角色名</td>
                    <td className="tdDescription">描述信息</td>
                  </tr>
                  {
                      this.state.serviceData.map((item, index) => {
                          return this.renderItem(item, index);
                      })
                  }
                </tbody>
              </table>
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }

        if(this.state.createTimer != null){
          createTimer = <div>
            <FormItem
                label="创建时间："
                {...formItemLayout}>
                  <Input style={{ width: 310 }} value={this.state.createTimer} disabled={true}/>
            </FormItem>
          </div>
        }
        if(this.state.updateTimer != null){
          updateTimer = <div>
            <FormItem
                label="修改时间："
                {...formItemLayout}>
                  <Input style={{ width: 310 }} value={this.state.updateTimer} disabled={true}/>
            </FormItem>
          </div>
        }
        return (
          <div className="divTagS">
                <div style={{textAlign: 'right', marginBottom: '10px'}}>
                    <Button type="primary" icon="plus" onClick={this.bindAddRole.bind(this)}>添加角色</Button>
                </div>
                <div className="divTagContent">
                    {serverList}
                    <TableView ref="tableView"
                               getServiceData={this.getServiceData.bind(this)}
                               isService={true} searchObject={this.state.searchObject} pageNum={10}
                               url={this.state.findSensitiveWords} method="post"/>
                </div>
                <Modal
                  visible={this.state.addSensitiveWords}
                  title={this.state.alertTitle + "：角色"}
                  width="450px"
                  maskClosable={false}
                  footer={null}
                  wrapClassName="vertical-center-modal"
                  onCancel={this.resetFields.bind(this)}
                >
                  <Spin spinning={this.state.updateLoading} tip="提交中...">
                    <Form onSubmit={this.handleSubmit.bind(this)} style={{width: '100%'}}>
                        <FormItem
                            label="角色名"
                            {...formItemLayout}>
                            {getFieldDecorator('roleName', {
                              rules: [{ required: true, message: '角色名必填。'}],
                              initialValue: this.state.selectData.roleName
                            })(
                              <Input style={{ width: 310 }} placeholder="请输入 角色名..." />
                            )}
                        </FormItem>
                        <FormItem
                            label="权限列表"
                            {...formItemLayout}>
                            {getFieldDecorator('permissiones', {
                              rules: [{ required: false}],
                              initialValue: this.state.selectData.permissiones
                            })(
                                <CheckboxGroup className="divAuthorityList" options={this.state.purviewOptions}/>
                            )}
                        </FormItem>
                        <FormItem
                            label="角色描述"
                            {...formItemLayout}>
                            {getFieldDecorator('description', {
                              rules: [{ required: false}],
                              initialValue: this.state.selectData.description
                            })(
                              <Input style={{ minWidth: 310 }} type="textarea" rows={4} placeholder="请输入 角色描述..." />
                            )}
                        </FormItem>
                        {createTimer}
                        {updateTimer}
                        <FormItem wrapperCol={{ span: 25, offset: 0 }} style={{ marginTop: 24,textAlign: 'center' }}>
                            <Button type="primary" className="btnPersonalClose" htmlType="submit" icon="save">{this.state.alertTitle + "：角色"}</Button>
                            <Button icon="close" onClick={this.resetFields.bind(this)}>取 消</Button>
                        </FormItem>
                    </Form>
                  </Spin>
                </Modal>

                <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleDeleteAlert} onOk={this.onConfirmDelete.bind(this)} onCancel={()=>{this.setState({visibleDeleteAlert: false})}}>
                    您确定要删除：{this.state.delSensitiveWord} 吗？
                </Modal>
                <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleAddAlert} onOk={this.onConfirmAdd.bind(this)} onCancel={()=>{this.setState({visibleAddAlert: false})}}>
                    您确定要 {this.state.alertTitle}角色 吗？
                </Modal>
            </div>
        )
    }
}

rolesList = Form.create()(rolesList)
export default rolesList;
