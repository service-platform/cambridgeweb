import React from 'react';
import { Tag, Form, Input, Select, Button, Modal, Icon, Checkbox, Spin } from 'antd';
const Option = Select.Option;
const FormItem = Form.Item;
import TableView from './../TableView';
import * as actions from './../../../actions/purviewAndRolesAction';
var clors = ['pink','pink-inverse','red','red-inverse','orange','orange-inverse','yellow','yellow-inverse','cyan','cyan-inverse','green','green-inverse','blue','blue-inverse','purple','purple-inverse'];

class purviewList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
          uId: '',
          delId: '',
          delSensitiveWord: '',
          findSensitiveWords: GLOBAL.baseURL + "/webAPI/role/findPermissonsByPagination",
          visibleDeleteAlert: false,
          visibleAddAlert: false,
          addSensitiveWords: false,
          updateLoading: false,
          selectData: {},
          serviceData: [],
          searchObject: {},
          alertTitle: '添加',
          createTimer: null,
          updateTimer: null,
          sensitiveWord: null
        }
    }
    // 提交表单
    handleSubmit(e) {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
              if(values.description == undefined){values.description = "";}
              if(this.state.alertTitle == '添加'){
                  this.setState({
                    visibleAddAlert: true,
                    selectData: values
                  });
              }else if(this.state.alertTitle == '修改'){
                  this.state.selectData.pName = values.pName;
                  this.state.selectData.description = values.description;
                  delete this.state.selectData.colorIndex;
                  this.setState({
                    visibleAddAlert: true
                  });
              }
          }
        });
    }
    onConfirmAdd(){
        this.setState({visibleAddAlert: false, updateLoading: true});
        actions.creatOrUpdatePermission(this, this.state.selectData);
    }
    deleteSensitiveWords(id, sensitiveWord){
        return;
        this.setState({visibleDeleteAlert: true, delId: id, delSensitiveWord: sensitiveWord});
    }
    cloneObj(obj){
        var str, newobj = obj.constructor === Array ? [] : {};
        if(typeof obj !== 'object'){
            return;
        } else if(window.JSON){
            str = JSON.stringify(obj), //序列化对象
            newobj = JSON.parse(str); //还原
        } else {
            for(var i in obj){
                newobj[i] = typeof obj[i] === 'object' ? cloneObj(obj[i]) : obj[i];
            }
        }
        return newobj;
    }
    modifySensitiveWords(item){
        this.setState({
          alertTitle: '修改',
          addSensitiveWords: true,
          selectData: this.cloneObj(item),
          createTimer: ((item.createTime == undefined)?null:new Date(item.createTime).Format("yyyy-MM-dd hh:mm:ss")),
          updateTimer: ((item.updateTime == undefined)?null:new Date(item.updateTime).Format("yyyy-MM-dd hh:mm:ss")),
          sensitiveWord: null
        });
    }
    onConfirmDelete(){
        actions.removeSensitiveWord(this, this.state.delId);
    }
    getServiceData(data) {
        for (var i = 0; i < data.length; i++) {
          try{
            if(this.state.serviceData[i].colorIndex != undefined){
              data[i].colorIndex = this.state.serviceData[i].colorIndex
            }
          }catch(e){
              data[i].colorIndex = parseInt(Math.random()*17);
          }
        }
        this.setState({serviceData: data});
    }
    renderItem(item, index) {
      return (
        <Tag key={item.id} style={{marginBottom: '10px'}} color={clors[item.colorIndex]}>
            <div style={{margin: '5px'}}>
              <span title={((item.description == "")?"":"描述：" + item.description)} style={{marginRight: '5px'}} onClick={this.modifySensitiveWords.bind(this, item)}>{item.pName}</span>
            </div>
        </Tag>
      )
    }
    resetFields(){
      this.setState({addSensitiveWords: false});
      this.props.form.resetFields();
    }
    bindAddSensitiveWords(){
      this.setState({
        selectData: {},
        addSensitiveWords: true,
        alertTitle: '添加',
        createTimer: null,
        updateTimer: null,
        sensitiveWord: null
      });
    }
    render() {
        var serverList = null; var createTimer = null; var updateTimer = null;
        const { getFieldDecorator } = this.props.form
        const formItemLayout = {
            labelCol: { span: 5 },
            wrapperCol: { span: 16 }
        }

        if(this.state.serviceData.length != 0){
            serverList = <div>
              {
                  this.state.serviceData.map((item, index) => {
                      return this.renderItem(item, index);
                  })
              }
            </div>
        }else{
            serverList = <div className="divEmptyData">对不起，没有相关数据。</div>
        }

        if(this.state.createTimer != null){
          createTimer = <div>
            <FormItem
                label="创建时间："
                {...formItemLayout}>
                  <Input style={{ width: 260 }} value={this.state.createTimer} disabled={true}/>
            </FormItem>
          </div>
        }
        if(this.state.updateTimer != null){
          updateTimer = <div>
            <FormItem
                label="修改时间："
                {...formItemLayout}>
                  <Input style={{ width: 260 }} value={this.state.updateTimer} disabled={true}/>
            </FormItem>
          </div>
        }
        return (
          <div className="divTagS">
                <div style={{textAlign: 'right', marginBottom: '10px'}}>
                    <Button type="primary" icon="plus" onClick={this.bindAddSensitiveWords.bind(this)}>添加权限</Button>
                </div>
                <div className="divTagContent">
                    {serverList}
                    <TableView ref="tableView"
                               getServiceData={this.getServiceData.bind(this)}
                               isService={true} searchObject={this.state.searchObject} pageNum={50}
                               url={this.state.findSensitiveWords} method="post"/>
                </div>
                <Modal
                  visible={this.state.addSensitiveWords}
                  title={this.state.alertTitle + "：权限"}
                  width="390px"
                  maskClosable={false}
                  footer={null}
                  wrapClassName="vertical-center-modal"
                  onCancel={this.resetFields.bind(this)}
                >
                  <Spin spinning={this.state.updateLoading} tip="提交中...">
                    <Form onSubmit={this.handleSubmit.bind(this)} style={{width: '100%'}}>
                        <FormItem
                            label="权限名"
                            {...formItemLayout}>
                            {getFieldDecorator('pName', {
                              rules: [{ required: true, message: '权限名必填。'}],
                              initialValue: this.state.selectData.pName
                            })(
                              <Input style={{ width: 260 }} placeholder="请输入 权限名..." />
                            )}
                        </FormItem>
                        <FormItem
                            label="权限描述"
                            {...formItemLayout}>
                            {getFieldDecorator('description', {
                              rules: [{ required: false}],
                              initialValue: this.state.selectData.description
                            })(
                              <Input style={{ minWidth: 260 }} type="textarea" rows={4} placeholder="请输入 权限描述..." />
                            )}
                        </FormItem>
                        {createTimer}
                        {updateTimer}
                        <FormItem wrapperCol={{ span: 25, offset: 0 }} style={{ marginTop: 24,textAlign: 'center' }}>
                            <Button type="primary" className="btnPersonalClose" htmlType="submit" icon="save">{this.state.alertTitle + "：权限"}</Button>
                            <Button icon="close" onClick={this.resetFields.bind(this)}>取 消</Button>
                        </FormItem>
                    </Form>
                  </Spin>
                </Modal>

                <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleDeleteAlert} onOk={this.onConfirmDelete.bind(this)} onCancel={()=>{this.setState({visibleDeleteAlert: false})}}>
                    您确定要删除：{this.state.delSensitiveWord} 吗？
                </Modal>
                <Modal title="提示：" maskClosable={false} wrapClassName="vertical-center-modal" visible={this.state.visibleAddAlert} onOk={this.onConfirmAdd.bind(this)} onCancel={()=>{this.setState({visibleAddAlert: false})}}>
                    您确定要 {this.state.alertTitle}权限 吗？
                </Modal>
            </div>
        )
    }
}

purviewList = Form.create()(purviewList)
export default purviewList;
