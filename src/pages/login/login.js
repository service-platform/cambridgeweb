import React from 'react';
import { browserHistory, Link } from 'react-router';
import { Form, Icon, Input, Button, Checkbox, Spin } from 'antd';
const FormItem = Form.Item;
import * as actions from './../../actions/loginAction';

class NormalLoginForm extends React.Component {
  constructor(props) {
      super(props)
      this.state = {
          loading: false
      }
  }
  componentDidMount(){
    window.onresize = function () {
      var marginTop = (document.body.clientHeight/2) - 189;
      var marginRight = parseInt(document.body.clientWidth/2)-240;
      $(".login").css("marginTop",marginTop + "px");
      $(".loginForm").css("marginTop",marginTop + "px");
      $(".login").css("marginRight",marginRight + "px");
      $(".loginForm").css("marginRight",marginRight + "px");
      window.reset();
    }
    window.onresize();

    setTimeout(function(){
      window.init()
    }, 500);
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        sessionStorage.setItem("username", values.username);
        if(values.username.indexOf("@") == -1){
            actions.login(this,{
                username: values.username,
                password: values.password
            });
        }else{
            actions.login(this,{
                email: values.username,
                password: values.password
            });
        }
      }
    });
  }
  selectTheme(val){
    if(val == 1){
      $("#output").css("display", "block");
      $("#canvas").css("display", "none");
    }else{
      $("#output").css("display", "none");
      $("#canvas").css("display", "block");
    }
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login">
        <canvas id="canvas"></canvas>
        <div id="container" className="container">
        	<div id="output" className="container"></div>
          <div className="loginForm" >
              <div className="divTopHeader">
                  <img src="/src/styles/images/logo.png"/>
                  <div>成都励慧科技有限公司</div>
              </div>
              <Spin spinning={this.state.loading} tip="登入中，请稍候...">
                <div className="divHeader">
                    <img src="/src/styles/images/logo.png" width="220"/>
                    <div className="divTitle">家长汇后台管理</div>
                </div>
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <FormItem>
                    {getFieldDecorator('username', {
                      rules: [{ required: true, message: '请输入账号、电子邮箱。' }],
                    })(
                      <Input addonBefore={<Icon type="user" size="large" style={{ fontSize: 14 }} />} placeholder="账号 / 电子邮箱" size="large" />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('password', {
                      rules: [{ required: true, message: '请输入密码。' }],
                    })(
                      <Input addonBefore={<Icon type="lock" style={{ fontSize: 14 }} />} type="password" placeholder="密码" size="large"/>
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator('remember', {
                      valuePropName: 'checked',
                      initialValue: false,
                    })(
                      <Checkbox>记住密码</Checkbox>
                    )}
                    <a className="login-form-forgot">忘记密码</a>
                    <Button type="primary" htmlType="submit" className="login-form-button">登录系统</Button>
                  </FormItem>
                </Form>
              </Spin>
              <div className="divCopyright">版权所有 © 2017 成都励慧科技</div>
              <div className="divFooter">支持IE10及以上版本的浏览器。为了您更顺畅的使用体验，建议使用：谷歌Chrome浏览器</div>
          </div>
          <div className="divSelectTheme">
              <div className="divThemeTitle">请选择主题：</div>
              <img src="/src/styles/images/theme/BG1.png" onClick={this.selectTheme.bind(this, 1)} title="选择：主题 1"/>
              <img src="/src/styles/images/theme/BG2.png" onClick={this.selectTheme.bind(this, 2)} title="选择：主题 2"/>
          </div>
        </div>

      </div>
    );
  }
}
const WrappedNormalLoginForm = Form.create()(NormalLoginForm);
export default WrappedNormalLoginForm;
